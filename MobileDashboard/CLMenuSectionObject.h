//
//  CLMenuSectionObject.h
//  Pearls
//
//  Created by Christopher Nelson on 1/26/16.
//  Copyright © 2016 Christopher Nelson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLMenuSectionObject : NSObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, copy) NSString* (^generatedTitle)();

@property (nonatomic, copy) BOOL (^canDisplayMenuSection)();

@property (nonatomic, strong, readonly) NSMutableArray* menuEntries;

@end
