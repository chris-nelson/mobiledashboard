//
//  PieChartMarker.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 2/3/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit
import Charts

class PieChartMarker: BalloonMarker {

    fileprivate var yFormatter = NumberFormatter()

    public override init(color: UIColor, font: UIFont, textColor: UIColor, insets: UIEdgeInsets)
    {
        super.init(color: color, font: font, textColor: textColor, insets: insets)
    
        yFormatter.minimumFractionDigits = 1
        yFormatter.maximumFractionDigits = 1
    }

    open override func refreshContent(entry: ChartDataEntry, highlight: Highlight)
    {
        let pieChartEntry : PieChartDataEntry? = entry as? PieChartDataEntry
        if(pieChartEntry != nil)
        {
            setLabel("\(pieChartEntry!.label!) - " + yFormatter.string(from: NSNumber(floatLiteral: entry.y))!)
        }
    }
}
