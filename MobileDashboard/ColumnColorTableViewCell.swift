//
//  ColumnColorTableViewCell.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/24/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ColumnColorTableViewCell: UITableViewCell {

    @IBOutlet weak var columnTextField: UILabel!
    @IBOutlet weak var columnColorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.columnColorView.layer.borderColor = UIColor.black.cgColor;
        self.columnColorView.layer.borderWidth = 1;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
