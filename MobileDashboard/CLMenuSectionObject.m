//
//  CLMenuSectionObject.m
//  Pearls
//
//  Created by Christopher Nelson on 1/26/16.
//  Copyright © 2016 Christopher Nelson. All rights reserved.
//

#import "CLMenuSectionObject.h"

@interface CLMenuSectionObject ()

@property (nonatomic, strong, readwrite) NSMutableArray* menuEntries;

@end

@implementation CLMenuSectionObject

- (NSMutableArray*) menuEntries
{
    if(_menuEntries == nil)
    {
        _menuEntries = [[NSMutableArray alloc]init];
    }
    return _menuEntries;
}

- (NSString*) title
{
    NSString* title = _title;
    if(self.generatedTitle != nil)
        title = self.generatedTitle();
    
    return title;
}

@end
