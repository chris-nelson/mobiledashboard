#import "CLPinView.h"

@interface CLPinView () <UITextFieldDelegate>

@property (nonatomic, strong) NSArray *imageViewsArray;
@property (nonatomic, strong) NSArray *labelsArray;
@property (nonatomic, strong) UITextField *inputTextField;

@property (nonatomic, getter = isInitialized) BOOL initialized;
@property (nonatomic, strong) NSString *firstTimeEnteredPin;

@property (nonatomic) BOOL firstTimePinVerified;

@end

#define NUMBERS_ONLY @"1234567890"

@implementation CLPinView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    if (!self.isInitialized)
    {
        self.numberOfPins = 4;
        
        //You can freely use background color in XIB's
        self.backgroundColor = [UIColor clearColor];
        _pinBorder = 1;
        _pinBorderColor = [UIColor blackColor];
        _pinBackgroundColor = [UIColor whiteColor];
        
        _secureMode = NO;

        _normalPinImage = [UIImage imageNamed:@"pinViewUnSelected"];
        _selectedPinImage = [UIImage imageNamed:@"pinViewSelected"];
        
        //Fake text field
        self.inputTextField.delegate = self;

        //Build UI
        
        //Add pincodes
        NSMutableArray *pinCodesContainer = [NSMutableArray new];
        NSMutableArray *pinLabelContainer = [NSMutableArray new];
        for (NSInteger cnt = 0;cnt < self.numberOfPins; cnt++)
        {
            UIImageView *pinImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
            pinImageView.image = _normalPinImage;
            pinImageView.highlightedImage = _selectedPinImage;
            pinImageView.contentMode = UIViewContentModeCenter;
            pinImageView.autoresizingMask =
            UIViewAutoresizingFlexibleWidth
            | UIViewAutoresizingFlexibleHeight
            | UIViewAutoresizingFlexibleLeftMargin
            | UIViewAutoresizingFlexibleRightMargin;
            pinImageView.hidden = !self.secureMode;
            [pinCodesContainer addObject:pinImageView];
            
            [self addSubview:pinImageView];

            UILabel* pinLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            [self addSubview:pinLabel];
            pinLabel.text = @" ";
            pinLabel.layer.borderColor = self.pinBorderColor.CGColor;
            pinLabel.layer.borderWidth = 3.0;
            pinLabel.textAlignment = NSTextAlignmentCenter;
            
            pinLabel.font = [pinLabel.font fontWithSize:75];
            pinLabel.backgroundColor = self.pinBackgroundColor;
            
            pinLabel.minimumScaleFactor = 0.0;
            pinLabel.adjustsFontSizeToFitWidth = NO;
            pinLabel.hidden = self.secureMode;
            [pinLabelContainer addObject:pinLabel];
        }
        _imageViewsArray = [pinCodesContainer copy];
        _labelsArray = [pinLabelContainer copy];
        
        //Tap gesture
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureOccured:)];
        [self addGestureRecognizer:tapGesture];
        
        _initialized = YES;
    }
}

- (UIFont *)findAdaptiveFontWithName:(NSString *)fontName forUILabelSize:(CGSize)labelSize withMinimumSize:(NSInteger)minSize
{
    UIFont *tempFont = nil;
    NSString *testString = @"8";
    
    NSInteger mid = 0;
    NSInteger tempMin = minSize;
    NSInteger tempMax = 256;
    NSInteger midWidth = 0;
    NSInteger difference = 0;
    
    while (tempMin <= tempMax)
    {
        midWidth = tempMin + (tempMax - tempMin) / 2;
        tempFont = [UIFont fontWithName:fontName size:midWidth];
        NSDictionary *attributes = @{NSFontAttributeName: tempFont};
        difference = labelSize.width - [testString sizeWithAttributes:attributes].width;
        
        if (midWidth == tempMin || midWidth == tempMax) {
            if (difference < 0)
            {
                midWidth = midWidth - 1;
                break;
            }
            
            break;
        }
        
        if (difference < 0)
        {
            tempMax = midWidth - 1;
        }
        else if (difference > 0)
        {
            tempMin = midWidth + 1;
        }
        else
        {
            break;
        }
    }
    
    tempFont = nil;
    
    tempMin = minSize;
    tempMax = 256;
    NSInteger midHeight = 0;
    difference = 0;
    
    while (tempMin <= tempMax)
    {
        midHeight = tempMin + (tempMax - tempMin) / 2;
        tempFont = [UIFont fontWithName:fontName size:midHeight];
        NSDictionary *attributes = @{NSFontAttributeName: tempFont};
        difference = labelSize.height - [testString sizeWithAttributes:attributes].height;

        if (midHeight == tempMin || midHeight == tempMax) {
            if (difference < 0)
            {
                midHeight = midHeight - 1;
                break;
            }
            
            break;
        }
        
        if (difference < 0)
        {
            tempMax = midHeight - 1;
        }
        else if (difference > 0)
        {
            tempMin = midHeight + 1;
        }
        else
        {
            break;
        }
    }

    if(midWidth < midHeight)
        mid = midWidth;
    else
        mid = midHeight;
    
    return [UIFont fontWithName:fontName size:mid];
}
- (void)layoutSubviews
{
    NSLog(@"Layout Subviews");
    
    CGFloat width = self.bounds.size.width;
    CGFloat itemWidth = floor(width / (CGFloat)self.numberOfPins);
    
    UIFont* fontSized = nil;

    for (NSInteger cnt = 0;cnt < self.numberOfPins; cnt++)
    {
        UIImageView *pinImageView = self.imageViewsArray[cnt];
        CGRect rect1 = CGRectMake(cnt * itemWidth, 0.0f, itemWidth, self.bounds.size.height);
        pinImageView.frame = rect1;
        
        UILabel* label = self.labelsArray[cnt];
        CGRect rect2 = CGRectMake((cnt * itemWidth) + self.pinBorder, 0.0f + self.pinBorder, itemWidth - (self.pinBorder * 2), self.bounds.size.height - (self.pinBorder * 2));
//        rect2 = CGRectInset(rect2, rect2.size.width * -0.2, rect2.size.height * -0.2);
        label.frame = rect2;

        if(fontSized == nil)
        {
            fontSized =  [self findAdaptiveFontWithName:label.font.fontName forUILabelSize:label.frame.size withMinimumSize:10];

        }
        label.font = [fontSized copy];
    }
}

#pragma mark - Lazy Instantiation

-(UITextField*) inputTextField
{
    if(_inputTextField == nil)
    {
        _inputTextField = [[UITextField alloc] initWithFrame:CGRectZero];
        _inputTextField.keyboardType = UIKeyboardTypeNumberPad;
        [_inputTextField addTarget:self action:@selector(textFieldTextChanged:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:_inputTextField];
    }
    return _inputTextField;
}

- (void)displayKeyboard
{
    double delayInSeconds = .0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self becomeFirstResponder];
    });
}

- (void) dismissKeyboard
{
    [self.inputTextField resignFirstResponder];
}

#pragma mark - Images

- (void)setNormalPinImage:(UIImage *)normalPinImage
{
    _normalPinImage = normalPinImage;
    
    //Set normal image
    [self.imageViewsArray makeObjectsPerformSelector:@selector(setImage:)
                                    withObject:normalPinImage];
}

- (void)setSelectedPinImage:(UIImage *)selectedPinImage
{
    _selectedPinImage = selectedPinImage;
    
    //Set selected image
    [self.imageViewsArray makeObjectsPerformSelector:@selector(setHighlightedImage:)
                                    withObject:selectedPinImage];
}

- (void) setSecureMode:(BOOL)secureMode
{
    _secureMode = secureMode;
    
    for (NSInteger cnt = 0;cnt < self.numberOfPins; cnt++)
    {
        UIImageView *pinImageView = self.imageViewsArray[cnt];
        UILabel* label = self.labelsArray[cnt];
        
        pinImageView.hidden = !_secureMode;
        label.hidden = _secureMode;
    }
}

- (void) setPinBorder:(NSUInteger)pinBorder
{
    _pinBorder = pinBorder;
    
    for (NSInteger cnt = 0;cnt < self.numberOfPins; cnt++)
    {
        UILabel* label = self.labelsArray[cnt];
        label.layer.borderWidth = _pinBorder;
    }
}

- (void) setPinBorderColor:(UIColor *)pinBorderColor
{
    _pinBorderColor = pinBorderColor;
    
    for (NSInteger cnt = 0;cnt < self.numberOfPins; cnt++)
    {
        UILabel* label = self.labelsArray[cnt];
        label.layer.borderColor = _pinBorderColor.CGColor;
    }
}

- (void) setPinBackgroundColor:(UIColor *)pinBackgroundColor
{
    _pinBackgroundColor = pinBackgroundColor;
    
    for (NSInteger cnt = 0;cnt < self.numberOfPins; cnt++)
    {
        UILabel* label = self.labelsArray[cnt];
        label.backgroundColor = _pinBackgroundColor;
    }
}

#pragma mark - Responder
- (BOOL)becomeFirstResponder
{
    if(self.pinMode != CLPinModeReadOnlyPin)
        [self.inputTextField becomeFirstResponder];
    
    return NO;
}

- (BOOL)resignFirstResponder
{
    [super resignFirstResponder];
    
    [self.inputTextField resignFirstResponder];
    return NO;
}

#pragma mark - UITextField
- (void)textFieldTextChanged:(UITextField *)textField
{
    //Trimmed text
    textField.text = [self trimmedStringWithMaxLenght:textField.text];
    
    _pinCode = textField.text;
    
    //Colorize pins
    [self colorizePins];
    
    //Notify delegate if needed
    [self checkForEnteredPin];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return (([string isEqualToString:filtered])&&(newLength <= self.numberOfPins));
}

- (void)setPinCode:(NSString *)pinCode
{
    //Trimmed text
    NSString *enteredCode = [self trimmedStringWithMaxLenght:pinCode];
    
    _pinCode = enteredCode;
    self.inputTextField.text = enteredCode;
    
    //Colorize pins
    [self colorizePins];
    
    //Notify delegate if needed
    [self checkForEnteredPin];
}

#pragma mark - ColorizeViews
- (void)colorizePins
{
    NSInteger pinsEntered = self.pinCode.length;
    NSInteger itemsCount = self.imageViewsArray.count;
    for (NSInteger i = 0; i < itemsCount; i++)
    {
        UIImageView *pinImageView = self.imageViewsArray[i];
        pinImageView.highlighted = i < pinsEntered;
        
        UILabel* label = self.labelsArray[i];
        label.text = (i < pinsEntered) ? [self.pinCode substringWithRange:NSMakeRange(i, 1) ] : @" ";
    }
}

#pragma mark - Delegate
- (void)checkForEnteredPin
{
    if(self.pinMode != CLPinModeReadOnlyPin)
    {
        if(self.pinCodeToCheck == nil && (self.pinMode == CLPinModeVerifyPin || self.pinMode == CLPinModeChangePin))
        {
            if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didFailVerificationWithCount:)])
            {
                [self.delegate pinCodeViewController:self didFailVerificationWithCount:0];
            }
            //Message
            if ([self.delegate respondsToSelector:@selector(showPinsNoAvailableMessage)])
            {
                [self.delegate showPinsNoAvailableMessage];
            }
//            [self didEnterPin:self.pinCode];
            _pinCode = @"";
            _inputTextField.text = @"";
            [self colorizePins];
        }
        else if (self.pinCode.length == self.numberOfPins)
        {
            if(self.pinMode == CLPinModeInputPin)
            {
                if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didInputPinCode:)])
                {
                    [self.delegate pinCodeViewController:self didInputPinCode:self.pinCode];
                }
            }
            else
            {
                [self didEnterPin:self.pinCode];
            }
        }
    }
}

#pragma mark - Gestures
- (void)tapGestureOccured:(UITapGestureRecognizer *)tapGesture
{
    if(self.pinMode != CLPinModeReadOnlyPin)
        [self becomeFirstResponder];
}

#pragma mark - Helpers
- (NSString *)trimmedStringWithMaxLenght:(NSString *)sourceString
{
    if (sourceString.length > self.numberOfPins) {
        sourceString = [sourceString substringToIndex:self.numberOfPins];
    }
    return sourceString;
}


/////
- (void) didEnterPin:(NSString*)pinCode
{
    // Small Hack to give time to show the last entered number
    double delayInSeconds = 0.15;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        self.pinCode = nil;
        
        //Verify case
        if (self.pinMode == CLPinModeVerifyPin)
        {
            if(self.pinCodeToCheck != nil)
            {
                [self verifyPinWithEntered:pinCode];
            }
            else
            {
                if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didFailVerificationWithCount:)])
                {
                    [self.delegate pinCodeViewController:self didFailVerificationWithCount:0];
                }
                //Message
                if ([self.delegate respondsToSelector:@selector(showPinsNoAvailableMessage)])
                {
                    [self.delegate showPinsNoAvailableMessage];
                }
            }
        }
        else if (self.pinMode == CLPinModeChangePin)
        {
            if(self.pinCodeToCheck != nil)
            {
                [self changePinWithEntered:pinCode];
            }
            else
            {
                if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didFailVerificationWithCount:)])
                {
                    [self.delegate pinCodeViewController:self didFailVerificationWithCount:0];
                }
                //Message
                if ([self.delegate respondsToSelector:@selector(showPinsNoAvailableMessage)])
                {
                    [self.delegate showPinsNoAvailableMessage];
                }
            }
        }
        else
        {
            [self setEnteredPin:pinCode];
        }
    });
}

- (void)verifyPinWithEntered:(NSString *)pinCode
{
    //Pins matched
    if ([self.pinCodeToCheck isEqualToString:pinCode])
    {
        if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didVerifiedPincodeSuccessfully:)])
        {
            [self.delegate pinCodeViewController:self didVerifiedPincodeSuccessfully:pinCode];
        }
        
        if ([self.delegate respondsToSelector:@selector(showPinVerifiedMessage)])
        {
            [self.delegate showPinVerifiedMessage];
        }
    }
    else
    {
        _failedAttempts++;
        
        if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didFailVerificationWithCount:)])
        {
            [self.delegate pinCodeViewController:self didFailVerificationWithCount:self.failedAttempts];
        }
        if ([self.delegate respondsToSelector:@selector(showPinsDontMatchMessage)])
        {
            [self.delegate showPinsDontMatchMessage];
        }
    }
}

- (void)setEnteredPin:(NSString *)pinCode
{
    //This is first attempt
    if (self.firstTimeEnteredPin == nil) {
        self.firstTimeEnteredPin = [pinCode copy];
        
        //Message
        if ([self.delegate respondsToSelector:@selector(showEnterOnceAgainMessage)])
        {
            [self.delegate showEnterOnceAgainMessage];
        }
        
        //Here we should compare them
    }
    else
    {
        //They are equal
        if ([pinCode isEqualToString:self.firstTimeEnteredPin])
        {
            if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didCreatePinCode:)])
            {
                [self.delegate pinCodeViewController:self didCreatePinCode:pinCode];
            }
            
            //Message
            if ([self.delegate respondsToSelector:@selector(showPinCreatedMessage)])
            {
                [self.delegate showPinCreatedMessage];
            }
            
            //Passwords don't match. Let's go over again
        }
        else
        {
            self.firstTimeEnteredPin = nil;
            _failedAttempts++;
            
            if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didFailVerificationWithCount:)])
            {
                [self.delegate pinCodeViewController:self didFailVerificationWithCount:self.failedAttempts];
            }
            
            //Message
            if ([self.delegate respondsToSelector:@selector(showPinsDontMatchMessage)])
            {
                [self.delegate showPinsDontMatchMessage];
            }
        }
    }
}

- (void)changePinWithEntered:(NSString *)pinCode
{
    //User have not verified password yet
    if (!_firstTimePinVerified)
    {
        //user entered valid
        if ([self.pinCodeToCheck isEqualToString:pinCode])
        {
            _firstTimePinVerified = YES;
            
            //Message
            if ([self.delegate respondsToSelector:@selector(showChangeThePinMessage)])
            {
                [self.delegate showChangeThePinMessage];
            }
        }
        else
        {
            //Notify about fail
            _failedAttempts++;
            if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didFailVerificationWithCount:)])
            {
                [self.delegate pinCodeViewController:self didFailVerificationWithCount:self.failedAttempts];
            }
            
            //Message
            if ([self.delegate respondsToSelector:@selector(showPinsDontMatchMessage)])
            {
                [self.delegate showPinsDontMatchMessage];
            }
        }
    }
    else
    {
        //Create new password
        if (self.firstTimeEnteredPin == nil)
        {
            self.firstTimeEnteredPin = [pinCode copy];
            
            if ([self.delegate respondsToSelector:@selector(showEnterOnceAgainMessage)])
            {
                [self.delegate showEnterOnceAgainMessage];
            }
        }
        else
        {
            //Everything is good
            if ([self.firstTimeEnteredPin isEqualToString:pinCode])
            {
                if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didChangePinCode:)])
                {
                    [self.delegate pinCodeViewController:self didChangePinCode:pinCode];
                }
                
                if ([self.delegate respondsToSelector:@selector(showPinCreatedMessage)])
                {
                    [self.delegate showPinCreatedMessage];
                }
                
                //Password don't match. Let's go to step 2
            }
            else
            {
                self.firstTimeEnteredPin = nil;
                _failedAttempts++;
                
                if ([self.delegate respondsToSelector:@selector(pinCodeViewController:didFailVerificationWithCount:)])
                {
                    [self.delegate pinCodeViewController:self didFailVerificationWithCount:self.failedAttempts];
                }
                //Message
                if ([self.delegate respondsToSelector:@selector(showPinsDontMatchMessage)])
                {
                    [self.delegate showPinsDontMatchMessage];
                }
            }
        }
    }
}

@end
