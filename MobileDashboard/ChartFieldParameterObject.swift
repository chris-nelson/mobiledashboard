//
//  ChartFieldParameterObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/15/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

enum FilterType : String {
    case remoteOnly = "RemoteOnly"
    case localAndRemote = "LocalAndRemote"
    case localOnly = "LocalOnly"
}

enum SelectionType : String {
    case none = "none"
    case multipleIndividuals = "multipleIndividuals"
    case single = "single"
}

enum DataType : String {
    case string = "string"
    case date = "date"
    case number = "number"
}

class ChartFieldParameterObject: NSObject, NSCoding {

    var domainUrl : String?
    var filterType : FilterType = .remoteOnly;
    var name : String?
    var required : Bool = false
    var selectionType : SelectionType = .multipleIndividuals
    var type : DataType = .string
    
    override init() {
        
        super.init()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.domainUrl = aDecoder.decodeObject(forKey: "domainUrl") as? String ?? ""
        self.filterType = FilterType(rawValue: (aDecoder.decodeObject( forKey: "filterType" ) as! String)) ?? .remoteOnly
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.required = aDecoder.decodeBool(forKey: "required")
        self.selectionType = SelectionType(rawValue: (aDecoder.decodeObject( forKey: "selectionType" ) as! String)) ?? .multipleIndividuals
        self.type = DataType(rawValue: (aDecoder.decodeObject( forKey: "type" ) as! String)) ?? .string
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.domainUrl, forKey: "domainUrl")
        aCoder.encode(self.filterType.rawValue, forKey: "filterType")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.required, forKey: "required")
        aCoder.encode(self.selectionType.rawValue, forKey: "selectionType")
        aCoder.encode(self.type.rawValue, forKey: "type")
    }

    func validateFieldParameters() -> Bool {
        
        var pass = true;

        if(name?.lengthOfBytes(using: String.Encoding.utf8) == 0)
        {
            pass = false;
        }
        
        return pass
    }

    override var description: String {
        
        get {
            
            let desc = "\nName: \(self.name!)\nFilter Type: \(self.filterType)\nRequired: \(self.required)\nDomainURL \(self.domainUrl!)\nSelectionType: \(self.selectionType)\nType: \(self.type)\n"
            
            return desc
        }
    }


}
