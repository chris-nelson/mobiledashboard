//
//  CLExternalDisplayManager.m
//  OdeonCoreLibrary
//
//  Created by Christopher Nelson on 10/5/14.
//  Copyright (c) 2014 Odeon Software. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "CLExternalDisplayManager.h"

@interface CLExternalDisplayManager ()

@property (nonatomic, strong) UIScreen* externalScreen;
@property (nonatomic, strong) UIWindow* externalWindow;
@property (nonatomic, strong, readwrite) NSArray* availableModes;

@property (nonatomic) BOOL notificationsEnabled;

@end

@implementation CLExternalDisplayManager

+ (CLExternalDisplayManager *)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[CLExternalDisplayManager alloc] init];
    });
    
    return _sharedObject;
}

- (instancetype)init
{
    if (self = [super init]) {
        // initializer logic
        [self enableNotifications];
        
        self.selectedScreenMode = -1;
    }
    return self;
}

-(void) setSelectedScreenMode:(int)selectedScreenMode
{
    if(selectedScreenMode < -1 || selectedScreenMode > [self.availableModes count])
    {
        _selectedScreenMode = -1;
    }
    else
    {
        _selectedScreenMode = selectedScreenMode;
    }
}

-(void) enableNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenDidChange:) name:UIScreenDidConnectNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenDidChange:) name:UIScreenDidDisconnectNotification object:nil];
    
    self.notificationsEnabled = YES;
}

-(void) disableNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenDidConnectNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenDidDisconnectNotification object:nil];
    
    self.notificationsEnabled = NO;
}

- (void)dealloc
{
    if(self.notificationsEnabled)
    {
        [self disableNotifications];
    }
}

-(NSArray*) refreshScreens
{
    NSArray* screens = [UIScreen screens];
    
    long screenNum = 1;
    for (UIScreen* aScreen in screens)
    {
        NSArray *displayModes;
        
        displayModes = [aScreen availableModes];
        NSLog(@"Display Modes - %lu", (unsigned long)[displayModes count]);
        displayModes = nil;
        
        screenNum++;
    }
    
    return screens;
}

- (void)screenDidChange:(NSNotification *)notification
{
    [self updateAvailableModes];
    
    if(self.delegate != nil)
    {
        [self.delegate externalDisplayDidChange];
    }
}

-(void) updateAvailableModes
{
    NSArray* screens = [self refreshScreens];
    NSUInteger screenCount = [screens count];
    
    self.availableModes = nil;
    if (screenCount > 1)
    {
        if([UIScreen mainScreen].mirroredScreen != nil)
            self.availableModes = [[UIScreen mainScreen].mirroredScreen availableModes];
        else
            self.availableModes = [[screens objectAtIndex:1] availableModes];
    }

    NSLog(@"Screens - %lu", (unsigned long)screenCount);

    if (screenCount > 1)
    {
        // Select first external screen
        if([UIScreen mainScreen].mirroredScreen != nil)
            self.externalScreen = [UIScreen mainScreen].mirroredScreen;
        else
            self.externalScreen = [screens objectAtIndex:1];
        
        if(self.selectedScreenMode >= 0)
        {
            self.externalScreen.currentMode = [self.availableModes objectAtIndex:self.selectedScreenMode];
            
            if (self.externalWindow == nil || !CGRectEqualToRect(self.externalWindow.bounds, [self.externalScreen bounds]))
            {
                // Size of window has actually changed
                
                self.externalWindow = [[UIWindow alloc] initWithFrame:[self.externalScreen bounds]];
                
                self.externalWindow.screen = self.externalScreen;
                
                [self.externalView removeFromSuperview];
                self.externalView = [[UIView alloc] initWithFrame:self.externalWindow.frame];
                self.externalView.contentMode = UIViewContentModeScaleAspectFit;
                self.externalView.backgroundColor = [UIColor blackColor];

                [self.externalWindow addSubview:self.externalView];
                [self.externalWindow makeKeyAndVisible];
            }
        }
        else
        {
            [self.externalView removeFromSuperview];
            self.externalWindow = nil;
            self.selectedScreenMode = -1;
        }
    }
    else
    {
        // Release external screen and window
//        self.externalScreen = nil;
        
        self.externalWindow = nil;
        [self.externalView removeFromSuperview];
        self.externalScreen = nil;
        self.availableModes = nil;
        self.selectedScreenMode = -1;
    }
}

@end
