//
//  CLMenuEntryObject.h
//

#import <UIKit/UIKit.h>

@interface CLMenuEntryObject : NSObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* badgeText;
@property (nonatomic, assign) NSUInteger badgeOffset;
@property (nonatomic, assign) NSUInteger screenType;
@property (nonatomic, strong) UIImage* image;

@property (nonatomic, copy) NSString* (^generatedTitle)();

@property (nonatomic, copy) BOOL (^test)(BOOL boolVal);
@property (nonatomic, copy) BOOL (^canDisplayMenuEntry)();
@property (nonatomic, copy) void (^executeMenuEntry)(UIView* view);

@property (nonatomic, strong, readonly) NSMutableArray* menuSubEntries;

@end
