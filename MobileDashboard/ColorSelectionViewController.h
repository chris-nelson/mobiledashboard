//
//  ColorSelectionViewController.h
//  Pods
//
//  Created by Christopher Nelson on 9/7/15.
//
//

#import <UIKit/UIKit.h>

@class ColorSelectionViewController;

@protocol ColorSelectionDelegate <NSObject>

@required

- (void) saveColorSelection:(ColorSelectionViewController*) colorSelectionViewController color:(UIColor*)color;

@optional

- (void) cancelColorSelection:(ColorSelectionViewController*) colorSelectionViewController;

@end


@interface ColorSelectionViewController : UIViewController

@property (nonatomic, weak) id<ColorSelectionDelegate> delegate;
@property (nonatomic, strong) UIColor *selectedColor;
@property (nonatomic, strong) NSString* titleOverride;

// if enabled be sure to implement 'cancelColorSelection' in the protocol to get a chance to close
@property (nonatomic, assign) BOOL enableSaveButton;
@property (nonatomic, assign) BOOL enableCloseButton;
@property (nonatomic, assign) BOOL hideClearColorButton;

@end
