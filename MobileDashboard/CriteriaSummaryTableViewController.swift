//
//  CriteriaSummaryTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/5/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class CriteriaSummaryTableViewController: UITableViewController {

    weak var chartDelegate : ChartConfigurationDelegate?
    var chartDataObject : ChartDataObject?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.chartDelegate?.enableAddButton(routingMode: .selectCriteria)
        
        let URL = self.chartDataObject?.chartQuery.getURL()
        if let goodURL = URL
        {
            print("URL - \(goodURL)")
        }

        if((self.chartDataObject?.chartQuery.chartQueryParamArray.count)! > 0)
        {
            self.tableView.setEditing(true, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if(self.chartDataObject?.chartQuery.chartQueryParamArray.count == 0)
        {
            return 1
        }
        
        return (self.chartDataObject?.chartQuery.chartQueryParamArray.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CriteraCell", for: indexPath)

        // Configure the cell...
        if(self.chartDataObject?.chartQuery.chartQueryParamArray.count == 0)
        {
            cell.textLabel?.text = "Press '+' to add criteria."
        }
        else
        {
            let chartQueryParameterObject = (self.chartDataObject?.chartQuery.chartQueryParamArray[indexPath.row])! as ChartQueryParameterObject
            cell.textLabel?.text = "\(chartQueryParameterObject.fieldParameterName!) = \(chartQueryParameterObject.fieldParameterValue!)"
        }

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            self.chartDataObject?.chartQuery.chartQueryParamArray.remove(at: indexPath.row)
            if(self.chartDataObject?.chartQuery.chartQueryParamArray.count == 0)
            {
                tableView.setEditing(false, animated: true)
                tableView.reloadData()
            }
            else
            {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }/* else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }  */
    }
}
