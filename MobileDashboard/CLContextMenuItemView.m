//
//  MenuItemView.m
//  TestMenu
//
//  Created by Christopher Nelson on 2/24/16.
//  Copyright © 2016 Odeon Software. All rights reserved.
//

#import "CLContextMenuItemView.h"

@interface CLContextMenuItemView ()


@end

@implementation CLContextMenuItemView

- (UIImageView*) menuImageView
{
    if(_menuImageView == nil)
    {
        _menuImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self addSubview:_menuImageView];
    }
    
    return  _menuImageView;
}

- (UILabel*) menuLabel
{
    if(_menuLabel == nil)
    {
        _menuLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _menuLabel.backgroundColor = [UIColor clearColor];
        _menuLabel.numberOfLines = 1;
        _menuLabel.font = [UIFont systemFontOfSize:12.0f];
        _menuLabel.textAlignment = NSTextAlignmentRight;
        _menuLabel.textColor = [UIColor whiteColor];
        _menuLabel.text = @"";
        [self addSubview:_menuLabel];
    }
    return  _menuLabel;
}

+ (UIView*) createDimmingView
{
    UIView* dimmingView;
    
    UIBlurEffect* blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    dimmingView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];//[[UIView alloc] init];
    dimmingView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];//[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.1];//
    dimmingView.alpha = 1.0;
    
    return dimmingView;
}

- (UIView*) blurView
{
    if(_blurView == nil)
    {
        _blurView = [CLContextMenuItemView createDimmingView];
        [self insertSubview:_blurView belowSubview:self.menuLabel];
    }

    return _blurView;
}

- (void) setMenuEntry:(CLMenuEntryObject *)menuEntry
{
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMenu:)];
    [self addGestureRecognizer:tapGesture];
    
    //self.menuImageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    //self.menuImageView.backgroundColor = [UIColor grayColor];
    self.menuImageView.image = menuEntry.image;
    self.menuImageView.backgroundColor = [UIColor clearColor];
    self.menuLabel.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.menuLabel.text = menuEntry.title;
    
    _menuEntry = menuEntry;
}

- (void) tapMenu:(UITapGestureRecognizer*)sender
{
    if(self.menuEntry.executeMenuEntry != nil)
    {
        self.menuEntry.executeMenuEntry(nil);
    }

    [self.delegate hideMenu:YES];
}

@end
