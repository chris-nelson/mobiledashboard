//
//  ChartDataObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/30/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

enum ChartType : Int {

    case barChart
    case horizonatalBarChart
    case stackedBarChart
    case horizonatalStackedBarChart
    case pieChart
    case singleLineChart
    case lineChart
    case scatterChart
    case bubbleChart
    case cancelstickChart
    case radarChart
    
    static func count() -> Int {
        return 11
    }
    
    func name () -> String {
        
        switch self {
        case .barChart:
            return "Bar Chart"
        case .horizonatalBarChart:
            return "Horizontal Bar Chart"
        case .stackedBarChart:
            return "Stacked Bar Chart"
        case .horizonatalStackedBarChart:
            return "Horizontal Stacked Bar Chart"
        case .pieChart:
            return "Pie Chart"
        case .singleLineChart:
            return "Single Line Chart"
        case .lineChart:
            return "Line Chart"
        case .bubbleChart:
            return "Bubble Chart"
        case .cancelstickChart:
            return "Candlestick Chart"
        case .radarChart:
            return "Radar Chart"
        case .scatterChart:
            return "Scatter Chart"
        }
    }
    func imageName () -> String {
        
        switch self {
        case .barChart:
            return "BarChart"
        case .horizonatalBarChart:
            return "HorizontalBarChart"
        case .stackedBarChart:
            return "StackedBarChart"
        case .horizonatalStackedBarChart:
            return "HorizonatalStackedBarChart"
        case .pieChart:
            return "PieChart"
        case .lineChart:
            return "LineChart"
        case .singleLineChart:
            return "LineChart"
        case .bubbleChart:
            return "BubbleChart"
        case .cancelstickChart:
            return "CandlestickChart"
        case .radarChart:
            return "RadarChart"
        case .scatterChart:
            return "ScatterChart"
        }
    }
}

enum ChartAnimationType : Int {
    case none
    case xAxis
    case yAxis
    case xyAxis
}

enum ChartLabelLocationType : Int {
    case hide
    case showAbove
    case showBelow
}

enum ChartGridLinesType : Int {
    case none
    case xAxisOnly
    case yAxisOnly
    case bothAxis
}

class ChartDataObject: NSObject, NSCoding {

    var name : String = "Unnamed"
    var identifier : String?
    var dataSourceIdentifier : Int = -1
    
    var chartQuery : ChartQueryCriteriaObject = ChartQueryCriteriaObject.init()
    
    var chartDescription: String?
    
    var axisColumn : String?
    var accumulatorColumn : String?
    var groupingColumn : String?
    
    var chartType : ChartType = .barChart
    var availableChartTypes : [ChartType]?
    
    var animationType : ChartAnimationType = .xyAxis
    var xAxisLabelsCount : Int = 3
    
    var displayAsSingleColor : Bool = false
    var chartColors : [UIColor]?
   
    var displayValueLabelLocation : ChartLabelLocationType = .showAbove
    
    var gridLinesType : ChartGridLinesType = .bothAxis
    var gridLineColor : UIColor = .gray

    var borderWidth : Int = 0 // Default to no border
    var borderColor : UIColor = .black

    var displayPieChartWithHole : Bool = false
    var pieChartHoleColor : UIColor = .white
    var displayHalfPieChart : Bool = false

    var chartBackgroundColor : UIColor = .white
    
    init(identifier : String, dataSourceIdentifier : Int) {
        
        self.identifier = identifier
        self.dataSourceIdentifier = dataSourceIdentifier

        super.init()

        self.generateDefaultChartColors()
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.identifier = aDecoder.decodeObject(forKey: "identifier") as? String ?? ""
        self.dataSourceIdentifier = aDecoder.decodeInteger(forKey: "dataSourceIdentifier")
        self.chartQuery = aDecoder.decodeObject(forKey: "chartQuery") as! ChartQueryCriteriaObject
        self.chartDescription = aDecoder.decodeObject(forKey: "chartDescription") as? String ?? ""
        self.groupingColumn = aDecoder.decodeObject(forKey: "groupingColumn") as? String ?? ""
        
        self.axisColumn = aDecoder.decodeObject(forKey: "axisColumn") as? String ?? ""
        self.xAxisLabelsCount = aDecoder.decodeInteger(forKey: "xAxisLabelsCount")
        self.accumulatorColumn = aDecoder.decodeObject(forKey: "accumulatorColumn") as? String ?? ""
        self.chartType = ChartType(rawValue: aDecoder.decodeInteger(forKey: "chartType"))!
        self.animationType = ChartAnimationType(rawValue: aDecoder.decodeInteger(forKey: "animationType"))!
        self.chartColors = aDecoder.decodeObject(forKey: "chartColors") as? [UIColor]
        self.chartBackgroundColor = aDecoder.decodeObject(forKey: "chartBackgroundColor") as? UIColor ?? .white
        
        self.displayAsSingleColor = aDecoder.decodeBool(forKey: "displayAsSingleColor")
        self.displayValueLabelLocation = ChartLabelLocationType(rawValue: aDecoder.decodeInteger(forKey: "displayValueLabelLocation"))!
        self.borderWidth = aDecoder.decodeInteger(forKey: "borderWidth")
        self.borderColor = aDecoder.decodeObject(forKey: "borderColor") as? UIColor ?? .black
        self.gridLinesType = ChartGridLinesType(rawValue: aDecoder.decodeInteger(forKey: "gridLinesType"))!
        self.gridLineColor = aDecoder.decodeObject(forKey: "gridLineColor") as? UIColor ?? .gray
        
        self.displayPieChartWithHole = aDecoder.decodeBool(forKey: "displayPieChartWithHole")
        self.pieChartHoleColor = aDecoder.decodeObject(forKey: "pieChartHoleColor") as? UIColor ?? .white
        self.displayHalfPieChart = aDecoder.decodeBool(forKey: "displayHalfPieChart")

        super.init()

        if(self.chartColors == nil || chartColors?.count == 0)
        {
            self.generateDefaultChartColors()
        }
    
//        self.availableChartTypes = aDecoder.decodeObject(forKey: "availableChartTypes") as? [ChartType] ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.identifier, forKey: "identifier")
        aCoder.encode(self.dataSourceIdentifier, forKey: "dataSourceIdentifier")
        aCoder.encode(self.chartQuery, forKey: "chartQuery")
        aCoder.encode(self.chartDescription, forKey: "chartDescription")
        aCoder.encode(self.groupingColumn, forKey: "groupingColumn")

        aCoder.encode(self.axisColumn, forKey: "axisColumn")
        aCoder.encode(self.xAxisLabelsCount, forKey: "xAxisLabelsCount")
        aCoder.encode(self.accumulatorColumn, forKey: "accumulatorColumn")
        aCoder.encode(self.chartType.rawValue, forKey: "chartType")
        aCoder.encode(self.animationType.rawValue, forKey: "animationType")
        aCoder.encode(self.chartColors, forKey: "chartColors")
        aCoder.encode(self.chartBackgroundColor, forKey: "chartBackgroundColor")

        aCoder.encode(self.displayAsSingleColor, forKey: "displayAsSingleColor")
        aCoder.encode(self.displayValueLabelLocation.rawValue, forKey: "displayValueLabelLocation")
        aCoder.encode(self.borderWidth, forKey: "borderWidth")
        aCoder.encode(self.borderColor, forKey: "borderColor")
        aCoder.encode(self.gridLinesType.rawValue, forKey: "gridLinesType")
        aCoder.encode(self.gridLineColor, forKey: "gridLineColor")
        
        aCoder.encode(self.displayPieChartWithHole, forKey: "displayPieChartWithHole")
        aCoder.encode(self.pieChartHoleColor, forKey: "pieChartHoleColor")
        aCoder.encode(self.displayHalfPieChart, forKey: "displayHalfPieChart")
    }

    override var description: String {
        get {
            
            if let desc = self.chartDescription
            {
                return desc
            }
            
            return super.description
            
        }
    }
    
    func generateDefaultChartColors()
    {
        self.chartColors = [UIColor.red, UIColor.green, UIColor.blue, UIColor.cyan, UIColor.yellow, UIColor.magenta, UIColor.orange, UIColor.purple, UIColor.brown, UIColor.black, UIColor.darkGray, UIColor.lightGray, UIColor.white, UIColor.gray]
    }
    
    func chartHasData() -> Bool
    {
        var pass = false
        
        let cachesURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        let queryFileName : String! = "\(self.identifier!).plist"
        let queryDeffileURL = cachesURL?.appendingPathComponent(queryFileName)
        
        if(FileManager.default.fileExists(atPath: queryDeffileURL!.path))
        {
            pass = true;
        }
        
        return pass
    }

    func chartImageCacheURL() -> URL {
        
        let path : String = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        var url : URL = URL.init(fileURLWithPath: path)
        url = url.appendingPathComponent(String.init(format: "%@.png", self.identifier!))
        
        return url;
    }
    
    func clearChartImageCache()
    {
        let url = self.chartImageCacheURL()
        if(FileManager.default.fileExists(atPath: url.path))
        {
            do
            {
                try FileManager.default.removeItem(atPath: url.path)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        
    }
    
    func clearChartDataCache()
    {
        let cachesURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        let queryFileName : String! = "\(self.identifier!).plist"
        let queryDeffileURL = cachesURL?.appendingPathComponent(queryFileName)

        if(FileManager.default.fileExists(atPath: queryDeffileURL!.path))
        {
            do
            {
                try FileManager.default.removeItem(atPath: queryDeffileURL!.path)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        
    }
    
    func getChartData(chartSource : ChartDataSource?, loadCache : Bool, saveCache: Bool, completion : @escaping (_ successful : Bool, _ downloaded : Bool, _ dataArray :  [[String:Any]]?)->Void) {
        
        var chartArray : [[String:Any]]?
        
        let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
//        let documentQueryStagingFolderURL = documentsURL?.appendingPathComponent("QueryDefsFilesStaging")
        let queryFileName : String! = "\(self.identifier!).plist"
//        let queryDeffileURL = documentQueryStagingFolderURL?.appendingPathComponent(queryFileName)
        let queryDeffileURL = documentsURL?.appendingPathComponent(queryFileName)
        
        CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
            
            if(loadCache || !successful)
            {
                if(FileManager.default.fileExists(atPath: queryDeffileURL!.path))
                {
                    chartArray = NSKeyedUnarchiver.unarchiveObject(withFile: (queryDeffileURL?.path)!) as? [[String:Any]]
                }
            }
            
            if(chartArray == nil && successful && chartSource != nil && DashboardDataObject.shared.loggedIn)
            {
                let urlChart : URL = URL.init(string:self.chartQuery.getURL()!)! //URL.init(string: "\(DashboardDataObject.shared.baseURLString)\(chartSource.endpoint!)")!
                let endpointTask : URLSessionDataTask = DashboardDataObject.shared.session.dataTask(with: urlChart) { (endpointData, endpointResponse, endpointError) in
                    
                    chartArray = nil
                    do
                    {
                        if(endpointData != nil)
                        {
                            //                let stringVal = String.init(data: endpointData!, encoding: String.Encoding.utf8)
                            chartArray = try JSONSerialization.jsonObject(with: endpointData!, options: .allowFragments) as? [[String:Any]]
                            if(chartArray != nil && chartSource != nil)
                            {
                                print("getChartData - Count for \(chartSource!.name!) - \(chartArray!.count)")
                            }
                            //                print("Chart String Data -  \(chartSource.name) - \(stringVal)");
                            //                print("Chart Data -  \(chartSource.name) - \(chartArray)");
                        }
                    }
                    catch
                    {
                        print(error.localizedDescription)
                    }
                    
                    if let chartDataArray = chartArray
                    {
                        // Save the data to the staging folder
                        //                    var isDirExist : ObjCBool = false
                        //                    if(!FileManager.default.fileExists(atPath: (documentQueryStagingFolderURL?.path)!, isDirectory: &isDirExist))
                        //                    {
                        //                        // Create a new empty staging folder
                        //                        do
                        //                        {
                        //                            try FileManager.default.createDirectory(at: documentQueryStagingFolderURL!, withIntermediateDirectories: true, attributes: nil)
                        //                        }
                        //                        catch
                        //                        {
                        //                            print(error.localizedDescription)
                        //                        }
                        //                    }
                        
                        if(saveCache)
                        {
                            if(NSKeyedArchiver.archiveRootObject(chartDataArray, toFile: (queryDeffileURL?.path)!))
                            {
                                // This process completed successfully, process the next item or all is done
                                completion(true, true, chartDataArray)
                            }
                            else
                            {
                                completion(false, false, nil)
                            }
                        }
                        else
                        {
                            completion(true, true, chartDataArray)
                        }
                        
                    }
                    else
                    {
                        completion(false, false, nil)
                    }
                }
                endpointTask.resume()
            }
            else
            {
                completion(true, false, chartArray)
            }
            
        
    })
    }
}
