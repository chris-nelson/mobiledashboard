#import "MenuOverlaySegue.h"
#import "OverlayTransitioner.h"

@interface MenuOverlaySegue ()

@property (nonatomic, strong) id<UIViewControllerTransitioningDelegate> mytransitioningDelegate;

@end

@implementation MenuOverlaySegue

- (void)perform
{
    self.mytransitioningDelegate = [[OverlayTransitioningDelegate alloc] init];
    self.destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
    self.destinationViewController.transitioningDelegate = self.mytransitioningDelegate;
    
    [super perform];
}

@end

