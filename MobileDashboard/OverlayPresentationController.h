#import <UIKit/UIKit.h>

@interface OverlayPresentationController : UIPresentationController

@property (nonatomic) UIView *dimmingView;

@end
