//
//  CLMenuView.h
//
//  Created by Christopher Nelson on 2/15/16.
//  Copyright © 2016 Odeon Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLMenuEntryObject.h"

@class CLContextMenuView;

@protocol CLContextMenuViewDelegate <NSObject>

- (void) menuSetup:(CLContextMenuView*)contextMenuView;

@end

@interface CLContextMenuView : UIView

@property (nonatomic, assign) NSUInteger menuLabelWidth;
@property (nonatomic, assign) NSUInteger menuIconWidth;
@property (nonatomic, assign) NSUInteger menuHeight;

@property (nonatomic, assign) CGFloat startEndAnimationDuration;
@property (nonatomic, assign) CGFloat animationDuration;
@property (nonatomic, assign) CGFloat animationUnfoldDuration;

@property (nonatomic, assign) CGPoint poistionOverridePoint;

@property (nonatomic, assign) BOOL hideFirstAnimation;

@property (nonatomic, weak) id<CLContextMenuViewDelegate> delegate;

- (void) displayMenu;
- (void) hideMenu:(BOOL)animation;

- (void) addMenuEntry:(CLMenuEntryObject*)menuEntry;

@end
