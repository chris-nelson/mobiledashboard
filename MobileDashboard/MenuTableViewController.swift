//
//  MenuTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/1/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {

    var parentSection : CLMenuSectionObject?
    var sections : [CLMenuSectionObject] = []
    var menuTitle : String?
    
    deinit {
        
        print("deinit - MenuTableViewController")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        if(self.menuTitle == nil)
        {
            self.menuTitle = "Dashboard Menu"
        }
        
        self.title = self.menuTitle;
        
        if( self.parentSection == nil)
        {
            self.configureMenu()
        }
    }

    func configureMenu() {
        
        let section : CLMenuSectionObject = CLMenuSectionObject.init()
        var menu : CLMenuEntryObject = CLMenuEntryObject.init()
        
//        section.title = "Main Menu"
        self.sections.append(section)
        
//        menu.title = "Refresh Chart Data"
//        menu.executeMenuEntry = { (myview)->Void in
//            
//        }
//        section.menuEntries.add(menu)

        menu = CLMenuEntryObject.init()
        menu.title = "Settings"
        menu.executeMenuEntry = { (myview)->Void in
            
            self.presentingViewController?.dismiss(animated: true, completion: {
                
                //                self.performSegue(withIdentifier: "settingsSegue", sender: nil)
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                
                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "SettingsNav")
                DashboardRootViewController.rootViewController().present(vc, animated: true, completion: nil)
                
            })
        }
        section.menuEntries.add(menu)

        menu = CLMenuEntryObject.init()
        menu.title = "Log Out / Log In"
        menu.generatedTitle = { ()->String in
            
            var returnString = "Log In"
            var available : Bool = false;
            let networkCheck = CLNetworkStatusCheck.forInternetConnection()
            if( networkCheck!.currentNetworkStatusCheck() == CLNetworkStatus.init(1) || networkCheck!.currentNetworkStatusCheck() == CLNetworkStatus.init(2))
            {
                available = true;
            }

            if(available)
            {
                if(DashboardDataObject.shared.loggedIn)
                {
                    returnString = "Log Out"
                }
            }
            else
            {
                returnString = "Network Offline"
            }
            return returnString
        }
        menu.canDisplayMenuEntry = { ()->Bool in
            
            var available : Bool = false;
            let networkCheck = CLNetworkStatusCheck.forInternetConnection()
            if( networkCheck!.currentNetworkStatusCheck() == CLNetworkStatus.init(1) || networkCheck!.currentNetworkStatusCheck() == CLNetworkStatus.init(2))
            {
                available = true;
            }
            
            return available
        }
        menu.executeMenuEntry = { (myview)->Void in
            
            self.presentingViewController?.dismiss(animated: true, completion: {
                
                if(DashboardDataObject.shared.loggedIn)
                {
                    DashboardRootViewController.rootViewController().LogOut()
                }
                else
                {
                    DashboardRootViewController.rootViewController().autoLogIn(delayedLogin: true)
                }
                
            })
        }
        section.menuEntries.add(menu)

        menu = CLMenuEntryObject.init()
        menu.title = "About Mobile Dashboard"
        menu.executeMenuEntry = { (myview)->Void in
            
            self.presentingViewController?.dismiss(animated: true, completion: {
                
                //                self.performSegue(withIdentifier: "settingsSegue", sender: nil)
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                
                let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "AboutBoxNav")
                DashboardRootViewController.rootViewController().present(vc, animated: true, completion: nil)
                
            })
            
        }
        section.menuEntries.add(menu)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationItem.title = self.menuTitle
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        if(self.parentSection != nil)
        {
            return 1
        }
        
        return self.sections.count
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(self.parentSection != nil)
        {
            return 0.0
        }
        
        var height : CGFloat = 0.0
        
        let menuSection = self.sections[section]
        
        if let menuTitle = menuSection.title
        {
            if(menuTitle.lengthOfBytes(using: String.Encoding.utf8) > 0)
            {
                height = 20
            }
        }
            
        
        if let canDisplayMenu = menuSection.canDisplayMenuSection
        {
            if(!canDisplayMenu())
            {
                height = 0.0
            }
        }
        
        return height
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(self.parentSection != nil)
        {
            return nil
        }
        
        let menuSection = self.sections[section]
        var sectionTitle : String? = menuSection.title
        
        if let canDisplayMenu = menuSection.canDisplayMenuSection
        {
            if(!canDisplayMenu())
            {
                sectionTitle = nil
            }
        }
        
        return sectionTitle
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if(self.parentSection != nil)
        {
            let parentMenu : CLMenuEntryObject = self.parentSection!.menuEntries[0] as! CLMenuEntryObject
            return parentMenu.menuSubEntries.count
        }

        let menuSection = self.sections[section]
        
        if let canDisplayMenu = menuSection.canDisplayMenuSection
        {
            if(!canDisplayMenu())
            {
                return 0
            }
        }
        
        return menuSection.menuEntries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath)

        cell.accessoryType = UITableViewCellAccessoryType.none
        
        var menu : CLMenuEntryObject?
        var activeMenu = true
        
        if(self.parentSection != nil)
        {
            let parentMenu : CLMenuEntryObject = self.parentSection?.menuEntries[0] as! CLMenuEntryObject
            menu = parentMenu.menuSubEntries[indexPath.row] as? CLMenuEntryObject
        }
        else
        {
            let section : CLMenuSectionObject = self.sections[indexPath.section]
            menu = section.menuEntries[indexPath.row] as? CLMenuEntryObject
            
            if let canDisplayMenu = menu?.canDisplayMenuEntry
            {
                activeMenu = canDisplayMenu()
            }

            if((menu?.menuSubEntries.count)! > 0 && activeMenu)
            {
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            }
            
        }
        
        if (menu?.executeMenuEntry == nil || !activeMenu)
        {
            cell.textLabel?.textColor = UIColor.gray
        }
        else
        {
            cell.textLabel?.textColor = UIColor.black
        }
        
        // Configure the cell...
        cell.textLabel?.text = menu?.title

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var activeMenu = true
        var menu : CLMenuEntryObject?
        
        if(self.parentSection != nil)
        {
            let parentMenu : CLMenuEntryObject = self.parentSection?.menuEntries[0] as! CLMenuEntryObject
            menu = parentMenu.menuSubEntries[indexPath.row] as? CLMenuEntryObject
        }
        else
        {
            let section : CLMenuSectionObject = self.sections[indexPath.section]
            menu = section.menuEntries[indexPath.row] as? CLMenuEntryObject
            
            if let canDisplayMenu = menu?.canDisplayMenuEntry
            {
                activeMenu = canDisplayMenu()
            }
        }
        
        if(menu?.executeMenuEntry != nil && activeMenu)
        {
            menu?.executeMenuEntry(nil)
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
