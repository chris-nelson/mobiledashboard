//
//  ChartTypeCollectionViewCell.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/4/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var chartTypeNameLabel: UILabel!
    @IBOutlet weak var chartTypeImageView: UIImageView!
    
}
