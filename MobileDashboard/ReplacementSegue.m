//
//  ReplacementSegue.m
//  Dash-Ex
//
//  Created by Christopher Nelson on 12/15/15.
//  Copyright © 2015 Anderson. All rights reserved.
//

#import "ReplacementSegue.h"

@implementation ReplacementSegue

- (void)perform
{
    UIView* destView = self.destinationViewController.view;
    destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    destView.frame = CGRectMake(0, 0, self.sourceViewController.view.frame.size.width, self.sourceViewController.view.frame.size.height);

    [self swapFromViewController:self.previousViewController toViewController:self.destinationViewController completion:self.completion];
}

- (void)swapFromViewController:( UIViewController* _Nullable )fromViewController toViewController:(UIViewController*)toViewController completion:(void (^)(BOOL finished))completion
{
    [fromViewController willMoveToParentViewController:nil];
    
    [self.sourceViewController addChildViewController:toViewController];
    
    [self swapFromView:fromViewController.view toView:toViewController.view completion:^(BOOL finished) {
        
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self.sourceViewController];
        
        if(completion)
        {
            completion(finished);
        }
    }];
}

- (void)swapFromView:(UIView*)fromView toView:(UIView*)toView completion:(void (^)(BOOL finished))completion
{
    self.sourceViewController.view.clipsToBounds = YES;
    
    CGRect startToFrame;
    CGRect endToFrame;
    CGRect endFromFrame;
    
    endToFrame = CGRectMake(0, 0, self.sourceViewController.view.frame.size.width, self.sourceViewController.view.frame.size.height);
    
    toView.hidden = YES;
    [self.sourceViewController.view addSubview:toView];
    [self.sourceViewController.view bringSubviewToFront:toView];
    
    if(self.forwardDirection)
    {
        startToFrame = CGRectMake(self.sourceViewController.view.frame.size.width, 0, self.sourceViewController.view.frame.size.width, self.sourceViewController.view.frame.size.height);
        endFromFrame = CGRectMake(-self.sourceViewController.view.frame.size.width, 0, self.sourceViewController.view.frame.size.width, self.sourceViewController.view.frame.size.height);
    }
    else
    {
        startToFrame = CGRectMake(-self.sourceViewController.view.frame.size.width, 0, self.sourceViewController.view.frame.size.width, self.sourceViewController.view.frame.size.height);
        endFromFrame = CGRectMake(self.sourceViewController.view.frame.size.width, 0, self.sourceViewController.view.frame.size.width, self.sourceViewController.view.frame.size.height);
    }
    
    toView.frame = startToFrame;
    toView.hidden = NO;
    
    NSTimeInterval duration = 0.5;
    if(!self.animate)
        duration = 0;
    
    [UIView animateWithDuration:duration
                     animations:^
     {
         toView.frame = endToFrame;
         fromView.frame = endFromFrame;
     }
     completion:^(BOOL finished)
     {
         [fromView removeFromSuperview];
         fromView.hidden = YES;
         
         if(completion)
         {
             completion(finished);
         }
     }];
}

@end
