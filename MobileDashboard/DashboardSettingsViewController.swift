//
//  DashboardSettingsViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/8/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit
import LocalAuthentication

class DashboardSettingsViewController: UIViewController, PinEntryViewControllerDelegate {

    var usingTableView : Bool = true
    @IBOutlet weak var chartMenuTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var pinEntryButton: UIButton!
    @IBOutlet weak var useTouchIDSwitch: UISwitch!
    
    deinit {
        
        var foo = "abc"
        print("deinit - DashboardSettingsViewController")
        foo = "abcd"
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.usingTableView = DashboardSettingsObject.shared.useTableView
        
        self.chartMenuTypeSegmentedControl!.selectedSegmentIndex = DashboardSettingsObject.shared.useTableView ? 0 : 1
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if let userPIN = DashboardSettingsObject.shared.userPIN
        {
            if(userPIN.lengthOfBytes(using: String.Encoding.utf8) > 0)
            {
                self.pinEntryButton.setTitle("Change PIN", for: .normal)
                
                let context : LAContext = LAContext.init()
                // Set text for the localized fallback button.
                context.localizedFallbackTitle = "Enter PIN";
                let touchIDActive = context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil)
                if(touchIDActive)
                {
                    self.useTouchIDSwitch.isEnabled = true
                    self.useTouchIDSwitch.isOn = DashboardSettingsObject.shared.useTouchId
                }
                else
                {
                    self.useTouchIDSwitch.isOn = false
                    self.useTouchIDSwitch.isEnabled = false
                }
            }
            else
            {
                self.pinEntryButton.setTitle("Create PIN", for: .normal)
                self.useTouchIDSwitch.isOn = false
                self.useTouchIDSwitch.isEnabled = false
            }
        }
        else
        {
            self.pinEntryButton.setTitle("Create PIN", for: .normal)
            self.useTouchIDSwitch.isOn = false
            self.useTouchIDSwitch.isEnabled = false
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func chartMenuTypeSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        
        DashboardSettingsObject.shared.useTableView = (sender.selectedSegmentIndex == 0)
    }

    @IBAction func changeCreateButtonPressed(_ sender: UIButton) {
    
        if let userPIN = DashboardSettingsObject.shared.userPIN
        {
            if(userPIN.lengthOfBytes(using: String.Encoding.utf8) > 0)
            {
                self.performSegue(withIdentifier: "PINChangeSegue", sender: nil)
            }
            else
            {
                self.performSegue(withIdentifier: "PINCreateSegue", sender: nil)
            }
        }
        else
        {
            self.performSegue(withIdentifier: "PINCreateSegue", sender: nil)
        }

    }
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true) { 
            
            if( DashboardSettingsObject.shared.useTableView != self.usingTableView)
            {
                DashboardRootViewController.rootViewController().selectCurrentMenuScreen(completion: nil)
            }
            
        }
        
    }
    
    
    @IBAction func useTouchIDButtonPressed(_ sender: UISwitch) {
        
        DashboardSettingsObject.shared.useTouchId = self.useTouchIDSwitch.isOn
    }
    
    // MARK: - PinEntryViewControllerDelegate
    
    func pinEntrySuccessCode(pinCode : String, pinMode : CLPinMode)
    {
        self.dismiss(animated: true, completion: nil)
    }

    func pinEntryFailure(count : Int)
    {
        self.dismiss(animated: true, completion: nil)
    }

    func pinEntryCancel()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "PINCreateSegue")
        {
            let navVC = segue.destination as! UINavigationController
            let vc = navVC.topViewController as! PINEntryViewController
            vc.delegate = self
            vc.pinMode = CLPinMode.createPin
        }
        else if(segue.identifier == "PINChangeSegue")
        {
            let navVC = segue.destination as! UINavigationController
            let vc = navVC.topViewController as! PINEntryViewController
            vc.delegate = self
            vc.pinMode = CLPinMode.changePin
            vc.pinCodeToCheck = DashboardSettingsObject.shared.userPIN
        }
    }
    

}
