//
//  HudController.m
//  DashExMail
//
//  Created by Christopher Nelson on 9/21/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import "HudController.h"
#import "MBProgressHUD.h"

@interface HudController ()

@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation HudController

+ (instancetype)sharedHud
{
    static HudController* _sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{

        _sharedInstance = [[self alloc] init];
        
    });
    return _sharedInstance;
}

- (void) showHUD:(UIView*)view
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        
    });
}

- (void) showHUD:(UIView*)view title:(NSString*)title
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        self.hud.labelText = title;
    });
}

- (void) hideHUD:(UIView*)view
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [MBProgressHUD hideHUDForView:view animated:YES];
        self.hud.mode = MBProgressHUDModeIndeterminate;
        self.hud.labelText = nil;
        self.hud = nil;
    });
}

- (void) showHUDPercent:(UIView*)view title:(NSString*)title
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        
        self.hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
        self.hud.labelText = title;
    });
}

- (void) updateHUDPercent:(CGFloat)progress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.hud.progress = progress;
        
    });
}

@end
