//
//  CriteriaDateEntryViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/6/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class CriteriaDateEntryViewController: UIViewController, EntryFieldDelegate {

    @IBOutlet weak var textEntryLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    weak var chartDelegate : ChartConfigurationDelegate?
    var chartQueryParameterObject : ChartQueryParameterObject?
    
    var errorMessage : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textEntryLabel.text = self.chartQueryParameterObject?.fieldParameterName
        
        self.datePicker.maximumDate = Date.init()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func entryValid() -> Bool
    {
        var pass : Bool = true
        
        let dateFormat : DateFormatter = DateFormatter.init()
        dateFormat.dateFormat = "yyyy-MM-dd"
        self.chartQueryParameterObject?.fieldParameterValue = dateFormat.string(from: datePicker.date)

        if(datePicker.date > Date.init())
        {
            pass = false
            self.errorMessage = "Invalid date for \(self.chartQueryParameterObject!.fieldParameterName!)"
        }
        
        return pass
    }

    func displayErrorMessage ()
    {
        if(!self.entryValid())
        {
            let alertController : UIAlertController = UIAlertController.init(title: "Error", message: self.errorMessage, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
