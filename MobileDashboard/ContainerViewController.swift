//
//  ContainerViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/29/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

protocol ContainerViewControllerDelegate : class {
    
    func containerPrepare(for segue : UIStoryboardSegue, sender : Any?)
    func containerShouldPerformSegue(withIdentifier : String, sender : Any?) -> Bool
}

extension ContainerViewControllerDelegate {
    
    func containerPrepare(for segue : UIStoryboardSegue, sender : Any?){}
    func containerShouldPerformSegue(withIdentifier : String, sender : Any?) -> Bool{return true}
}

class ContainerViewController: UIViewController {

    var currentViewController : UIViewController?
    var forward : Bool = true
    var animate : Bool = true
    var transitionInProgress : Bool = false;
    weak var delegate : ContainerViewControllerDelegate?
    var initialSegueIdentifier : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let initSegue = self.initialSegueIdentifier
        {
            self.executeSegueWithIdentifier(identifier: initSegue, animate: false, forward: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func display(title: String, fromStoryboard: String, entryPoint: String?, animate : Bool, initialize: ((_ finished : Bool, _ viewController : UIViewController?)->Void)?, completion: ((_ finished : Bool, _ viewController : UIViewController?)->Void)?)
    {
        let storyboard = UIStoryboard.init(name: fromStoryboard, bundle: nil)
        
        var vc : UIViewController?
        
        if let entryPointName = entryPoint
        {
            vc = storyboard.instantiateViewController(withIdentifier: entryPointName)
        }
        else
        {
            vc = storyboard.instantiateInitialViewController()!
        }
        
        if(vc == nil)
        {
            if(initialize != nil)
            {
                initialize!(false, nil)
            }
            else if(completion != nil)
            {
                completion!(false, nil)
            }
        }
        else
        {
            if(initialize != nil)
            {
                initialize!(true, vc)
            }
            
            self.display(viewController: vc!, animate: animate, completion: completion)
        }
    }
    
    func display(viewController: UIViewController, animate : Bool, completion: ((_ finished : Bool, _ viewController : UIViewController)->Void)?)
    {
        self.swap(fromViewController: self.currentViewController, toViewController: viewController, animate: animate, completion: completion)
    }

    private func swap(fromViewController: UIViewController?, toViewController: UIViewController, animate : Bool, completion: ((_ finished : Bool, _ viewController : UIViewController)->Void)?)
    {
        let segue : ReplacementSegue = ReplacementSegue.init(identifier: "", source:self, destination:toViewController)
        self.currentViewController = fromViewController;
        self.animate = animate
        self.prepare(for: segue, sender: self)
        segue.perform()
        if (completion != nil)
        {
            completion!(true, toViewController);
        }
    }
    
    func executeSegueWithIdentifier(identifier : String, animate: Bool, forward : Bool) {
        
        if(self.transitionInProgress)
        {
            self.view.layer.removeAllAnimations()
        }

        self.forward = forward;
        self.animate = animate
        self.performSegue(withIdentifier: identifier, sender: self.currentViewController)
    }
    
    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        var pass : Bool = super.shouldPerformSegue(withIdentifier: identifier, sender: sender)
        
        if pass
        {
            if let thedelegate = self.delegate
            {
                pass = thedelegate.containerShouldPerformSegue(withIdentifier: identifier, sender: sender)
            }
        }
        
        return pass
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        let previousViewController = self.currentViewController
        self.currentViewController = nil
        
        if( segue is ReplacementSegue )
        {
            let replacementSegue : ReplacementSegue = segue as! ReplacementSegue
            replacementSegue.previousViewController = previousViewController
            replacementSegue.forwardDirection = self.forward
            replacementSegue.animate = self.animate
        }
        if let thedelegate = self.delegate
        {
            thedelegate.containerPrepare(for: segue, sender: sender)
        }

        // Return to default values in case the segue is called from other than executeSegueWithIdentifier
        self.forward = true
        self.animate = true
        
        self.currentViewController = segue.destination
        let destView : UIView = segue.destination.view
        destView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        destView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
    }

}
