//
//  CLColorPickerView.h
//  Pods
//
//  Created by Christopher Nelson on 9/6/15.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class CLColorPickerView;

@protocol CLColorPickerDelegate <NSObject>

@optional

- (void) cancelColorPicker:(CLColorPickerView*)colorPicker;
- (void) colorPickerView:(CLColorPickerView*)colorPicker selectedColorGrid:(UIColor*)color;
- (void) colorPickerView:(CLColorPickerView*)colorPicker selectedColorSelection:(UIColor*)color;
- (void) colorPickerView:(CLColorPickerView*)colorPicker didScrollToPage:(NSUInteger*)colorPage;



@end

@interface CLColorPickerView : UIView

- (void) generateColorGrid;
- (void) scrollToPage:(NSUInteger)page animated:(BOOL)animated;

@property (nonatomic, assign, readonly) NSUInteger currentPage;
@property (nonatomic, weak) id<CLColorPickerDelegate> delegate;
@property (nonatomic, strong) UIColor *selectedColor;
@property (nonatomic, assign) BOOL hideClearColorButton;
@end
