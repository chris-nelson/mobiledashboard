//
//  ChartPieDetailsViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 2/2/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartPieDetailsViewController: UIViewController, ColorSelectionDelegate {

    var chartDataObject : ChartDataObject?
    weak var chartDelegate : ChartConfigurationDelegate?

    @IBOutlet weak var drawPieChartHoleSwitch: UISwitch!
    @IBOutlet weak var pieChartHoleColorView: UIView!
    @IBOutlet weak var useHalfSizePieChartSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.drawPieChartHoleSwitch.isOn = self.chartDataObject!.displayPieChartWithHole
        self.pieChartHoleColorView.backgroundColor = self.chartDataObject?.pieChartHoleColor
        self.useHalfSizePieChartSwitch.isOn = self.chartDataObject!.displayHalfPieChart
        self.pieChartHoleColorView.layer.borderColor = UIColor.black.cgColor;
        self.pieChartHoleColorView.layer.borderWidth = 1;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func drawPieChartWithHoleSwitchValueChanged(_ sender: UISwitch) {
        
        self.chartDataObject!.displayPieChartWithHole = sender.isOn
    
    }
    
    @IBAction func useHalfSizePieChartSwitchValueChanged(_ sender: UISwitch) {
    
        self.chartDataObject!.displayHalfPieChart = sender.isOn

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "colorSelectionSegue")
        {
            let vc = segue.destination as! ColorSelectionViewController
            vc.enableSaveButton = true
            vc.enableCloseButton = true
            vc.hideClearColorButton = false
            vc.selectedColor = self.chartDataObject?.pieChartHoleColor
            
            vc.titleOverride = "Pie Chart Hole Color"
            vc.delegate = self
        }
    }

    // MARK: - ColorSelectionDelegate
    func saveColorSelection(_ colorSelectionViewController: ColorSelectionViewController!, color: UIColor!) {
        
        self.chartDataObject?.pieChartHoleColor = color
        self.pieChartHoleColorView.backgroundColor = self.chartDataObject?.pieChartHoleColor
        
        self.navigationController!.popViewController(animated: true)
    }
    
    func cancelColorSelection(_ colorSelectionViewController: ColorSelectionViewController!) {
        
        self.navigationController!.popViewController(animated: true)
    }
}
