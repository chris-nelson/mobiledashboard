//
//  DomainValueObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/20/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class DomainValueObject: NSObject, NSCoding {

    var sortGroup : Int = 0
    var domainValue : String?
    
    override init() {
        
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.domainValue = aDecoder.decodeObject(forKey: "domainValue") as? String ?? ""
        self.sortGroup = aDecoder.decodeObject(forKey: "sortGroup") as? Int ?? 0
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.domainValue, forKey: "domainValue")
        aCoder.encode(self.sortGroup, forKey: "sortGroup")
    }

    override var description: String {
        get {
            
            let desc = "\nName: \(self.domainValue ?? "- - -")  SortGroup: \(self.sortGroup)"
            
            return desc
        }
    }
}
