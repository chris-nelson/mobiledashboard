
#import <UIKit/UIKit.h>

@protocol CLPinViewDelegate;

typedef NS_ENUM(NSUInteger, CLPinMode) {
    CLPinModeReadOnlyPin,
    CLPinModeInputPin,
    CLPinModeCreatePin,
    CLPinModeVerifyPin,
    CLPinModeChangePin
} NS_ENUM_AVAILABLE(10_10,7_0);

typedef NS_ENUM(NSUInteger, CLKeychainMode) {
    CLKeychainModeNone,
    CLKeychainModeSave,
    CLKeychainModeSaveTouchID,
    CLKeychainModeSaveTouchIDPassword
} NS_ENUM_AVAILABLE(10_10,7_0);

@interface CLPinView : UIView

@property (nonatomic, assign) CLPinMode pinMode;
@property (nonatomic, assign) CLKeychainMode keychainMode;

//Pins image
@property (nonatomic, strong) UIImage *normalPinImage;
@property (nonatomic, strong) UIImage *selectedPinImage;

@property (nonatomic) BOOL secureMode;
@property (nonatomic) BOOL allowTouchID;

@property (nonatomic, assign) NSUInteger pinBorder;
@property (nonatomic, assign) NSUInteger numberOfPins;
@property (nonatomic, copy) UIColor* pinBorderColor;
@property (nonatomic, copy) UIColor* pinBackgroundColor;

@property (nonatomic, strong) NSString *pinCode;
@property (nonatomic, strong) NSString *pinCodeToCheck;

/////@property (nonatomic) BOOL shouldResetPinCode;

@property (nonatomic, readonly) NSUInteger failedAttempts;

@property (nonatomic, weak) id <CLPinViewDelegate> delegate;

//- (BOOL) saveStringToKeychain:(NSString*)pin keychainMode:(CLKeychainMode)keychainMode;
//- (NSString*) loadPinFromKeychain;
//- (void) clearPinFromKeychain;

- (void) displayKeyboard;
- (void) dismissKeyboard;

@end

@protocol CLPinViewDelegate <NSObject>
@optional

// Will be called when input pin code is entered
- (void)pinCodeViewController:(CLPinView *)pinView didInputPinCode:(NSString *)pinCode;

// Will be called when pin code created
- (void)pinCodeViewController:(CLPinView *)pinView didCreatePinCode:(NSString *)pinCode;

// Will be called when pinCodeToCheck isEqualTo: entered PinCode
- (void)pinCodeViewController:(CLPinView *)pinView didVerifiedPincodeSuccessfully:(NSString *)pinCode;

// Will be called when entered pin code don't match pinCodeToCheck;
- (void)pinCodeViewController:(CLPinView *)pinView didFailVerificationWithCount:(NSUInteger)failsCount;

// Will be called when pin code changed
- (void)pinCodeViewController:(CLPinView *)pinView didChangePinCode:(NSString *)pinCode;

- (void)pinCodeViewController:(CLPinView *)pinView didVerifyUsingTouchID:(BOOL)pass;

- (void)showChangeThePinMessage;
//- (void)showEnterYourPinMessage;
- (void)showPinsDontMatchMessage;
- (void)showPinCreatedMessage;
- (void)showEnterOnceAgainMessage;
- (void)showPinVerifiedMessage;
- (void)showPinsNoAvailableMessage;

@end
