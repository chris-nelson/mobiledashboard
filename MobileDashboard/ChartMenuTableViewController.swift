//
//  ChartMenuTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/30/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartMenuTableViewController: UITableViewController, ConfigurationDelegate {

    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var addingChart = false

    override func viewDidLoad() {
        super.viewDidLoad()

        DashboardDataObject.shared.loadChartPages(completion: { (successful) in
            
            DispatchQueue.main.async {
                
                self.refreshControl = nil
                self.enableRefreshControl(enable: true)
                self.tableView.reloadData()
            }
        })
    }

    func enableRefreshControl(enable : Bool)
    {
        if(enable)
        {
            if(self.refreshControl == nil)
            {
                let refreshControl = UIRefreshControl.init()
                refreshControl.addTarget(self, action: #selector(refreshControlRequested), for: .valueChanged)
                refreshControl.tintColor = UIColor.darkGray
                refreshControl.attributedTitle = NSAttributedString.init(string: "Refreshing . . .")
                self.refreshControl = refreshControl
                self.tableView!.addSubview(self.refreshControl!)
            }
        }
        else
        {
            self.refreshControl?.removeFromSuperview()
            self.refreshControl = nil
        }
    }
    
    func reloadCharts()
    {
        DispatchQueue.main.async {
            
            self.tableView.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateEditState()
    }
    
    func refreshControlRequested()
    {
        if (self.isEditing)
        {
            self.refreshControl!.endRefreshing()
        }
        else
        {
            if(DashboardDataObject.shared.loggedIn)
            {
                CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
                    
                    if(successful)
                    {
                        DispatchQueue.main.async {
                            DashboardDataObject.shared.clearChartPageImageCache()
                            DashboardDataObject.shared.clearChartPageDataCache()
                            self.tableView.reloadData()
                            
                            self.refreshControl!.endRefreshing()
                        }
                    }
                    else
                    {
                        let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to refresh charts while not connected to the network", preferredStyle: .alert)
                        let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: {
                            DispatchQueue.main.async {
                                
                                self.refreshControl!.endRefreshing()
                                
                            }
                        })
                    }
                })
            }
            else
            {
                let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to refresh charts while not logged in", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: {
                    DispatchQueue.main.async {
                        
                        self.refreshControl!.endRefreshing()
                        
                    }
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return DashboardDataObject.shared.chartPageCount()
    }

    func updateEditState(){
        
        if DashboardDataObject.shared.chartPageCount() == 0
        {
            self.editButton.title = ""
            self.editButton.isEnabled = false;
            self.menuButton.isEnabled = true
            self.enableRefreshControl(enable: false)

        }
        else
        {
            self.editButton.isEnabled = true;
            if(self.tableView.isEditing)
            {
                self.editButton.title = "Done"
                self.enableRefreshControl(enable: false)
                self.menuButton.isEnabled = false
            }
            else
            {
                self.editButton.title = "Edit"
                self.menuButton.isEnabled = true
                self.enableRefreshControl(enable: true)
                self.tableView.reloadData()
            }
        }
    }
        
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChartCell", for: indexPath) as! ChartMenuTableViewCell

        let selectedChartPage : ChartPageDataObject? = DashboardDataObject.shared.chartPage(atIndex:indexPath.row)
        
        if let chartPage = selectedChartPage
        {
            let url = chartPage.chartImageCacheURL()
            
            var pass = true
            if cell.chartImageView.image == nil || !chartPage.chartsHaveData()
            {
                if(FileManager.default.fileExists(atPath: url.path))
                {
                    if let imageData = try? Data(contentsOf: url)
                    {
                        cell.chartImageView.image = UIImage.init(data: imageData)
                    }
                    else
                    {
                        pass = false
                        chartPage.deleteCacheImage()
                        chartPage.deleteChartFiles()
                    }
                }
                
                //
                if(!chartPage.chartsHaveData() && !chartPage.dataDownloadFailed)
                {
                    pass = false
                    chartPage.refreshCharts(completion: { (successful, downlaoded, dataArray) in
                        DispatchQueue.main.async {
                            print("Chart \(chartPage.name!) Completed Download")
                            
                            if(!chartPage.processingData)
                            {
                                DashboardRootViewController.previewForChartPage(chartPageDataObject: chartPage, iPhoneSize : false, completion: { (successful : Bool) in
                                    
                                    DispatchQueue.main.async {
                                        print("Chart \(chartPage.name!) Completed Thumbnail")
                                        tableView.reloadData()
                                    }
                                })
                            }
                        }
                    })
                }
                else if(!chartPage.dataDownloadFailed)
                {
                    if(!chartPage.processingData && cell.chartImageView.image == nil)
                    {
                        pass = false
                        DashboardRootViewController.previewForChartPage(chartPageDataObject: chartPage, iPhoneSize : false, completion: { (successful : Bool) in
                            
                            DispatchQueue.main.async {
                                print("Chart \(chartPage.name!) Completed Thumbnail")
                                tableView.reloadData()
                            }
                        })
                    }
                }
            }
            
            if(pass)
            {
                cell.chartImageView.isHidden = false
                cell.activityIndicatorView.stopAnimating()
                cell.activityIndicatorView.isHidden = true
            }
            else
            {
                cell.chartImageView.isHidden = true
                cell.activityIndicatorView.startAnimating()
                cell.activityIndicatorView.isHidden = false
            }

                //
        
            
            /*if cell.chartImageView.image == nil  && cell.tag == 0
            {
                cell.tag = 1
                cell.chartImageView.isHidden = true
                cell.activityIndicatorView.isHidden = false
                cell.activityIndicatorView.startAnimating()
                /*DashboardRootViewController.previewForChartPage(chartPageDataObject: chartPage, iPhoneSize : false, completion: { (successful : Bool) in
                    
                    tableView.reloadData()
                    
                })*/
            }
            else if cell.chartImageView.image != nil// && cell.tag == 1
            {
                cell.tag = 0
                cell.activityIndicatorView.stopAnimating()
                cell.activityIndicatorView.isHidden = true;
                cell.chartImageView.isHidden = false
            }*/
            
            
            
            cell.chartTextLabel.text = String.init(format: "%d - %@", arguments: [indexPath.row, chartPage.name!])
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if DashboardDataObject.shared.chartPageCount() > 0
        {
            return 0
        }
        
        return 100
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 200))

        label.text = "Please Select Add to Create a New Chart"
        label.textAlignment = .center
        
        return label;
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let selectedChartPage : ChartPageDataObject? = DashboardDataObject.shared.chartPage(atIndex: indexPath.row)
            
            if let chartPage = selectedChartPage
            {
                if(DashboardDataObject.shared.removeChartPage(atIndex: indexPath.item))
                {
                    chartPage.deleteCacheImage()
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    
                    if (DashboardDataObject.shared.chartPageCount() == 0)
                    {
                        self.tableView.isEditing = false
                        self.tableView.reloadData()
                    }
                    self.updateEditState()
                }
            }
        }
    }

    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

        DashboardDataObject.shared.swapChartPagePosition(atSourceIndex: fromIndexPath.item, forDestinationIndex: to.item)
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }

    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
            
            if(successful)
            {
                if(DashboardDataObject.shared.isConfigurationDataAvailable())
                {
                    self.tableView.setEditing(false, animated: true)
                    self.updateEditState()
                    self.performSegue(withIdentifier: "addChartSegue", sender: self)
                }
                else
                {
                    let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Configuration data is unavailable please log out and log in again", preferredStyle: .alert)
                    let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to add charts while offline", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }

    @IBAction func addEditButtonPressed(_ sender: UIBarButtonItem)
    {
        if DashboardDataObject.shared.chartPageCount() > 0
        {
            self.tableView.setEditing(!self.tableView.isEditing, animated: true)
        }
        else
        {
            self.tableView.setEditing(false, animated: true)
        }

        self.updateEditState()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "chartTableSegue") {
            
            let vc = segue.destination as! FourWayViewController
            vc.screenLayout = .OneByOne1
            let indexPath = self.tableView?.indexPath(for: sender as! UITableViewCell)
            vc.chartPageObject = DashboardDataObject.shared.chartPage(atIndex: indexPath!.row)
            if(vc.chartPageObject!.chartCount() > 2)
            {
                vc.screenLayout = .TwoByTwo
            }
            else if(vc.chartPageObject!.chartCount() > 1)
            {
                vc.screenLayout = .TwoByOne
            }
        }
        else if(segue.identifier == "addChartSegue") {
            
            let navCtrl : UINavigationController = segue.destination as! UINavigationController
            let chartConfigVC = navCtrl.topViewController as! ChartConfigurationViewController
            chartConfigVC.chartConfigurationDelegate = self
            self.addingChart = true
        }
    }

    func chartConfigurationDismissedSaved()
    {
        self.dismiss(animated: true) {
            
            self.tableView?.reloadData()
            if(self.addingChart)
            {
                let item = DashboardDataObject.shared.chartPageCount() - 1
                if(item > 0)
                {
                    let indexPath = IndexPath.init(item: item, section: 0)
                    self.tableView?.selectRow(at: indexPath, animated: true, scrollPosition: .top)
                }
            }
            self.addingChart = false

        }
    }
    
    func chartConfigurationDismissedCancelled()
    {
        self.dismiss(animated: true, completion: nil)
        self.addingChart = false
    }
}
