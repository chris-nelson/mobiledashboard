//
//  ChartQueryCriteriaObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/4/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartQueryCriteriaObject: NSObject, NSCoding {
    
    var dataSourceIdentifier : Int = 0
    var chartQueryParamArray : [ChartQueryParameterObject] = []
    var name : String?
    
    func reset() {
        
        self.dataSourceIdentifier = 0
        chartQueryParamArray = []
    }
    
    override init()
    {
        super.init()
        
        self.dataSourceIdentifier = 0
        self.chartQueryParamArray = []
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.dataSourceIdentifier = aDecoder.decodeInteger(forKey: "dataSourceIdentifier")
        self.chartQueryParamArray = aDecoder.decodeObject(forKey: "chartQueryParamArray") as? [ChartQueryParameterObject] ?? []
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.dataSourceIdentifier, forKey: "dataSourceIdentifier")
        aCoder.encode(self.chartQueryParamArray, forKey: "chartQueryParamArray")

    }

    func getURL() -> String?
    {
        var goodURL : String?
        var url : String? = nil
        var cnt : Int = 0
        let chartDataSource = DashboardDataObject.shared.chartDataSources[(self.dataSourceIdentifier)]

        while (self.chartQueryParamArray.count > cnt)
        {
            let chartQueryParameterObject : ChartQueryParameterObject = self.chartQueryParamArray[cnt]
            
            if let urlString = url
            {
                url = urlString + "&\(chartQueryParameterObject.fieldParameterName!)=\(chartQueryParameterObject.fieldParameterValue!)"
            }
            else
            {
                url = "?\(chartQueryParameterObject.fieldParameterName!)=\(chartQueryParameterObject.fieldParameterValue!)"
            }
            
            cnt = cnt + 1
        }
        if let urlString = url
        {
            goodURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            goodURL = DashboardDataObject.shared.baseURLString + (chartDataSource!.endpoint)! + goodURL!
        }
        else
        {
            goodURL = DashboardDataObject.shared.baseURLString + (chartDataSource!.endpoint)!
        }
        
        return goodURL
    }
}
