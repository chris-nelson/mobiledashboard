//
//  ChartMenuCollectionViewCell.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/30/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

protocol ChartDeleteDelegate : class {
    
    func chartDeleteButtonPressed(cell : ChartMenuCollectionViewCell)
}

class ChartMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    weak var delegate : ChartDeleteDelegate?

    let animationRotateDegres: CGFloat = 0.5
    let animationTranslateX: CGFloat = 1.0
    let animationTranslateY: CGFloat = 1.0
    let count: Int = 1
    var jiggling = false
    var even = false
    
    override func prepareForReuse() {
        
        self.imageView.image = nil;
        self.activityIndicatorView.stopAnimating()
        self.activityIndicatorView.isHidden = true;
//        self.deleteButton.isHidden = true;
        self.layer.removeAllAnimations()
        self.jiggling = false
    }
    
    func startJiggling() {
        
        if(!jiggling)
        {
            jiggling = true
            
            /*let transformAnim  = CAKeyframeAnimation(keyPath:"transform")
            transformAnim.values  = [NSValue(caTransform3D: CATransform3DMakeRotation(0.03, 0.0, 0.0, 1.0)),NSValue(caTransform3D: CATransform3DMakeRotation(-0.03 , 0, 0, 1))]
            transformAnim.autoreverses = true
            transformAnim.duration  =  (even) ? 0.115 : 0.105
            transformAnim.repeatCount = Float.infinity
            self.layer.add(transformAnim, forKey: "transform")
            */
            self.deleteButton.isHidden = false;
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        
        if(self.delegate != nil)
        {
            self.delegate!.chartDeleteButtonPressed(cell: self)
        }
    }
}
