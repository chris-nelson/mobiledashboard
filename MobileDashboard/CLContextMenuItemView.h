//
//  MenuItemView.h
//  TestMenu
//
//  Created by Christopher Nelson on 2/24/16.
//  Copyright © 2016 Odeon Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLMenuEntryObject.h"
#import "CLContextMenuView.h"

@protocol CLContextMenuItemDelegate <NSObject>

- (void) hideMenu:(BOOL)animation;

@end

@interface CLContextMenuItemView : UIView

@property (nonatomic, strong) UIImageView* menuImageView;
@property (nonatomic, strong) UILabel* menuLabel;
@property (nonatomic, strong) UIView* blurView;

@property (nonatomic, strong) CLMenuEntryObject* menuEntry;

@property (nonatomic, weak) id<CLContextMenuItemDelegate> delegate;

+ (UIView*) createDimmingView;

@end
