//
//  ChartQueryParameterObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/4/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartQueryParameterObject: NSObject, NSCoding {

    var fieldParameterIdentifier : String?
    var fieldParameterName : String?
    var fieldParameterValue : String?

    override init() {
    
        super.init()
    
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.fieldParameterIdentifier = aDecoder.decodeObject(forKey: "fieldParameterIdentifier") as? String ?? ""
        self.fieldParameterName = aDecoder.decodeObject(forKey: "fieldParameterName") as? String ?? ""
        self.fieldParameterValue = aDecoder.decodeObject(forKey: "fieldParameterValue") as? String ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.fieldParameterIdentifier, forKey: "fieldParameterIdentifier")
        aCoder.encode(self.fieldParameterName, forKey: "fieldParameterName")
        aCoder.encode(self.fieldParameterValue, forKey: "fieldParameterValue")
    }

}
