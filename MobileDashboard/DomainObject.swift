//
//  DomainObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/20/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class DomainObject: NSObject, NSCoding {

    var name : String = ""
    var endpoint : String = ""
    var identifier : Int = 0
    var domainValues : [DomainValueObject] = []
    
    override init() {
        
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.endpoint = aDecoder.decodeObject(forKey: "endpoint") as? String ?? ""
        self.identifier = aDecoder.decodeObject(forKey: "identifier") as? Int ?? 0
        self.domainValues = aDecoder.decodeObject(forKey: "domainValues") as? [DomainValueObject] ?? []
    
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.endpoint, forKey: "endPoint")
        aCoder.encode(self.identifier, forKey: "identifier")
        aCoder.encode(self.domainValues, forKey: "domainValues")
    
    }
    
    override var description: String {
        get {
            let desc = "\nName: \(self.name)\nEndPoint: \(self.endpoint)\nIdentifier: \(self.identifier)\nValues: \(self.domainValues)"
            
            return desc
        }
    }

}
