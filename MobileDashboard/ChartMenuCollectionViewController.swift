//
//  ChartMenuCollectionViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/30/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ChartMenuCell"

class ChartMenuCollectionViewController: UICollectionViewController, CLContextMenuViewDelegate, UICollectionViewDelegateFlowLayout, ChartDeleteDelegate, UIGestureRecognizerDelegate, ConfigurationDelegate {

    let preferredWidthHeight : Int = 225
    var contextMenuView : CLContextMenuView?
    var deleteButton : UIButton?
    var animatingCell : Bool = false
    var addingChart = false
    
    var processingCellsCount = 0
    
    var genratingCounter = 0

    @IBOutlet weak var menuButton: UIBarButtonItem!
    let headerViewIdentifier = "Test Header View";
    let footerViewIdentifier = "Test Footer View";

    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    weak var refreshControl : UIRefreshControl?

    deinit {
        
        print("deinit - ChartMenuCollectionViewController")
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.contextMenuView = CLContextMenuView.init()
        self.contextMenuView!.frame = CGRect(x: 0, y: 0, width: 55, height: 55)
        self.view.addSubview(contextMenuView!)
        self.view.bringSubview(toFront: self.contextMenuView!)
        
        // Enable moving the chart to a different location
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer.init(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.3; // To detect after how many seconds you want shake the cells
        lpgr.delegate = self;
        lpgr.delaysTouchesBegan = true;
        self.collectionView!.addGestureRecognizer(lpgr)
        
        DashboardDataObject.shared.loadChartPages(completion: { (successful) in
            
            DispatchQueue.main.async {

                self.enableRefreshControl(enable: true)
                self.collectionView?.reloadData()
                
            }
        })
    }
    
    func reloadCharts()
    {
        DispatchQueue.main.async {
            
            self.collectionView!.reloadData()
        }
    }
    

    func enableRefreshControl(enable : Bool)
    {
        if(enable)
        {
            if(self.refreshControl == nil)
            {
                let refreshControl = UIRefreshControl.init()
                refreshControl.addTarget(self, action: #selector(refreshControlRequested), for: .valueChanged)
                refreshControl.tintColor = UIColor.darkGray
                refreshControl.attributedTitle = NSAttributedString.init(string: "Refreshing . . .")
                self.refreshControl = refreshControl
                self.collectionView!.addSubview(self.refreshControl!)
            }
        }
        else
        {
            self.refreshControl?.removeFromSuperview()
            self.refreshControl = nil
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.updateEditState()
    }

    func refreshControlRequested()
    {
        if (self.isEditing)
        {
            self.refreshControl!.endRefreshing()
        }
        else
        {
            if(DashboardDataObject.shared.loggedIn)
            {
                CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
                    
                    if(successful)
                    {
                        DispatchQueue.main.async {
                            DashboardDataObject.shared.clearChartPageImageCache()
                            DashboardDataObject.shared.clearChartPageDataCache()
                            self.collectionView?.reloadData()
                            
                            self.refreshControl!.endRefreshing()
                        }
                    }
                    else
                    {
                        let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to refresh charts while not connected to the network", preferredStyle: .alert)
                        let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: {
                            DispatchQueue.main.async {
                                
                                self.refreshControl!.endRefreshing()
                                
                            }
                        })
                    }
                })
            }
            else
            {
                let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to refresh charts while not logged in", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: {
                    DispatchQueue.main.async {
                        
                        self.refreshControl!.endRefreshing()
                        
                    }
                })
            }
        }
    }

    func handleLongPress(gesture : UILongPressGestureRecognizer) {
        
        if(self.isEditing)
        {
            if(gesture.state == .began)
            {
                let selectedIndexPath : IndexPath? = self.collectionView?.indexPathForItem(at: gesture.location(in: self.collectionView))
                if(selectedIndexPath != nil)
                {
                    self.collectionView?.beginInteractiveMovementForItem(at: selectedIndexPath!)
                }
            }
            else if(gesture.state == .changed)
            {
                self.collectionView?.updateInteractiveMovementTargetPosition(gesture.location(in: self.collectionView))
            }
            else if(gesture.state == .ended)
            {
                self.collectionView?.endInteractiveMovement()
            }
            else
            {
                self.collectionView?.cancelInteractiveMovement()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        DashboardDataObject.shared.swapChartPagePosition(atSourceIndex: sourceIndexPath.item, forDestinationIndex: destinationIndexPath.item)

    }
    
    func chartDeleteButtonPressed(cell : ChartMenuCollectionViewCell)
    {
        let indexPath : IndexPath = (self.collectionView?.indexPath(for: cell))!
        let chartPage : ChartPageDataObject? = DashboardDataObject.shared.chartPage(atIndex:indexPath.item)
        
        if let deleteChartPage = chartPage
        {
            let alertController : UIAlertController = UIAlertController.init(title: "Delete Chart", message: "Are you sure you wish to delete the chart named '\(deleteChartPage.name!)'?", preferredStyle: .alert)
            let alertYes = UIAlertAction.init(title: "Yes", style: .default, handler: { (action) in
                
                DispatchQueue.main.async {
                    
                    if(DashboardDataObject.shared.removeChartPage(atIndex: indexPath.item))
                    {
                        deleteChartPage.deleteCacheImage()
                        deleteChartPage.deleteFile();
                        self.collectionView?.deleteItems(at: [indexPath])
                        if(DashboardDataObject.shared.chartPageCount() == 0)
                        {
                            self.isEditing = false
                        }
                    }
                    self.updateEditState()
                    
                }
            })
            let alertNo = UIAlertAction.init(title: "No", style: .default, handler:nil)
            alertController.addAction(alertYes)
            alertController.addAction(alertNo)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        DashboardDataObject.shared.clearAllChartPagesDownloadFailedStatus(clearImage: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        var shouldSegue = true
        if(self.isEditing)
        {
            shouldSegue = false
        }
        
        return shouldSegue;
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "chartGridSegue") {

            let vc = segue.destination as! FourWayViewController
            vc.screenLayout = .OneByOne1
            let indexPath = self.collectionView?.indexPath(for: sender as! UICollectionViewCell)
            
            vc.chartPageObject = DashboardDataObject.shared.chartPage(atIndex:indexPath!.row)
            if(vc.chartPageObject!.chartCount() > 2)
            {
                vc.screenLayout = .TwoByTwo
            }
            else if(vc.chartPageObject!.chartCount() > 1)
            {
                vc.screenLayout = .TwoByOne
            }
            
//            if segue is ReplacementSegue
//            {
//                let replacementSegue = segue as! ReplacementSegue
//                replacementSegue.completion = { (success)->Void in
//
//                    print ("Done")
//                }
//            }
        }
        else if(segue.identifier == "addChartSegue") {
            
            let navCtrl : UINavigationController = segue.destination as! UINavigationController
            let chartConfigVC = navCtrl.topViewController as! ChartConfigurationViewController
            chartConfigVC.chartConfigurationDelegate = self
            self.addingChart = true
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return DashboardDataObject.shared.chartPageCount()
    }
    
    @IBAction func editButtonPressed(_ sender: UIBarButtonItem) {
        
        // Edit mode
        if DashboardDataObject.shared.chartPageCount() > 0
        {
            self.setEditing(!self.isEditing, animated: true)
        }
        else
        {
            self.setEditing(false, animated: true)
            self.updateEditState()
            
        }
        self.updateEditState()
    }
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {

        CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
            
            if(successful)
            {
                if(DashboardDataObject.shared.isConfigurationDataAvailable())
                {
                    self.setEditing(false, animated: true)
                    self.updateEditState()
                    
                    self.performSegue(withIdentifier: "addChartSegue", sender: self)
                }
                else
                {
                    let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Configuration data is unavailable please log out and log in again", preferredStyle: .alert)
                    let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to add charts while offline", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func updateEditState(){
        
        if DashboardDataObject.shared.chartPageCount() == 0
        {
            self.editButton.title = ""
            self.editButton.isEnabled = false
            self.editButton.isEnabled = true

            self.menuButton.isEnabled = true
            self.enableRefreshControl(enable: false)
        }
        else
        {
            if(self.isEditing)
            {
                self.editButton.title = "Done"
                self.editButton.isEnabled = true
                
//                self.addButton.title = "Add"
                self.addButton.isEnabled = true

                self.menuButton.isEnabled = false
                self.enableRefreshControl(enable: false)
            }
            else
            {
                self.editButton.title = "Edit"
                self.menuButton.isEnabled = true
                
//                self.addButton.title = "Add"
                self.addButton.isEnabled = true
                
                self.enableRefreshControl(enable: true)
            }
            self.collectionView?.reloadData()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)) as! ChartMenuCollectionViewCell

        let selectedChartPage : ChartPageDataObject? = DashboardDataObject.shared.chartPage(atIndex: indexPath.item)
        
        if let chartPage = selectedChartPage
        {
            let url = chartPage.chartImageCacheURL()
            
            var pass = true
            if cell.imageView.image == nil || !chartPage.chartsHaveData()
            {
                if(FileManager.default.fileExists(atPath: url.path))
                {
                    if let imageData = try? Data(contentsOf: url)
                    {
                        cell.imageView.image = UIImage.init(data: imageData)
                    }
                    else
                    {
                        pass = false
                        chartPage.deleteCacheImage()
                        chartPage.deleteChartFiles()
                    }
                }
                
                if(!chartPage.chartsHaveData() && !chartPage.dataDownloadFailed)
                {
                    pass = false
                    chartPage.refreshCharts(completion: { (successful, downlaoded, dataArray) in
                        DispatchQueue.main.async {
                            print("Chart \(chartPage.name!) Completed Download")
                            
                            if(!chartPage.processingData)
                            {
                                DashboardRootViewController.previewForChartPage(chartPageDataObject: chartPage, iPhoneSize : false, completion: { (successful : Bool) in
                                    
                                    DispatchQueue.main.async {
                                        print("Chart \(chartPage.name!) Completed Thumbnail")
                                        collectionView.reloadData()
                                    }
                                })
                            }
                        }
                    })
                }
                else if(!chartPage.dataDownloadFailed)
                {
                    if(!chartPage.processingData && cell.imageView.image == nil)
                    {
                        pass = false
                        DashboardRootViewController.previewForChartPage(chartPageDataObject: chartPage, iPhoneSize : false, completion: { (successful : Bool) in
                            
                            DispatchQueue.main.async {
                                print("Chart \(chartPage.name!) Completed Thumbnail")
                                collectionView.reloadData()
                            }
                        })
                    }
                }
            }
            
            if(pass)
            {
                cell.imageView.isHidden = false
                cell.activityIndicatorView.stopAnimating()
                cell.activityIndicatorView.isHidden = true
            }
            else
            {
                cell.imageView.isHidden = true
                cell.activityIndicatorView.startAnimating()
                cell.activityIndicatorView.isHidden = false
            }
            
            cell.delegate = self
            cell.even = ((Double(indexPath.item).truncatingRemainder(dividingBy: 2.0)) == 0)
            /*if cell.imageView.image == nil// && cell.tag == 0
            {
                
//                if(cell.tag == 0)
//                {
//                    cell.tag = 1
//                    cell.imageView.isHidden = true
//                    cell.activityIndicatorView.startAnimating()
//                    cell.activityIndicatorView.isHidden = false
//                }
//                
//                if(chartPage.chartsHaveData())
//                {
//                    print("Getting Chart \(chartPage.name!)")
//                    DashboardRootViewController.previewForChartPage(chartPageDataObject: chartPage, iPhoneSize : false, completion: { (successful : Bool) in
//                        
//                        collectionView.reloadData()
//                        
//                    })
//                }
//                else
//                {
//                    collectionView.reloadData()
//                }

 
                if(cell.tag == 0)
                {
                    cell.tag = indexPath.item
                    chartPage.chartItem = indexPath.item
                    cell.imageView.isHidden = true
                    cell.activityIndicatorView.startAnimating()
                    cell.activityIndicatorView.isHidden = false

                    print("Getting Chart \(chartPage.name!)")
                    DashboardRootViewController.previewForChartPage(chartPageDataObject: chartPage, iPhoneSize : false, delegate: cell, completion: { (successful : Bool) in
                
                        collectionView.reloadData()
                    
                    })
                }
            
                
            }
            else if cell.imageView.image != nil //&& cell.tag == 1
            {
                cell.tag = 0
                cell.activityIndicatorView.stopAnimating()
                cell.activityIndicatorView.isHidden = true;
                cell.imageView.isHidden = false
            }*/
            
            cell.textLabel.text = String.init(format: "%d - %@", arguments: [indexPath.item, chartPage.name!])
            
            if(self.isEditing)
            {
                cell.startJiggling()
            }
            else
            {
                cell.deleteButton.isHidden = true;
            }
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if DashboardDataObject.shared.chartPageCount() == 0
        {
            return CGSize(width: collectionView.frame.width, height: 50)
        }
        
        return CGSize(width: collectionView.frame.width, height: 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var identifier : String?
        
        if kind == UICollectionElementKindSectionHeader
        {
            identifier = headerViewIdentifier;
        }
        else if kind == UICollectionElementKindSectionFooter
        {
            identifier = footerViewIdentifier;
        }
        
        let supplementaryView : UICollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: identifier!, for: indexPath)

        return supplementaryView
    }
    /*
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
    }*/
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        //    NSLog(@" CollectionView width %f", collectionView.frame.size.width);
        var count : Int = Int(Int(collectionView.frame.size.width) / Int(preferredWidthHeight));
        let mod : Int = Int(Int(collectionView.frame.size.width) % Int(preferredWidthHeight));
        if(mod > 10)
        {
            count += 1;
        }
        let newWidth : Int = Int((Int(collectionView.frame.size.width) / count) - 10);
        
        return CGSize(width:newWidth, height:newWidth);
    }

    @IBAction func addChartButtonPressed(_ sender: UIBarButtonItem) {
    
        self.contextMenuView!.poistionOverridePoint = CGPoint(x: self.view.frame.size.width - 60, y:0);
        self.contextMenuView!.delegate = self;
        self.contextMenuView!.displayMenu()
    }
    
    
    func menuSetup ( _ menuView : CLContextMenuView)
    {
        menuView.frame = CGRect(x: self.view.frame.width - 55, y: 0, width: 55, height: 55)
        menuView.menuLabelWidth = 125;
        menuView.menuHeight = 55;
        menuView.menuIconWidth = 55;
        
        menuView.startEndAnimationDuration = 0.25;
        menuView.animationUnfoldDuration = 0.05;
        menuView.animationDuration = 0.25;
        
        menuView.hideFirstAnimation = true;
        
        var menuEntry = CLMenuEntryObject.init()
        menuEntry.title = "Menu One";
        menuEntry.image = UIImage.init(named: "ShopComContextMenu");
        
        menuEntry.test = { (myBoolVal)->Bool in
            
            return false
        }
        
        menuEntry.canDisplayMenuEntry = { ()->Bool in

            return true
        
        }
        menuEntry.executeMenuEntry = { (myview)->Void in
            

        }
        menuView.addMenuEntry(menuEntry)
        

        menuEntry = CLMenuEntryObject.init()
        menuEntry.title = "Menu Two";
        menuEntry.image = UIImage.init(named: "ShopComContextMenu");
        menuEntry.canDisplayMenuEntry = (() -> Bool)! {
            
            return true
         
        }
        menuEntry.executeMenuEntry = { (myview) -> Void in
            
        }
        menuView.addMenuEntry(menuEntry)
        
        menuEntry = CLMenuEntryObject.init()
        menuEntry.title = "Menu Three";
        menuEntry.image = UIImage.init(named: "ShopComContextMenu");
        menuEntry.canDisplayMenuEntry = (() -> Bool)! {
            
            return true
            
        }
        menuEntry.executeMenuEntry = { (myview) -> Void in
            
        }
        menuView.addMenuEntry(menuEntry)
        
    }
    
    func chartConfigurationDismissedSaved()
    {
        self.dismiss(animated: true) { 
            
            self.collectionView?.reloadData()
            if(self.addingChart)
            {
                let item = DashboardDataObject.shared.chartPageCount() - 1
                if(item > 0)
                {
                    let indexPath = IndexPath.init(item: item, section: 0)
                    self.collectionView?.selectItem(at: indexPath, animated: true, scrollPosition: .top)
                }
            }
            self.addingChart = false
            self.updateEditState()
        }
    }
    
    func chartConfigurationDismissedCancelled()
    {
        self.dismiss(animated: true) {
            
            self.addingChart = false
            self.updateEditState()

        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        self.contextMenuView?.hideMenu(false)

        super.viewWillTransition(to: size, with: coordinator)
    }
}
