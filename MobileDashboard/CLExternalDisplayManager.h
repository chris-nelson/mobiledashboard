//
//  CLExternalDisplayManager.h
//  OdeonCoreLibrary
//
//  Created by Christopher Nelson on 10/5/14.
//  Copyright (c) 2014 Odeon Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CLExternalDisplayManagerDelegate <NSObject>

- (void) externalDisplayDidChange;

@end

@interface CLExternalDisplayManager : NSObject

+ (CLExternalDisplayManager *)sharedInstance;

@property (nonatomic, strong, readonly) NSArray* availableModes;
@property (nonatomic, weak) id <CLExternalDisplayManagerDelegate> delegate;

@property (nonatomic) int selectedScreenMode;
@property (nonatomic, strong) UIView* externalView;

-(void) updateAvailableModes;

@end
