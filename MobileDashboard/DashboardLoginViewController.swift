//
//  DashboardLoginViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/7/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class DashboardLoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var networkStatusLabel: UILabel!
    
    var doNetworkCheck = true

    deinit {
        
        print("deinit - DashboardLoginViewController")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
        {
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }

        if let userName = DashboardSettingsObject.shared.userName
        {
            self.userNameTextField.text = userName
            self.passwordTextField.becomeFirstResponder()
        }
        else
        {
            self.userNameTextField.becomeFirstResponder()
            
        }
        
        self.userNameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.networkCheck()
    }

    func networkCheck()
    {
        if(self.doNetworkCheck)
        {
            CLNetworkStatusCheck.networkStatusCheck(completionhandler: { [weak self] (successful : Bool) in
                
                DispatchQueue.main.async {
                    
                    if(successful)
                    {
                        self?.networkStatusLabel.isHidden = true
                    }
                    else
                    {
                        self?.networkStatusLabel.isHidden = false
                    }
                    self?.perform(#selector(self?.networkCheck), with: nil, afterDelay: 1.0)
                }

            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
    
        self.doNetworkCheck = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        
        // This ensuers the keyboard stays visible because it may be obscurred in landscape
        if(  UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiom.phone)
        {
            return super.supportedInterfaceOrientations
        }

        return [.portrait]
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        self.attemptLogin()
    }
    
    private func attemptLogin() {
        
        let username = self.userNameTextField.text
        let password = self.passwordTextField.text
        
        DashboardRootViewController.rootViewController().attemptLogin(userName: username, password: password, completion: { (successful) in
        
            DispatchQueue.main.async {
                
                if (successful)
                {
                    DashboardSettingsObject.shared.userName = username!
                    DashboardSettingsObject.shared.password = password!
                    
                    self.passwordTextField.resignFirstResponder()
                    self.userNameTextField.resignFirstResponder()
                    
                    DashboardRootViewController.rootViewController().processLogin(fromTouchID: false)
                }
                else
                {
                    
                    // Warning: this may be overkill
//                    DashboardSettingsObject.shared.userName = nil
//                    DashboardSettingsObject.shared.password = ""
//                    DashboardSettingsObject.shared.userPIN = ""
//                    DashboardSettingsObject.shared.useTouchId = false
                    
                    let alertController : UIAlertController = UIAlertController.init(title: "Login Failed", message: "Login failed.\n\nPlease try again by entering in your user name and password", preferredStyle: .alert)
                    let alertAction = UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                        
//                        self.userNameTextField.text = ""
                        self.passwordTextField.text = ""
//                        self.userNameTextField.becomeFirstResponder()
                        self.passwordTextField.becomeFirstResponder()
                        
                    })
                    alertController.addAction(alertAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            }
        
        /*
        DashboardDataObject.shared.performLogin(userName: userName, password: password) { (successful) in
            
            DispatchQueue.main.async {
                
                if(successful)
                {
                    DashboardRootViewController.rootViewController().selectCurrentMenuScreen()

                }
                else
                {
                }
            }*/
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == self.userNameTextField)
        {
            self.passwordTextField.becomeFirstResponder()
        }
        else if(textField == self.passwordTextField)
        {
            self.attemptLogin()
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
