//
//  ColumnColorTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/24/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ColumnColorTableViewController: UITableViewController, ColorSelectionDelegate, AddButtonDelegate {

    weak var chartDelegate : ChartConfigurationDelegate?
    var chartDataObject : ChartDataObject?
    var selectedColorRow : Int = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.tableView.isEditing = true
        self.tableView.allowsSelectionDuringEditing = true
        self.chartDelegate?.enableAddButton(addDelegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (self.chartDataObject?.chartColors?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rowColorCell", for: indexPath) as! ColumnColorTableViewCell

        // Configure the cell...
        cell.columnTextField.text = "Element - \(indexPath.row + 1)"
        cell.columnColorView.backgroundColor = self.chartDataObject?.chartColors?[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "colorDetails", sender: indexPath)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if(self.chartDataObject!.chartColors!.count <= 1)
        {
            return false
        }
        
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            self.chartDataObject?.chartColors?.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.perform(#selector(reloadAfterDelay), with: nil, afterDelay: 0.25)
        }
    }
 
    func reloadAfterDelay()
    {
        self.tableView.reloadData()
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

        let itemToMove : UIColor = (self.chartDataObject?.chartColors?[fromIndexPath.row])!
        self.chartDataObject?.chartColors?.remove(at: fromIndexPath.row)
        self.chartDataObject?.chartColors?.insert(itemToMove, at: to.row)

        self.perform(#selector(reloadAfterDelay), with: nil, afterDelay: 0.25)
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        
        if(self.chartDataObject!.chartColors!.count <= 1)
        {
            return false
        }
        return true
    }

    func addButtonPressed()
    {
        self.chartDataObject?.chartColors?.append(UIColor.red)
        
        self.tableView.reloadData()
        
        let indexPath = IndexPath.init(row: (self.chartDataObject?.chartColors?.count)! - 1, section: 0)
        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
    }

    // MARK: - ColorSelectionDelegate
    func saveColorSelection(_ colorSelectionViewController: ColorSelectionViewController!, color: UIColor!) {
    
        if(self.selectedColorRow >= 0)
        {
            self.chartDataObject?.chartColors?[self.selectedColorRow] = color
            self.tableView.reloadData()
        }
        
        self.selectedColorRow = -1

        self.navigationController!.popViewController(animated: true)
    }
    
    func cancelColorSelection(_ colorSelectionViewController: ColorSelectionViewController!) {
        
        self.selectedColorRow = -1

        self.navigationController!.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "colorDetails")
        {
            let vc = segue.destination as! ColorSelectionViewController
            vc.enableSaveButton = true
            vc.enableCloseButton = true
            vc.hideClearColorButton = true
            
            vc.titleOverride = "Column Color"
            
            let indexPath = sender as! IndexPath
            self.selectedColorRow = indexPath.row
            vc.selectedColor = self.chartDataObject?.chartColors?[self.selectedColorRow]
            vc.delegate = self
        }
        
    }

}
