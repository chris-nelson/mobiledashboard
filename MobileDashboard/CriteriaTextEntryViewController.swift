//
//  CriteriaTextEntryViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/6/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class CriteriaTextEntryViewController: UIViewController, EntryFieldDelegate, UITextFieldDelegate {

    @IBOutlet weak var textEntryLabel: UILabel!
    @IBOutlet weak var textEntryTextField: UITextField!
    
    weak var chartDelegate : ChartConfigurationDelegate?
    var chartQueryParameterObject : ChartQueryParameterObject?
    
    var numberField : Bool = false

    var errorMessage : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.textEntryLabel.text = self.chartQueryParameterObject?.fieldParameterName
        // Do any additional setup after loading the view.
        
        self.textEntryTextField.delegate = self

        if self.numberField
        {
            self.textEntryTextField.keyboardType = .numberPad
        }
        
        self.textEntryTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleKeyboardButtonPressed(_ sender: UIButton) {
    
        if(self.textEntryTextField.isFirstResponder)
        {
            self.textEntryTextField.resignFirstResponder()
        }
        else
        {
            self.textEntryTextField.becomeFirstResponder()
        }
    
    }
    
    func entryValid() -> Bool
    {
        var pass : Bool = true
        
        self.chartQueryParameterObject?.fieldParameterValue = self.textEntryTextField.text

        /*if let stringValue = self.chartQueryParameterObject?.fieldParameterValue
        {
            if(stringValue.lengthOfBytes(using: String.Encoding.utf8) == 0)
            {
                pass = false
                self.errorMessage = "Please enter a value for \(self.chartQueryParameterObject!.fieldParameterName!)"
            }
        }
        else
        {
            pass = false
            self.errorMessage = "Please enter a value for \(self.chartQueryParameterObject!.fieldParameterName!)"
        }*/
        
        return pass
    }
    
    func displayErrorMessage ()
    {
        if(!self.entryValid())
        {
            let alertController : UIAlertController = UIAlertController.init(title: "Error", message: self.errorMessage, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if self.numberField
        {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            return string == numberFiltered
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == self.textEntryTextField)
        {
            self.chartDelegate?.moveNext()
        }
        
        return true
    }
}
