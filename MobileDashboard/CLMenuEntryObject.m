//
//  CLMenuEntryObject.m
//

#import "CLMenuEntryObject.h"

@interface CLMenuEntryObject ()

@property (nonatomic, strong, readwrite) NSMutableArray* menuSubEntries;

@end

@implementation CLMenuEntryObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        _badgeOffset = NSNotFound;
    }
    return self;
}
- (NSMutableArray*) menuSubEntries
{
    if(_menuSubEntries == nil)
    {
        _menuSubEntries = [[NSMutableArray alloc]init];
    }
    return _menuSubEntries;
}

- (NSString*) title
{
    NSString* title = _title;
    if(self.generatedTitle != nil)
        title = self.generatedTitle();
    
    return title;
}

@end
