//
//  ChartAxisSelectionTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/10/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartAxisSelectionTableViewController: UITableViewController, EntryFieldDelegate {

    var localQueryParamArray : [ChartFieldParameterObject] = []

    weak var chartDelegate : ChartConfigurationDelegate?
    var chartDataSource : ChartDataSource?
    var chartDataObject : ChartDataObject?

    var errorMessage : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    
        for (_, fieldParam) in self.chartDataSource!.fieldParameters
        {
            if(fieldParam.filterType == .localOnly || fieldParam.filterType == .localAndRemote)
            {
                localQueryParamArray.append(fieldParam)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.localQueryParamArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "queryCell", for: indexPath)

        // Configure the cell...
        let queryParam = self.localQueryParamArray[indexPath.row]
        
        cell.textLabel?.text = queryParam.name
        
        if(queryParam.name == self.chartDataObject?.axisColumn)
        {
            cell.textLabel?.textColor = UIColor.blue
        }
        else
        {
            cell.textLabel?.textColor = UIColor.black
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let chartDelegate = self.chartDelegate
        {
            let queryParam = self.localQueryParamArray[indexPath.row]
            self.chartDataObject?.axisColumn = queryParam.name

            chartDelegate.moveNext()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func entryValid() -> Bool
    {
        var pass : Bool = true
        
        if let stringValue = self.chartDataObject?.axisColumn
        {
            if(stringValue.lengthOfBytes(using: String.Encoding.utf8) == 0)
            {
                pass = false
                self.errorMessage = "Please Select a value for the axis"
            }
        }
        else
        {
            pass = false
            self.errorMessage = "Please Select a value for the axis"
        }
        
        return pass
    }
    
    func displayErrorMessage ()
    {
        if(!self.entryValid())
        {
            let alertController : UIAlertController = UIAlertController.init(title: "Error", message: self.errorMessage, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }

}
