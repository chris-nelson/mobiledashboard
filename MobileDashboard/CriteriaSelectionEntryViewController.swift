//
//  CriteriaSelectionEntryViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/9/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class CriteriaSelectionEntryViewController: UIViewController, EntryFieldDelegate, UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating
{
    @IBOutlet weak var textEntryLabel: UILabel!
    weak var chartDelegate : ChartConfigurationDelegate?
    var chartQueryParameterObject : ChartQueryParameterObject?
    var domain : DomainObject?
    var entriesGroup : [String:[DomainValueObject]] = [:]
    var entriesSorted : [[DomainValueObject]] = []
    var allEntriesSortedItems : [DomainValueObject] = []
    var filteredItems : [DomainValueObject] = []
    var indexTitles : [String]?
    var errorMessage : String?

    @IBOutlet weak var tableView: UITableView!
    var searchResultsTableViewController : UITableViewController = UITableViewController.init()
    var searchController : UISearchController?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.textEntryLabel.text = self.chartQueryParameterObject?.fieldParameterName
        
        self.domain = DashboardDataObject.shared.domains[chartQueryParameterObject!.fieldParameterIdentifier!]
//        self.indexTitles = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"]
        
        self.sortAndGroupEntries()

        self.searchController = UISearchController.init(searchResultsController: searchResultsTableViewController)
        self.searchController!.searchResultsUpdater = self
        self.searchController!.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.searchController!.searchBar;

        self.searchResultsTableViewController.tableView.delegate = self;
        self.searchResultsTableViewController.tableView.dataSource = self;
        
        self.searchController!.delegate = self
        self.searchController!.hidesNavigationBarDuringPresentation = true
        self.searchController!.dimsBackgroundDuringPresentation = true
        self.searchController!.searchBar.delegate = self

        self.searchResultsTableViewController.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "selectionCell")
        
//        self.definesPresentationContext = true // know where you want UISearchController to be displayed

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {

        if (tableView == self.searchResultsTableViewController.tableView)
        {
            return 1
        }
        else if(self.indexTitles == nil)
        {
            return 1
        }
        return self.indexTitles!.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (tableView == self.searchResultsTableViewController.tableView)
        {
            return self.filteredItems.count;
        }
        else if(self.indexTitles == nil)
        {
            return allEntriesSortedItems.count
        }
        
        let sectionTitle = self.indexTitles?[section]
        let sectionRows = self.entriesGroup[sectionTitle!]
        
        if(sectionRows == nil)
        {
            return 0
        }
        return sectionRows!.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if (tableView == self.searchResultsTableViewController.tableView)
        {
            return nil;
        }
        else if(self.indexTitles == nil)
        {
            return nil;
        }
        
        let sectionTitle = self.indexTitles?[section]
        let sectionRows = self.entriesGroup[sectionTitle!]
        if(sectionRows?.count == 0)
        {
            return nil;
        }
        
        return sectionTitle
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        if (tableView == self.searchResultsTableViewController.tableView)
        {
            return nil
        }
        else if(self.indexTitles == nil)
        {
            return nil;
        }

        return indexTitles
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell", for: indexPath)

        let domainValueObject : DomainValueObject?
        if(tableView == self.searchResultsTableViewController.tableView)
        {
            domainValueObject = self.filteredItems[indexPath.row]
        }
        else
        {
            if(self.indexTitles == nil)
            {
                domainValueObject = self.allEntriesSortedItems[indexPath.row]
            }
            else
            {
                let sectionTitle = self.indexTitles?[indexPath.section]
                let sectionRows = self.entriesGroup[sectionTitle!]
                domainValueObject = sectionRows?[indexPath.row]
            }
        }

        
        // Configure the cell...
        cell.textLabel?.text = domainValueObject!.domainValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let domainValueObject : DomainValueObject?
        if(tableView == self.searchResultsTableViewController.tableView)
        {
            domainValueObject = self.filteredItems[indexPath.row]
            self.dismiss(animated: true, completion: { 
                
                DispatchQueue.main.async {

                    self.chartQueryParameterObject?.fieldParameterValue = domainValueObject?.domainValue
                    
                    self.chartDelegate?.moveNext()
                    
                    tableView.deselectRow(at: indexPath, animated: true)
                }
            })
        }
        else
        {
            if(self.indexTitles == nil)
            {
                domainValueObject = self.allEntriesSortedItems[indexPath.row]
            }
            else
            {
                let sectionTitle = self.indexTitles?[indexPath.section]
                let sectionRows = self.entriesGroup[sectionTitle!]
                domainValueObject = sectionRows?[indexPath.row]
            }

            self.chartQueryParameterObject?.fieldParameterValue = domainValueObject?.domainValue
            
            self.chartDelegate?.moveNext()
            
            tableView.deselectRow(at: indexPath, animated: true)
}
    }

    func entryValid() -> Bool
    {
        var pass : Bool = true
        
//        self.chartQueryParameterObject?.fieldParameterValue = self.textEntryTextField.text
        
        if let stringValue = self.chartQueryParameterObject?.fieldParameterValue
        {
            if(stringValue.lengthOfBytes(using: String.Encoding.utf8) == 0)
            {
                pass = false
                self.errorMessage = "Please select a value for \(self.chartQueryParameterObject!.fieldParameterName!)"
            }
        }
        else
        {
            pass = false
            self.errorMessage = "Please select a value for \(self.chartQueryParameterObject!.fieldParameterName!)"
        }
        
        return pass
    }
    
    func displayErrorMessage()
    {
        if(!self.entryValid())
        {
            let alertController : UIAlertController = UIAlertController.init(title: "Error", message: self.errorMessage, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func sortAndGroupEntries()
    {
        self.entriesGroup.removeAll()
        self.entriesSorted.removeAll()
        self.allEntriesSortedItems.removeAll()
    
        if(self.indexTitles == nil)
        {
            self.allEntriesSortedItems = self.domain!.domainValues.sorted(by: { (dv1, dv2) -> Bool in
                
                if(dv1.domainValue!.compare(dv2.domainValue!) == .orderedAscending)
                {
                    return true
                }
                return false
            })
        }
        else
        {
            var predicate : NSPredicate? = nil
            
            for letter in self.indexTitles!
            {
                if (letter == "#")
                {
                    predicate = NSPredicate.init(format: "(SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@ || SELF.domainValue BEGINSWITH[c] %@)", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "", " ")
                }
                else
                {
                    predicate = NSPredicate.init(format: "SELF.domainValue BEGINSWITH[c] %@", letter)
                }
                
                let filteredEntries = self.domain!.domainValues.filter({ predicate!.evaluate(with: $0) })
                let sortedandFilteredArray = filteredEntries.sorted(by: { (dv1, dv2) -> Bool in
                    
                    if(dv1.domainValue!.compare(dv2.domainValue!) == .orderedAscending)
                    {
                        return true
                    }
                    return false
                })
                
                if(sortedandFilteredArray.count > 0)
                {
                    self.entriesSorted.append(sortedandFilteredArray)
                    self.entriesGroup[letter] = sortedandFilteredArray
                }
            }
        }
    }
    
    // Mark: UISearchControllerDelegate Delegate
    
    func willDismissSearchController(_ searchController: UISearchController) {
        
        self.filteredItems.removeAll()
        
        self.tableView.reloadData()
    }
    
    // Mark: UISearchControllerDelegate Delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchString = searchController.searchBar.text;
        
        let predicate : NSPredicate? = NSPredicate.init(format: "  SELF.domainValue.lowercaseString contains[c] %@", searchString!.lowercased())
        
        let filteredEntries = self.domain!.domainValues.filter({ predicate!.evaluate(with: $0) })
        self.filteredItems = filteredEntries.sorted(by: { (dv1, dv2) -> Bool in
            
            if(dv1.domainValue!.compare(dv2.domainValue!) == .orderedAscending)
            {
                return true
            }
            return false
        })
        self.searchResultsTableViewController.tableView.reloadData()
    }
}
