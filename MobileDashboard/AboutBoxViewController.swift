//
//  AboutBoxViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/23/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class AboutBoxViewController: UIViewController {

    deinit {
        
        var foo = "abc"
        print("deinit - AboutBoxViewController")
        foo = "abcd"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func closeButtonPressed(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
