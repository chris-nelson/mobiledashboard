//
//  DashboardDataObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/29/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class DashboardDataObject: NSObject, URLSessionDelegate {

    static public let shared = DashboardDataObject()
    
    var session : URLSession!

    public var name = "Name"
    public var offlineMode = false
    
    public var loggedIn = false
    public var dataLoaded = false
    
    var forceDownload = false
    
    public var chartDataSources : [Int : ChartDataSource] = [:]
    
    private var chartPagesIndex : [String] = []
    private var chartPagesDictionary : [String:ChartPageDataObject] = [:]
    
    public var domains : [String : DomainObject] = [:]
    
    public let baseURLString : String = "http://343256linuxjavadev.mhsl01.mhsl.local:8091/mobile-dashboard-service"
    public let endPointURLString : String =  "/endPoints"
    public let domainsURLString : String = "/domains"
    public let queryDefsURLString : String = "/queryDefs"
    public let dataURLString : String = "/data"
    
    public let indexFileName : String = "ChartPageIndex.plist"
    
    let domainName = "name"
    let domainIdetifier = "identifier"
    let domainEndPoint = "endpointUrl"
    
    let domainSortGroup = "SORTGROUP"
    let domainValue = "VALUE"

    let chartSourceName = "name"
    let chartSourceVersion = "version"
    let chartSourceIdentifier = "identifier"
    let chartSourceEndPoint = "endpointUrl"
    
    let fieldParam = "fieldParams"
    let fieldParamDomainURL = "domainUrl"
    let fieldParamFilterType = "filterType"
    let fieldParamName = "name"
    let fieldParamRequired = "required"
    let fieldParamSelectionType = "selectionType"
    let fieldParamType = "type"
    
    override init() {

        super.init()
    }
    
    func isConfigurationDataAvailable() -> Bool
    {
        var pass = false
        
        if(self.domains.count > 0 && chartDataSources.count > 0)
        {
            pass = true
        }
        
        return pass
    }
    
    private func saveChartPageIndex()
    {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let chartindexURL : URL! = documentsURL?.appendingPathComponent(self.indexFileName)
        
        self.validateIndex()
        
        if(NSKeyedArchiver.archiveRootObject(self.chartPagesIndex, toFile: chartindexURL.path))
        {
            print("Saved")
        }
        else
        {
            print("Not Saved")
        }
    }
    
    private func loadChartPageIndex()
    {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let chartindexURL : URL! = documentsURL?.appendingPathComponent(self.indexFileName)
        
        if(FileManager.default.fileExists(atPath: chartindexURL.path))
        {
            self.chartPagesIndex = (NSKeyedUnarchiver.unarchiveObject(withFile: (chartindexURL?.path)!) as? [String])!
        }
        else
        {
            self.chartPagesIndex = []
        }
    }
    
    func resetData()
    {
        self.chartDataSources.removeAll()
        self.chartPagesIndex.removeAll()
        self.chartPagesDictionary.removeAll()
        self.domains.removeAll()
    }
    
    func dataSourcesSortedArray() -> [ChartDataSource] {
        
        let sortedDataSources : [ChartDataSource] =  self.chartDataSources.values.sorted { (dataSource1, dataSource2) -> Bool in
            
            if(dataSource1.name?.compare(dataSource2.name!) == ComparisonResult.orderedAscending)
            {
                return true
            }
            
            return false
        }
        
        return sortedDataSources
    }
    
    func add(chartPage : ChartPageDataObject)
    {
        //        let index = self.chartPagesIndex.index(of: chartPage.identifier!)
        //        if(index == nil)
        //        {
        self.chartPagesIndex.append(chartPage.identifier!)
        //        }
        self.chartPagesDictionary[chartPage.identifier!] = chartPage
        
        self.saveChartPageIndex()
    }
    
    func importFromShare(chartPage : ChartPageDataObject)
    {
        let index = self.chartPagesIndex.index(of: chartPage.identifier!)
        if(index == nil)
        {
            self.chartPagesIndex.append(chartPage.identifier!)
        }
        self.chartPagesDictionary[chartPage.identifier!] = chartPage
        
        self.saveChartPageIndex()
    }
    
    func update(chartPage : ChartPageDataObject) -> Bool
    {
        var pass = false
        
        let oldChartPage = self.chartPagesDictionary[chartPage.identifier!]
        if(oldChartPage != nil)
        {
            self.chartPagesDictionary[chartPage.identifier!] = chartPage
            
            self.saveChartPageIndex()
            
            pass = true
        }
        return pass
    }
    
    func chartPageCount() -> Int
    {
        return self.chartPagesIndex.count;
    }

    func clearChartPageImageCache(){
        
        for chartPageIdentifier in self.chartPagesIndex
        {
            let chartPageLookup = self.chartPagesDictionary[chartPageIdentifier]
            if let chartPage = chartPageLookup
            {
                chartPage.deleteCacheImage()
            }
        }
    }
    
    func clearChartPageDataCache(){
        
        for chartPageIdentifier in self.chartPagesIndex
        {
            let chartPageLookup = self.chartPagesDictionary[chartPageIdentifier]
            if let chartPage = chartPageLookup
            {
                chartPage.deleteChartFiles()
            }
        }
    }
    
    func clearAllChartPagesDownloadFailedStatus(clearImage : Bool)
    {
        for chartPageIdentifier in self.chartPagesIndex
        {
            let chartPageLookup = self.chartPagesDictionary[chartPageIdentifier]
            if let chartPage = chartPageLookup
            {
                if(chartPage.dataDownloadFailed && clearImage)
                {
                    chartPage.deleteCacheImage()
                }
                chartPage.dataDownloadFailed = false
            }
        }
    }
    
    func swapChartPagePosition(atSourceIndex : Int, forDestinationIndex : Int)
    {
        if((atSourceIndex >= 0 && atSourceIndex < self.chartPagesIndex.count) && (forDestinationIndex >= 0 && forDestinationIndex < self.chartPagesIndex.count))
        {
            let itemToMove : String = self.chartPagesIndex[atSourceIndex]
            self.chartPagesIndex.remove(at: atSourceIndex)
            self.chartPagesIndex.insert(itemToMove, at: forDestinationIndex)
        }
        self.saveChartPageIndex()
    }
    
    func chartPage(atIndex : Int)->ChartPageDataObject?
    {
        var chartPage : ChartPageDataObject? = nil
        
        if(atIndex >= 0 && atIndex < self.chartPagesIndex.count)
        {
            let chartPageIdentifier = self.chartPagesIndex[atIndex]
            let chartPageLookup = self.chartPagesDictionary[chartPageIdentifier]
            if let chartPageFound = chartPageLookup
            {
                chartPage = chartPageFound
            }
        }
        
        return chartPage
    }
    
    func removeChartPage(atIndex : Int) -> Bool
    {
        var pass = false
        
        if(atIndex >= 0 && atIndex < self.chartPagesIndex.count)
        {
            let chartPageIdentifier = self.chartPagesIndex[atIndex]
            self.chartPagesDictionary.removeValue(forKey: chartPageIdentifier)
            self.chartPagesIndex.remove(at: atIndex)
            
            self.saveChartPageIndex()

            pass = true
        }
        
        return pass
    }

    func performLogin(userName : String?, password : String?, completion : @escaping (_ successful : Bool)->Void )
    {
        self.offlineMode = false
        self.loggedIn = false
        
        if let userNameLogin = userName , let passwordLogin = password
        {
            if ( userNameLogin.lengthOfBytes(using: String.Encoding.utf8) > 0 &&  passwordLogin.lengthOfBytes(using: String.Encoding.utf8) > 0 )
            {
                CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
                    
                    if(successful)
                    {
                        self.loggedIn = true
                        let url : NSURL = NSURL.init(string: self.baseURLString + self.endPointURLString)!
                        var urlRequest : URLRequest = URLRequest.init(url: url as URL)
                        
                        let authStr : String = "\(userNameLogin):\(passwordLogin)"
                        
                        let authData : Data = authStr.data(using: String.Encoding.utf8)!
                        let base64EncodedCredential = authData.base64EncodedString()
                        let authString = "Basic \(base64EncodedCredential)"
                        
                        urlRequest.setValue(authString, forHTTPHeaderField: "Authorization")
                        urlRequest.httpMethod = "GET"
                        
                        let config = URLSessionConfiguration.default
                        self.session = URLSession.init(configuration: config)
                        
                        //create the task
                        let task : URLSessionDataTask = self.session.dataTask(with: urlRequest, completionHandler: {(data, response, error) in
                            
                            let urlResponse = response as? HTTPURLResponse
                            
                            if(urlResponse != nil)
                            {
                                print("Login Response - \(urlResponse!)")
                                if ((urlResponse!.statusCode >= 200 && urlResponse!.statusCode < 300) && error == nil && data != nil)
                                {
                                    completion(true)
                                }
                                else
                                {
                                    completion(false)
                                }
                            }
                            else
                            {
                                completion(false)
                            }
                        })
                        
                        task.resume()
                    }
                    else
                    {
                        // if the user name and password are a match allow logging in while offline, PIN or Touch ID along with the user name / password
                        if(DashboardSettingsObject.shared.userName == userNameLogin && DashboardSettingsObject.shared.password == passwordLogin)
                        {
                            self.offlineMode = true
                            completion(true)
                        }
                        else
                        {
                            completion(false)
                        }
                    }
                })
            }
            else
            {
                completion(false)
            }
        }
        else
        {
            completion(false)
        }
    }
    
    func loadDomains(completion : @escaping (_ successful : Bool)->Void ) {
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let domainBackupFileURL = documentsURL?.appendingPathComponent("backupDomains.plist")
        let currentDomainFileURL = documentsURL?.appendingPathComponent("domains.plist")
        
        // If download fails or is unavailable attempt to load from cached data
        let loadDomainsFromCache : () -> () = {
            
            self.domains.removeAll()
            if(FileManager.default.fileExists(atPath: (currentDomainFileURL?.path)!))
            {
                let data = NSKeyedUnarchiver.unarchiveObject(withFile: (currentDomainFileURL?.path)!) as?  [String : DomainObject]
                if(data == nil)
                {
                    completion(false)
                }
                else
                {
                    self.domains = data!
                    completion(true)
                }
            }
            else
            {
                completion(false)
            }
        }
        
        CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
            
            if(successful)
            {
                let url : URL = URL.init(string: "\(self.baseURLString)\(self.domainsURLString)")!
                let task : URLSessionDataTask = self.session.dataTask(with: url) { (data, response, error) in
                    
                    if let mydata = data
                    {
                        self.domains = [:]

                        // Delete the backup file if it exists
                        if(FileManager.default.fileExists(atPath: (domainBackupFileURL?.path)!))
                        {
                            do
                            {
                                try FileManager.default.removeItem(atPath: (domainBackupFileURL?.path)!)
                            }
                            catch
                            {
                                print(error.localizedDescription)
                            }
                        }

                        var domainEndpointDownloadArray : [[String:Any]]?
                        do
                        {
                            domainEndpointDownloadArray = try JSONSerialization.jsonObject(with: mydata, options: .allowFragments) as? [[String:Any]]
                        }
                        catch
                        {
                            print(error.localizedDescription)
                        }
                        
                        // Add each Domain file name to an array, once all domains have been saved save the list, also delete the name list file once the initial download is successful
                        
                        //                print("Domain Endpoints \(domainEndpointArray)")
                        
                        var queues = [() -> ()]()
                        var count : Int = 0
                        
                        if let domainEndpointArray = domainEndpointDownloadArray
                        {
                            for endpointDict : [String : Any] in domainEndpointArray
                            {
                                let domain : DomainObject = DomainObject.init()
                                
                                domain.name = endpointDict[self.domainName] as! String
                                // Warning:  The Identifier in the two domains given is the same
                                domain.identifier = endpointDict[self.domainIdetifier] as! Int
                                domain.endpoint = endpointDict[self.domainEndPoint] as! String
                                
                                self.domains[domain.name] = domain
                                
                                //                            print("\n\nName - \(endpointName)  Identifier - \(endpointIdentifier)\n\n")
                                
                                let getEndpoint : () -> () = {
                                    
                                    let urlDomain : URL = URL.init(string: "\(self.baseURLString)\(domain.endpoint)")!
                                    
                                    //                        print("\nProcessing - Name - \(domain.name)  Identifier - \(domain.identifier )")
                                    
                                    let endpointTask : URLSessionDataTask = self.session.dataTask(with: urlDomain) { (endpointData, endpointResponse, endpointError) in
                                        
                                        do
                                        {
                                            let domainDownloadArray = try JSONSerialization.jsonObject(with: endpointData!, options: .allowFragments) as? [[String:Any]]
                                            
                                            if let domainArray = domainDownloadArray
                                            {
                                                for domainDict : [String:Any] in domainArray
                                                {
                                                    let domainValue : DomainValueObject = DomainValueObject.init()
                                                    
                                                    // Warning: the is coming in as a string sometimes this is an unnecessary conversion, See Wendell
                                                    if let sortInt = domainDict[self.domainSortGroup] as? Int
                                                    {
                                                        domainValue.sortGroup = sortInt
                                                    }
                                                    else if let sortString = domainDict[self.domainSortGroup] as? String
                                                    {
                                                        domainValue.sortGroup = Int(sortString)!
                                                    }
                                                    else
                                                    {
                                                        print("Domain SortGroup missing")
                                                    }
                                                    
                                                    if let val = domainDict[self.domainValue] as? String
                                                    {
                                                        domainValue.domainValue = val
                                                    }
                                                    else
                                                    {
                                                        print("Domain Value missing")
                                                    }
                                                    
                                                    if(domainValue.domainValue != nil)
                                                    {
                                                        domain.domainValues.append(domainValue)
                                                    }
                                                }
                                                
                                                domain.domainValues = domain.domainValues.sorted(by: { (dv1, dv2) -> Bool in
                                                    if(dv1.sortGroup == dv2.sortGroup)
                                                    {
                                                        if(dv1.domainValue!.compare(dv2.domainValue!) == .orderedAscending)
                                                        {
                                                            return true
                                                        }
                                                        return false
                                                    }
                                                    else if(dv1.sortGroup < dv2.sortGroup)
                                                    {
                                                        return true
                                                    }
                                                    
                                                    return false
                                                })
                                                
                                                //                                        print(domain)
                                                if(count + 1 < queues.count)
                                                {
                                                    let block = queues[count + 1]
                                                    block()
                                                }
                                                else
                                                {
                                                    // rename the current to backup just in case
                                                    if(FileManager.default.fileExists(atPath: (currentDomainFileURL?.path)!))
                                                    {
                                                        do
                                                        {
                                                            try FileManager.default.moveItem(at: currentDomainFileURL!, to: domainBackupFileURL!)
                                                        }
                                                        catch
                                                        {
                                                            print(error.localizedDescription)
                                                        }
                                                    }
                                                    
                                                    // Save the current domains
                                                    if(NSKeyedArchiver.archiveRootObject(self.domains, toFile: (currentDomainFileURL?.path)!))
                                                    {
                                                        if(FileManager.default.fileExists(atPath: (domainBackupFileURL?.path)!))
                                                        {
                                                            do
                                                            {
                                                                try FileManager.default.removeItem(atPath: (domainBackupFileURL?.path)!)
                                                            }
                                                            catch
                                                            {
                                                                print(error.localizedDescription)
                                                            }
                                                        }
                                                        completion(true)
                                                    }
                                                    else
                                                    {
                                                        // Unable to save to cache load from
                                                        if(FileManager.default.fileExists(atPath: (currentDomainFileURL?.path)!))
                                                        {
                                                            do
                                                            {
                                                                try FileManager.default.removeItem(atPath: (currentDomainFileURL?.path)!)
                                                            }
                                                            catch
                                                            {
                                                                print(error.localizedDescription)
                                                            }
                                                        }
                                                        
                                                        do
                                                        {
                                                            try FileManager.default.moveItem(at: domainBackupFileURL!, to: currentDomainFileURL!)
                                                        }
                                                        catch
                                                        {
                                                            print(error.localizedDescription)
                                                        }

                                                        loadDomainsFromCache()
                                                    }
                                                    
                                                    //                                            print("Domains from Network\n\(self.domains)")
                                                }
                                                count += 1
                                            }
                                            else
                                            {
                                                // unable to download a domain load all domains from cache
                                                loadDomainsFromCache()
                                            }
                                        }
                                        catch
                                        {
                                            // exception thrown while processing JSON
                                            print(error.localizedDescription)
                                            loadDomainsFromCache()
                                        }
                                    }
                                    endpointTask.resume()
                                }
                                queues.append(getEndpoint)
                            }
                            let block = queues[0]
                            block()
                        }
                        else
                        {
                            // Unable to download the list of domaains
                            loadDomainsFromCache()
                        }
                    }
                        
                    else
                    {
                        // No data downloaded
                        loadDomainsFromCache()
                    }
                }
                
                task.resume()
            }
            else
            {
                // User is offline
                loadDomainsFromCache()
            }
        })
        
    }
    
    func loadQueryDefs(completion : @escaping (_ successful : Bool)->Void )
    {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        
        let fileURL = documentsURL?.appendingPathComponent("QueryDefs.plist")
        
        let documentQueryFolderURL = documentsURL?.appendingPathComponent("QueryDefsFiles")
        let documentQueryStagingFolderURL = documentsURL?.appendingPathComponent("QueryDefsFilesStaging")
        
        var originalChartSources : [Int : ChartDataSource]? = nil
        if(!self.forceDownload)
        {
            originalChartSources = NSKeyedUnarchiver.unarchiveObject(withFile: (fileURL?.path)!) as? [Int : ChartDataSource]
        }
        
        self.forceDownload = false
        
        // Confirm that all data exist or nil the
        if(originalChartSources != nil)
        {
            var pass : Bool = true
            for (_, dataSource) in originalChartSources!
            {
                let queryFileName : String! = "\(dataSource.name!).plist"
                let queryDeffilepreviousURL = documentQueryFolderURL!.appendingPathComponent(queryFileName)
                if(!FileManager.default.fileExists(atPath: (queryDeffilepreviousURL.path)))
                {
                    pass = false
                    break
                }
            }
            
            if(!pass)
            {
                // One or more files missing, force a download all
                originalChartSources = nil
            }
        }
        
        let loadQueryDefsFromCache : () -> () = {
            
            var folderExist = false
            self.chartDataSources.removeAll()
            if(originalChartSources != nil)
            {
                self.chartDataSources = originalChartSources!
            }
            
            
            var isDirExist : ObjCBool = false
            if(FileManager.default.fileExists(atPath: (documentQueryFolderURL?.path)!, isDirectory: &isDirExist))
            {
                if(isDirExist.boolValue)
                {
                    folderExist = true
                }
            }
            
            // Warning: This assumes that the file list in self.chartSources matches the files in documentQueryFolderURL
            // Should be okay because originalChartSources confirms all the files are present and self.chartSources is assigned to originalChartSources
            if(folderExist && self.chartDataSources.count > 0)
            {
                completion(true)
            }
            else
            {
                completion(false)
            }
        }
        
        CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
            
            if(successful)
            {
                // Download the list of query defintion files
                let url : URL = URL.init(string: "\(self.baseURLString)\(self.queryDefsURLString)")!
                let task : URLSessionDataTask = self.session.dataTask(with: url) { (data, response, error) in
                    
                    // Data downloaded
                    if let mydata = data
                    {
                        self.chartDataSources = [:]
                        
                        // Delete the staging folder if it exists. . .
                        /////
                        var isDir : ObjCBool = false
                        if(FileManager.default.fileExists(atPath: (documentQueryStagingFolderURL?.path)!, isDirectory: &isDir))
                        {
                            if(isDir.boolValue)
                            {
                                do
                                {
                                    try FileManager.default.removeItem(atPath: documentQueryStagingFolderURL!.path)
                                }
                                catch
                                {
                                    print(error.localizedDescription)
                                }
                            }
                        }
                        
                        // Create a new empty staging folder
                        do
                        {
                            try FileManager.default.createDirectory(at: documentQueryStagingFolderURL!, withIntermediateDirectories: true, attributes: nil)
                        }
                        catch
                        {
                            print(error.localizedDescription)
                        }
                        /////
                        
                        // Parse the downloaded JSON formatted data into an Array of dictionaries
                        var queryDefsEndpointsDownloadArray : [[String:Any]]?
                        do
                        {
                            queryDefsEndpointsDownloadArray = try JSONSerialization.jsonObject(with: mydata, options: .allowFragments) as? [[String:Any]]
                        }
                        catch
                        {
                            queryDefsEndpointsDownloadArray = nil
                            print(error.localizedDescription)
                        }
                        //                        print("QueryDefs \(queryDefsEndpointsArray)")
                        
                        // The queues are an array of closures to download each file sequentially
                        /////
                        var queues = [() -> ()]()
                        var count : Int = 0
                        /////
                        
                        if let queryDefsEndpointsArray = queryDefsEndpointsDownloadArray
                        {
                            // From the JSON parsed array, populate the chart sources dictionary
                            for queryDefDict : [String : Any] in queryDefsEndpointsArray
                            {
                                let chartSource : ChartDataSource = ChartDataSource.init()
                                
                                chartSource.name = queryDefDict[self.chartSourceName] as! String?
                                chartSource.version = queryDefDict[self.chartSourceVersion] as! Int
                                chartSource.identifier =  queryDefDict[self.chartSourceIdentifier] as! Int
                                chartSource.endpoint =  queryDefDict[self.chartSourceEndPoint] as! String?
                                
                                let fieldParamArray : [[String:Any]] = queryDefDict[self.fieldParam] as! [[String:Any]]
                                for fieldParamDict : [String:Any] in fieldParamArray
                                {
                                    let fieldParam : ChartFieldParameterObject = ChartFieldParameterObject.init()
                                    
                                    fieldParam.domainUrl = fieldParamDict[self.fieldParamDomainURL] as! String?
                                    fieldParam.filterType = FilterType(rawValue:(fieldParamDict[self.fieldParamFilterType] as! String))!
                                    fieldParam.name = fieldParamDict[self.fieldParamName] as! String?
                                    fieldParam.required = (fieldParamDict[self.fieldParamRequired] != nil)
                                    fieldParam.selectionType  = SelectionType(rawValue:(fieldParamDict[self.fieldParamSelectionType] as! String))!
                                    fieldParam.type = DataType(rawValue:(fieldParamDict[self.fieldParamType] as! String))!
                                    
                                    if(fieldParam.validateFieldParameters())
                                    {
                                        chartSource.fieldParameters[fieldParam.name!] = fieldParam
                                    }
                                }
                                if( chartSource.name == "cntFixtureLocatedByDay")
                                {
                                    print("Chart Source - cntFixtureLocatedByDay - \(chartSource)");
                                }
                                
                                if(chartSource.validateChartSource())
                                {
                                    self.chartDataSources[chartSource.identifier] = chartSource
                                }
                                else
                                {
                                    print("Unable to read Chart Data Source \(chartSource.name)")
                                }
                            }
                            
                            if(self.chartDataSources.count > 0)
                            {
                                /////
                                //completion(true)
                                /////
                                
                                /////
                                // Save the ChartSource Information without the saved data
                                // Warning: this data will only need to be retrieved when a chart is selected
                                for (identifier, chartSource) in self.chartDataSources
                                {
                                    let getEndpoint : () -> () = {
                                        
                                        // This local block is called from multiple locations and if all the chart souces have been downloaded or moved.
                                        // been downloaded or moved.it then moves the directories around from the staging location
                                        let completeProcess : () -> () = {
                                            
                                            // Check if all blocks have processed, if not run the next block
                                            if(count + 1 < queues.count)
                                            {
                                                let block = queues[count + 1]
                                                count += 1
                                                block()
                                            }
                                            else
                                            {
                                                // All blocks have run and all data downloaded or moved
                                                var pass : Bool = true
                                                count += 1
                                                
                                                // If the Queries are currently stored in the data folder rename to a folder for deletion
                                                if(FileManager.default.fileExists(atPath: (documentQueryFolderURL?.path)!, isDirectory: &isDir))
                                                {
                                                    if(isDir.boolValue)
                                                    {
                                                        do
                                                        {
                                                            try FileManager.default.removeItem(atPath: documentQueryFolderURL!.path)
                                                        }
                                                        catch
                                                        {
                                                            print(error.localizedDescription)
                                                            pass = false
                                                        }
                                                    }
                                                }
                                                
                                                // Rename the staging folder to the data folder
                                                if(pass && FileManager.default.fileExists(atPath: (documentQueryStagingFolderURL?.path)!, isDirectory: &isDir))
                                                {
                                                    if(isDir.boolValue)
                                                    {
                                                        do
                                                        {
                                                            try FileManager.default.moveItem(at: documentQueryStagingFolderURL!, to: documentQueryFolderURL!)
                                                        }
                                                        catch
                                                        {
                                                            print(error.localizedDescription)
                                                            pass = false
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    pass = false
                                                }
                                                
                                                // if all went well complete, save the updated chart sources list
                                                if pass
                                                {
                                                    if(NSKeyedArchiver.archiveRootObject(self.chartDataSources, toFile: fileURL!.path))
                                                    {
                                                        completion(true)
                                                    }
                                                    else
                                                    {
                                                        pass = false
                                                    }
                                                }
                                                else
                                                {
                                                    pass = false
                                                }
                                                
                                                if(!pass)
                                                {
                                                    // Unknown failures reset the query defintion file
                                                    if(FileManager.default.fileExists(atPath: fileURL!.path))
                                                    {
                                                        do
                                                        {
                                                            try FileManager.default.removeItem(atPath: fileURL!.path)
                                                        }
                                                        catch
                                                        {
                                                            print(error.localizedDescription)
                                                        }
                                                    }
                                                    // could not save the querydef file, give up :(
                                                    completion(false)
                                                }
                                            }
                                        }
                                        
                                        // if there is a previous query definition file use it to compare versions and download only what is necessary
                                        let originalChartSource : ChartDataSource? = originalChartSources?[identifier]
                                        
                                        let queryFileName : String! = "\(chartSource.name!).plist"
                                        let queryDeffileURL = documentQueryStagingFolderURL?.appendingPathComponent(queryFileName)
                                        
                                        var previousFileExists = false
                                        let queryDeffilepreviousURL = documentQueryFolderURL!.appendingPathComponent(queryFileName)
                                        if(originalChartSource != nil && FileManager.default.fileExists(atPath: (queryDeffilepreviousURL.path)))
                                        {
                                            previousFileExists = true
                                        }
                                        
                                        // Warning: Force a download for now
                                        previousFileExists = false
                                        
                                        // if there is no originalChartSource or the versions # are different or the previous version file does not exist, start the download
                                        if originalChartSource == nil || originalChartSource!.version != chartSource.version || !previousFileExists
                                        {
                                            let urlChart : URL = URL.init(string: "\(self.baseURLString)\(chartSource.endpoint!)")!
                                            let endpointTask : URLSessionDataTask = self.session.dataTask(with: urlChart) { (endpointData, endpointResponse, endpointError) in
                                                
                                                var chartArray : [[String:Any]]?
                                                do
                                                {
                                                    let stringVal = String.init(data: endpointData!, encoding: String.Encoding.utf8)
                                                    chartArray = try JSONSerialization.jsonObject(with: endpointData!, options: .allowFragments) as? [[String:Any]]
                                                    print("Count for QueryDefs - \(chartSource.name) - \(chartArray!.count)")
                                                    if( chartSource.name == "cntFixtureLocatedByDay")
                                                    {
                                                        print("Chart String Data - cntFixtureLocatedByDay - \(stringVal)");
                                                        print("Chart Data - cntFixtureLocatedByDay - \(chartArray)");
                                                    }
                                                }
                                                catch
                                                {
                                                    chartArray = nil
                                                    print(error.localizedDescription)

                                                    loadQueryDefsFromCache()
                                                }
                                                
                                                if let chartDataArray = chartArray
                                                {
                                                    // Save the data to the staging folder
                                                    if(NSKeyedArchiver.archiveRootObject(chartDataArray, toFile: (queryDeffileURL?.path)!))
                                                    {
                                                        // This process completed successfully, process the next item or all is done
                                                        completeProcess()
                                                    }
                                                    else
                                                    {
                                                        loadQueryDefsFromCache()
                                                    }
                                                }
                                                else
                                                {
                                                    loadQueryDefsFromCache()
                                                }
                                            }
                                            endpointTask.resume()
                                        }
                                        else
                                        {
                                            if(previousFileExists)
                                            {
                                                // Copy the file download not needed
                                                do
                                                {
                                                    try FileManager.default.moveItem(at: queryDeffilepreviousURL, to: queryDeffileURL!)
                                                    
                                                    completeProcess()
                                                }
                                                catch
                                                {
                                                    print(error.localizedDescription)
                                                    // Error try to load from cache
                                                    loadQueryDefsFromCache()
                                                }
                                            }
                                            else
                                            {
                                                // No previous file try and load from cache
                                                loadQueryDefsFromCache()
                                            }
                                        }
                                    }
                                    queues.append(getEndpoint)
                                }
                                
                                // Run the first block
                                let block = queues[0]
                                block()
                                /////
                            }
                            else
                            {
                                // No charts sources were added to the list
                                loadQueryDefsFromCache()
                            }
                        }
                        else
                        {
                            // The downloaded data is not present
                            loadQueryDefsFromCache()
                        }
                    }
                    else
                    {
                        // Unable to download data
                        loadQueryDefsFromCache()
                    }
                }
                
                task.resume()
            }
            else
            {
                // Network not available, load from cache
                loadQueryDefsFromCache()
            }
        })
    }
    
    func loadChartPages(completion : @escaping (_ successful : Bool)->Void ) {
        
        var pass = false
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        self.dataLoaded = false
        
        self.loadChartPageIndex()
        
        do
        {
            let files = try FileManager.default.contentsOfDirectory(atPath: (documentsURL?.path)!)
            for file : String in files
            {
                if(file.hasSuffix(".chartpage"))
                {
//                    print(file)
                    
                    let chartPageDataObjectFound : ChartPageDataObject? = ChartPageDataObject.loadChartPage(filename: file)
                    if let chartPageDataObject = chartPageDataObjectFound
                    {
                        self.chartPagesDictionary[chartPageDataObject.identifier!] = chartPageDataObject
                    }
                }
            }
            
            // Will validate index and save
            self.saveChartPageIndex()
            
            pass = true
        }
        catch
        {
            print(error.localizedDescription)
        }
        
        completion(pass)
        self.dataLoaded = true
    }
    
    private func validateIndex(){
        
        var toDelete : [String] = []
        for chartIdentifier : String in self.chartPagesIndex
        {
            let chartPageDataObject = self.chartPagesDictionary[chartIdentifier]
            if(chartPageDataObject == nil)
            {
                toDelete.append(chartIdentifier)
            }
        }
        
        for chartIdentifier in toDelete
        {
            let index = self.chartPagesIndex.index(of: chartIdentifier)
            self.chartPagesIndex.remove(at: index!)
        }
    }
}

