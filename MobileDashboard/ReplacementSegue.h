//
//  ReplacementSegue.h
//  Dash-Ex
//
//  Created by Christopher Nelson on 12/15/15.
//  Copyright © 2015 Anderson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReplacementSegue : UIStoryboardSegue

@property (assign, nonatomic) BOOL forwardDirection;
@property (nonatomic, weak) UIViewController* previousViewController;
@property (nonatomic, assign) BOOL animate;
@property (nonatomic, strong) void (^completion)(BOOL successful);

@end
