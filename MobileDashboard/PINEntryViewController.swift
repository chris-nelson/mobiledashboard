//
//  PINEntryViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/8/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

protocol PinEntryViewControllerDelegate : class
{
    func pinEntrySuccessCode(pinCode : String, pinMode : CLPinMode)
    func pinEntryFailure(count : Int)
    func pinEntryCancel()
}

class PINEntryViewController: UIViewController, CLPinViewDelegate {

    @IBOutlet weak var pinView: CLPinView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var clearPinButton: UIBarButtonItem!
    
    var allowClearing : Bool = true
    var allowCancel : Bool = true
    
    weak var delegate : PinEntryViewControllerDelegate?
    
    var pinMode : CLPinMode = CLPinMode.inputPin
    var pinCode : String?
    var pinCodeToCheck : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.pinView.secureMode = true
        self.pinView.pinMode = self.pinMode
        self.pinView.pinBorder = 1
        self.pinView.pinBorderColor = UIColor.black
        
        if(self.pinView.pinMode == CLPinMode.readOnlyPin)
        {
            self.title = "View PIN";
            pinView.secureMode = false
            pinView.pinCode = self.pinCode
        }
        else if(self.pinView.pinMode == CLPinMode.inputPin)
        {
            self.title = "Enter PIN";
            self.messageLabel.text = "Enter PIN";
        }
        else if(self.pinView.pinMode == CLPinMode.createPin)
        {
            self.title = "Create PIN";
            self.messageLabel.text = "Enter New PIN";
        }
        else if(self.pinView.pinMode == CLPinMode.verifyPin)
        {
            self.title = "Verify PIN";
            self.messageLabel.text = "Enter PIN";
            self.pinView.pinCodeToCheck = self.pinCodeToCheck
        }
        else if(self.pinView.pinMode == CLPinMode.changePin)
        {
            self.title = "Change PIN";
            self.messageLabel.text = "Enter Current PIN";
            self.pinView.pinCodeToCheck = self.pinCodeToCheck
        }

        if(self.allowClearing)
        {
            self.clearPinButton.isEnabled = true
        }
        else
        {
            self.clearPinButton.isEnabled = false
        }
        
        if(self.allowCancel)
        {
            self.doneButton.isEnabled = true
        }
        else
        {
            self.doneButton.isEnabled = false
        }
        
        self.pinView.delegate = self
        
        self.pinView.displayKeyboard()
    }

    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
    
        self.pinView.dismissKeyboard()
        self.delegate?.pinEntryCancel()
    }
    
    @IBAction func clearButtonPressed(_ sender: UIBarButtonItem) {
    
        DashboardSettingsObject.shared.userPIN = nil
        self.pinView.dismissKeyboard()
        self.delegate?.pinEntryCancel()
    }
    
    func dismissPINEntry() {
        
        self.delegate?.pinEntrySuccessCode(pinCode: DashboardSettingsObject.shared.userPIN!, pinMode: self.pinMode)
    }
    
    func pinCodeViewController(_ pinView: CLPinView!, didInputPinCode pinCode: String!) {
        
    }
    
    func pinCodeViewController(_ pinView: CLPinView!, didCreatePinCode pinCode: String!) {
        
        DashboardSettingsObject.shared.userPIN = pinCode

    }
    
    func pinCodeViewController(_ pinView: CLPinView!, didVerifiedPincodeSuccessfully pinCode: String!) {
        
    }
    
    func pinCodeViewController(_ pinView: CLPinView!, didFailVerificationWithCount failsCount: UInt) {
        
    }
    
    func pinCodeViewController(_ pinView: CLPinView!, didChangePinCode pinCode: String!) {
        
        DashboardSettingsObject.shared.userPIN = pinCode
    }
    
    func showChangeThePinMessage() {

        self.messageLabel.text = "PIN Code Verified, Enter New PIN"

    }
    
    func showPinsDontMatchMessage() {
        
        if(self.pinView.pinMode == CLPinMode.changePin)
        {
            self.messageLabel.text = "PIN Does Not match, please enter PIN again"
        }
        else if(self.pinView.pinMode == CLPinMode.verifyPin)
        {
            self.messageLabel.text = "PIN Does Not match, try again"
        }
        else
        {
            self.messageLabel.text = "PINS Do Not match, please enter new PIN again"
        }
    }
    
    func showPinCreatedMessage() {
        
        if(self.pinView.pinMode == CLPinMode.changePin)
        {
            self.messageLabel.text = "PIN Changed";
        }
        else
        {
            self.messageLabel.text = "PIN Created";
        }
        
        self.pinView.dismissKeyboard()

        self.perform(#selector(dismissPINEntry), with: nil, afterDelay: 2.0)
    }
    
    func showEnterOnceAgainMessage() {
        
        self.messageLabel.text = "Enter the PIN Again";

    }

    func showPinVerifiedMessage() {
        
        self.messageLabel.text = "PIN Code Verified"

        self.pinView.dismissKeyboard()
        
        self.perform(#selector(dismissPINEntry), with: nil, afterDelay: 1.0)

    }
    
    func showPinsNoAvailableMessage() {
        
        self.messageLabel.text = "Active PIN not entered"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
