//
//  SaveChartViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/11/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class SaveChartViewController: UIViewController, EntryFieldDelegate, SaveChartDelegate, UITextFieldDelegate {

    var chartPageObject : ChartPageDataObject?
    weak var chartDelegate : ChartConfigurationDelegate?

    @IBOutlet weak var chartNameTextField: UITextField!
    var errorMessage : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.chartNameTextField.delegate = self
        self.chartNameTextField.text = chartPageObject!.name
        // Do any additional setup after loading the view.
        
        if let chartName = chartPageObject!.name
        {
            if(chartName.lengthOfBytes(using: .utf8) == 0)
            {
                self.chartNameTextField.becomeFirstResponder()
            }
        }
        else
        {
            self.chartNameTextField.becomeFirstResponder()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleKeyboardButtonPressed(_ sender: UIButton) {
        
        if(self.chartNameTextField.isFirstResponder)
        {
            self.chartNameTextField.resignFirstResponder()
        }
        else
        {
            self.chartNameTextField.becomeFirstResponder()
        }
        
    }
    
    func entryValid() -> Bool
    {
        var pass : Bool = true
        
        self.chartPageObject!.name = self.chartNameTextField.text
        
        if let stringValue = self.chartPageObject!.name
        {
            if(stringValue.lengthOfBytes(using: String.Encoding.utf8) == 0)
            {
                pass = false
                self.errorMessage = "Please Select a Chart Name"
            }
        }
        else
        {
            pass = false
            self.errorMessage = "Please Select a Chart Name"
        }
        
        return pass
    }
    
    func displayErrorMessage ()
    {
        if(!self.entryValid())
        {
            let alertController : UIAlertController = UIAlertController.init(title: "Error", message: self.errorMessage, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    func saveChart(completion : ((_ successful : Bool)->Void)?)
    {
        self.chartPageObject!.saveChartPage(completion: { (successful) in
            
            if(successful)
            {
                if(self.chartPageObject!.isNew)
                {
                    DashboardDataObject.shared.add(chartPage: self.chartPageObject!)
                }
            }
            
            if(completion != nil)
            {
                completion!(successful)
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == self.chartNameTextField)
        {
            self.chartDelegate?.moveNext()
        }
        
        return true
    }
}
