//
//  ChartGroupingTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/26/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartGroupingTableViewController: UITableViewController, EntryFieldDelegate {

    var localQueryParamArray : [ChartFieldParameterObject] = []

    weak var chartDelegate : ChartConfigurationDelegate?
    var chartDataSource : ChartDataSource?
    var chartDataObject : ChartDataObject?
    var errorMessage : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

//        let counterParam = ChartFieldParameterObject.init()
//        counterParam.name = "[No Grouping]"
//        localQueryParamArray.append(counterParam)

        for (_, fieldParam) in self.chartDataSource!.fieldParameters
        {
            if(fieldParam.filterType == .localOnly || fieldParam.filterType == .localAndRemote)
            {
                localQueryParamArray.append(fieldParam)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.localQueryParamArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupingCell", for: indexPath)
        
        // Configure the cell...
        let queryParam = self.localQueryParamArray[indexPath.row]
        
        cell.textLabel?.text = queryParam.name
        
        if(queryParam.name == self.chartDataObject?.groupingColumn)
        {
            cell.textLabel?.textColor = UIColor.blue
        }
        else
        {
            cell.textLabel?.textColor = UIColor.black
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let chartDelegate = self.chartDelegate
        {
            let queryParam = self.localQueryParamArray[indexPath.row]
            self.chartDataObject?.groupingColumn = queryParam.name
            
            chartDelegate.moveNext()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func entryValid() -> Bool
    {
        var pass : Bool = true
        
        if let stringValue = self.chartDataObject?.groupingColumn
        {
            if(stringValue.lengthOfBytes(using: String.Encoding.utf8) == 0)
            {
                pass = false
                self.errorMessage = "Please Select a value for the Grouping"
            }
        }
        else
        {
            pass = false
            self.errorMessage = "Please Select a value for the Grouping"
        }
        
        return pass
    }
    
    func displayErrorMessage ()
    {
        if(!self.entryValid())
        {
            let alertController : UIAlertController = UIAlertController.init(title: "Error", message: self.errorMessage, preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
