//
//  ChartDataSource.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/9/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartDataSource: NSObject, NSCoding {

    var identifier : Int = 0
    var name : String?
    var version : Int = 0
    var endpoint : String?
    var fieldParameters : [String:ChartFieldParameterObject] = [:]

    override init() {
        
        super.init()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        
//        START HERE!!!!!!!!
// Warning: the identifier and version are not being reloaded causing the version comparisom to always fail
        self.identifier = aDecoder.decodeInteger(forKey: "identifier")
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.version = aDecoder.decodeInteger(forKey: "version")
        self.endpoint = aDecoder.decodeObject(forKey: "endpoint") as? String ?? ""
        self.fieldParameters = aDecoder.decodeObject(forKey: "fieldParameters") as? [String:ChartFieldParameterObject] ?? [:]
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.identifier, forKey: "identifier")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.version, forKey: "version")
        aCoder.encode(self.endpoint, forKey: "endpoint")
        aCoder.encode(self.fieldParameters, forKey: "fieldParameters")
    }

    func validateChartSource() -> Bool {
        
        var pass = true;
        
        if(self.identifier == 0)
        {
            pass = false;
            print("Chart Data Source Error - No Identifier")
        }
        
        if(name?.lengthOfBytes(using: String.Encoding.utf8) == 0)
        {
            pass = false;
            print("Chart Data Source Error - No Name")
        }
        
        if(self.version == 0)
        {
            pass = false;
            print("Chart Data Source Error - No Version")
        }
        
        if(endpoint?.lengthOfBytes(using: String.Encoding.utf8) == 0)
        {
            pass = false;
            print("Chart Data Source Error - No Endpoint")
        }
        
        if(fieldParameters.count == 0)
        {
            pass = false;
            print("Chart Data Source Error - No Field Parameters")
        }
        
        return pass
    }
    
    override var description: String {
        
        get {
        
            let desc = "\nName: \(self.name!)\nID: \(self.identifier)\nVersion: \(self.version)\nEndpoint \(self.endpoint!)\nFieldParameters:\n\(self.fieldParameters)"
//\n\nFieldData: \(self.data)
            return desc
        }
    }
    
    func saveToFile () ->Bool {
        
        return false;
    }
}
