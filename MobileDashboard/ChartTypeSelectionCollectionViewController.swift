//
//  ChartTypeSelectionCollectionViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/4/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ChartTypeSelectionCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let preferredWidthHeight : Int = 300
    var chartDataObject : ChartDataObject?
    weak var chartDelegate : ChartConfigurationDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        let indexPath = IndexPath.init(item: self.chartDataObject!.chartType.rawValue, section: 0)
        self.collectionView?.selectItem(at: indexPath, animated: false, scrollPosition: .centeredVertically)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ChartType.count()
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chartTypeCell", for: indexPath) as! ChartTypeCollectionViewCell
    
        let chartType = ChartType(rawValue: indexPath.row)
        // Configure the cell
        cell.chartTypeNameLabel.text = chartType?.name()
        cell.chartTypeImageView.image = UIImage.init(named: (chartType?.imageName())!)
        
        if(self.chartDataObject!.chartType.rawValue == chartType?.rawValue)
        {
            cell.backgroundColor = .blue
        }
        else
        {
            cell.backgroundColor = .white
        }
    
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //    NSLog(@" CollectionView width %f", collectionView.frame.size.width);
        var count : Int = Int(Int(collectionView.frame.size.width) / Int(preferredWidthHeight));
        let mod : Int = Int(Int(collectionView.frame.size.width) % Int(preferredWidthHeight));
        if(mod > 10)
        {
            count += 1;
        }
        let newWidth : Int = Int((Int(collectionView.frame.size.width) / count) - 10);
        
        return CGSize(width:newWidth, height:newWidth);
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let chartDelegate = self.chartDelegate
        {
            let chartType = ChartType(rawValue: indexPath.row)
            chartDelegate.selectChartType(chartType: chartType!, action: .MoveForward)
        }

        
    }
}
