#import <UIKit/UIKit.h>

@interface OverlayTransitioningDelegate : NSObject <UIViewControllerTransitioningDelegate>
@end

@interface OverlayAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic) BOOL isPresentation;

@end

