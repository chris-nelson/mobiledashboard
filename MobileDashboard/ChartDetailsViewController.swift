//
//  ChartDetailsViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/25/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartDetailsViewController: UIViewController, ColorSelectionDelegate, UITextFieldDelegate {

    var chartDataObject : ChartDataObject?
    weak var chartDelegate : ChartConfigurationDelegate?
    
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var animationSegmentedControl: UISegmentedControl!
    @IBOutlet weak var chartBackgroundColorView: UIView!
    @IBOutlet weak var chartLabelLocationSegmentedControl: UISegmentedControl!
    @IBOutlet weak var xAxisCountTextField: UITextField!
    @IBOutlet weak var xAxisLabel: UILabel!
    @IBOutlet weak var xAxisStepper: UIStepper!
    
    @IBOutlet weak var gridLinesLabel: UILabel!
    @IBOutlet weak var gridLinesColorLabel: UILabel!
    @IBOutlet weak var gridLinesColorView: UIView!
    @IBOutlet weak var gridLinesColorButton: UIButton!
    
    @IBOutlet weak var gridLinesSegmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.descriptionTextField.text = self.chartDataObject?.chartDescription
        self.descriptionTextField.delegate = self
        
        self.animationSegmentedControl.selectedSegmentIndex = (self.chartDataObject?.animationType.rawValue)!
        
        self.chartBackgroundColorView.layer.borderColor = UIColor.black.cgColor;
        self.chartBackgroundColorView.layer.borderWidth = 1;
        self.chartBackgroundColorView.backgroundColor = chartDataObject?.chartBackgroundColor
        self.xAxisCountTextField.text = String(describing: self.chartDataObject!.xAxisLabelsCount)
        self.xAxisStepper.value = Double(self.chartDataObject!.xAxisLabelsCount)
        
        chartLabelLocationSegmentedControl.removeAllSegments()
        
        if(chartDataObject?.chartType == .barChart   || chartDataObject?.chartType == .horizonatalBarChart || chartDataObject?.chartType == .stackedBarChart   || chartDataObject?.chartType == .horizonatalStackedBarChart)
        {
            chartLabelLocationSegmentedControl.insertSegment(withTitle: "Hide", at: 0, animated: false)
            chartLabelLocationSegmentedControl.insertSegment(withTitle: "Above", at: 1, animated: false)
            chartLabelLocationSegmentedControl.insertSegment(withTitle: "Below", at: 2, animated: false)
        }
        else
        {
            chartLabelLocationSegmentedControl.insertSegment(withTitle: "Hide", at: 0, animated: false)
            chartLabelLocationSegmentedControl.insertSegment(withTitle: "Show", at: 1, animated: false)
            
            if(self.chartDataObject?.displayValueLabelLocation == .showBelow)
            {
                self.chartDataObject?.displayValueLabelLocation = .showAbove
            }
        }
        
        if(chartDataObject?.chartType == .barChart   || chartDataObject?.chartType == .horizonatalBarChart || chartDataObject?.chartType == .stackedBarChart   || chartDataObject?.chartType == .horizonatalStackedBarChart || chartDataObject?.chartType == .singleLineChart || chartDataObject?.chartType == .lineChart || chartDataObject?.chartType == .scatterChart)
        {
            self.gridLinesLabel.isHidden = false
            self.gridLinesSegmentedControl.isHidden = false
            self.gridLinesSegmentedControl.selectedSegmentIndex = self.chartDataObject!.gridLinesType.rawValue
            self.gridLinesColorLabel.isHidden = false
            self.gridLinesColorButton.isHidden = false
            self.gridLinesColorView.isHidden = false
            self.gridLinesColorView.layer.borderColor = UIColor.black.cgColor;
            self.gridLinesColorView.layer.borderWidth = 1;
            self.gridLinesColorView.backgroundColor = self.chartDataObject?.gridLineColor

            self.xAxisCountTextField.isHidden = false
            self.xAxisLabel.isHidden = false
            self.xAxisStepper.isHidden = false
        }
        else
        {
            self.gridLinesLabel.isHidden = true
            self.gridLinesSegmentedControl.isHidden = true
            self.gridLinesColorLabel.isHidden = true
            self.gridLinesColorButton.isHidden = true
            self.gridLinesColorView.isHidden = true
            
            self.xAxisCountTextField.isHidden = true
            self.xAxisLabel.isHidden = true
            self.xAxisStepper.isHidden = true
        }
        
        chartLabelLocationSegmentedControl.selectedSegmentIndex = (self.chartDataObject?.displayValueLabelLocation.rawValue)!
        
//        if let desc = self.chartDataObject?.chartDescription
//        {
//            if(desc.lengthOfBytes(using: .utf8) == 0)
//            {
//                self.descriptionTextField.becomeFirstResponder()
//            }
//        }
//        else
//        {
//            self.descriptionTextField.becomeFirstResponder()
//        }
    }
    
    @IBAction func descriptionTextFieldEditingChanged(_ sender: UITextField) {
    
        self.chartDataObject!.chartDescription = self.descriptionTextField.text

    }
    
    @IBAction func chartLabelLocationValueChanged(_ sender: UISegmentedControl) {
        
        if(sender.selectedSegmentIndex == 1)
        {self.chartDataObject?.displayValueLabelLocation = .showAbove
            
        }
        else if(sender.selectedSegmentIndex == 2)
        {
            self.chartDataObject?.displayValueLabelLocation = .showBelow
        }
        else
        {
            self.chartDataObject?.displayValueLabelLocation = .hide
        }
    }
    
    @IBAction func xAxisCountStepperValueChanged(_ sender: UIStepper)
    {
        self.chartDataObject!.xAxisLabelsCount = Int(sender.value)
        self.xAxisCountTextField.text = String(describing: self.chartDataObject!.xAxisLabelsCount)
    }
    
    @IBAction func gridLinesValueChanged(_ sender: UISegmentedControl) {
        
        self.chartDataObject!.gridLinesType = ChartGridLinesType(rawValue: sender.selectedSegmentIndex)!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(textField == self.descriptionTextField)
        {
            self.descriptionTextField.resignFirstResponder()
        }
        
        return true
    }

    @IBAction func toggleKeyboardButtonPressed(_ sender: UIButton) {
        
        if(self.descriptionTextField.isFirstResponder)
        {
            self.descriptionTextField.resignFirstResponder()
        }
        else
        {
            self.descriptionTextField.becomeFirstResponder()
        }
        
    }

    @IBAction func animationTypeChanged(_ sender: UISegmentedControl) {
        
        self.chartDataObject?.animationType = ChartAnimationType(rawValue: sender.selectedSegmentIndex)!
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "colorSelectionSegue")
        {
            let vc = segue.destination as! ColorSelectionViewController
            vc.enableSaveButton = true
            vc.enableCloseButton = true
            vc.hideClearColorButton = true
            vc.selectedColor = self.chartDataObject?.chartBackgroundColor
            
            vc.titleOverride = "Background Color"
            vc.view.tag = 1
            vc.delegate = self
        }
        else if(segue.identifier == "gridColorSelectionSegue")
        {
            let vc = segue.destination as! ColorSelectionViewController
            vc.enableSaveButton = true
            vc.enableCloseButton = true
            vc.hideClearColorButton = true
            vc.selectedColor = self.chartDataObject?.gridLineColor
            
            vc.titleOverride = "Grid Color"
            vc.view.tag = 2
            vc.delegate = self
        }
    }

    // MARK: - ColorSelectionDelegate
    func saveColorSelection(_ colorSelectionViewController: ColorSelectionViewController!, color: UIColor!) {
        
        if(colorSelectionViewController.view.tag == 1)
        {
            self.chartDataObject?.chartBackgroundColor = color
            self.chartBackgroundColorView.backgroundColor = self.chartDataObject?.chartBackgroundColor
        }
        else if(colorSelectionViewController.view.tag == 2)
        {
            self.chartDataObject?.gridLineColor = color
            self.gridLinesColorView.backgroundColor = self.chartDataObject?.gridLineColor
        }
        
        self.navigationController!.popViewController(animated: true)
    }
    
    func cancelColorSelection(_ colorSelectionViewController: ColorSelectionViewController!) {
        
        self.navigationController!.popViewController(animated: true)
    }

}
