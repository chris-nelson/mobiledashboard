//
//  FourWayViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/29/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

protocol ChartPageDelegate : class {
    
    func chartRenderComplete(chartViewController : ChartViewController)
    
}

class FourWayViewController: UIViewController, ConfigurationDelegate, ChartPageDelegate {

    var chartPageObject : ChartPageDataObject?
    var displayMode = false;
    var addButton: UIBarButtonItem =  UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed))
    var editButton: UIBarButtonItem =  UIBarButtonItem.init(title: "Edit", style: .plain, target: self, action: #selector(editButtonPressed))
    var deleteButton: UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .trash, target: self, action: #selector(deleteButtonPressed))
    var orderButton: UIBarButtonItem =  UIBarButtonItem.init(title: "Order", style: .plain, target: self, action: #selector(orderButtonPressed))
    var closeButton: UIBarButtonItem =  UIBarButtonItem.init(title: "Close", style: .plain, target: self, action: #selector(closeButtonPressed))
    var shareButton: UIBarButtonItem =  UIBarButtonItem.init(barButtonSystemItem: .action, target: self, action: #selector(shareButtonPressed))
    var orderMode = false;
    var addMode = false;

    enum ScreenLayout: Int {
        
        case TwoByTwo
        case TwoByOne
        case OneByOne1
        case OneByOne2
        case OneByOne4
        case OneByOne3
    }

    var containerViewController1 : ContainerViewController?
    var containerViewController2 : ContainerViewController?
    var containerViewController3 : ContainerViewController?
    var containerViewController4 : ContainerViewController?

    @IBOutlet private weak var container1: UIView!
    @IBOutlet private weak var container2: UIView!
    @IBOutlet private weak var container3: UIView!
    @IBOutlet private weak var container4: UIView!
    
    @IBOutlet private weak var containerTwoWidth: NSLayoutConstraint!
    @IBOutlet private weak var containerThreeHeight: NSLayoutConstraint!
    
    var screenLayout = ScreenLayout.OneByOne1 {
        
        didSet{
            self.updateChartInteraction()
        }
    }
    
    deinit {
        
        var foo = "abc"
        print("deinit - FourWayViewController")
        foo = "abcd"
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let tapContainer1 = UITapGestureRecognizer(target: self, action:#selector(tapContainerFunc1))
        self.container1.addGestureRecognizer(tapContainer1)
        
        let tapContainer2 = UITapGestureRecognizer(target: self, action:#selector(tapContainerFunc2))
        self.container2.addGestureRecognizer(tapContainer2)
        
        let tapContainer3 = UITapGestureRecognizer(target: self, action:#selector(tapContainerFunc3))
        self.container3.addGestureRecognizer(tapContainer3)
        
        let tapContainer4 = UITapGestureRecognizer(target: self, action:#selector(tapContainerFunc4))
        self.container4.addGestureRecognizer(tapContainer4)
        
        self.addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addButtonPressed))
        self.editButton =  UIBarButtonItem.init(title: "Edit", style: .plain, target: self, action: #selector(editButtonPressed))
        self.deleteButton = UIBarButtonItem.init(barButtonSystemItem: .trash, target: self, action: #selector(deleteButtonPressed))
        self.orderButton =  UIBarButtonItem.init(title: "Order", style: .plain, target: self, action: #selector(orderButtonPressed))
        self.closeButton = UIBarButtonItem.init(title: "Close", style: .plain, target: self, action: #selector(closeButtonPressed))
        self.shareButton =  UIBarButtonItem.init(barButtonSystemItem: .action, target: self, action: #selector(shareButtonPressed))
        
        self.updateButtons()
    }
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
        self.updateScreenLayout(animate: false) 
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.title = self.chartPageObject?.name
    }

    override func viewDidAppear(_ animated: Bool) {
    
        super.viewDidAppear(animated)
        
        if(!self.displayMode)
        {
            self.refreshCharts(changeLayout: true)
        }
    }
    
    @objc private func tapContainerFunc1(recognizer : UITapGestureRecognizer) {
        
        if(!self.orderMode)
        {
            if (self.chartPageObject!.chartCount() > 2)
            {
                if(self.screenLayout == ScreenLayout.OneByOne1)
                {
                    self.screenLayout = ScreenLayout.TwoByTwo;
                }
                else
                {
                    self.screenLayout = ScreenLayout.OneByOne1;
                }
                self.updateScreenLayout(animate: true)
            }
            else if (self.chartPageObject!.chartCount() > 1)
            {
                if(self.screenLayout == ScreenLayout.OneByOne1)
                {
                    self.screenLayout = ScreenLayout.TwoByOne;
                }
                else
                {
                    self.screenLayout = ScreenLayout.OneByOne1;
                }
                self.updateScreenLayout(animate: true)
            }
        }
    }
    
    @objc private func tapContainerFunc2(recognizer : UITapGestureRecognizer) {
        
        if(self.orderMode)
        {
            self.chartPageObject?.swapChartPosition(atSourceIndex: 1, forDestinationIndex: 0)
            self.chartPageObject!.deleteCacheImage()
            self.chartPageObject!.saveChartPage(completion: { (successful) in
                
                self.refreshCharts(changeLayout: true)
                
            })
        }
        else
        {
            if (self.chartPageObject!.chartCount() > 2)
            {
                if(self.screenLayout == ScreenLayout.OneByOne2)
                {
                    self.screenLayout = ScreenLayout.TwoByTwo;
                }
                else
                {
                    self.screenLayout = ScreenLayout.OneByOne2;
                }
                
                self.updateScreenLayout(animate: true)
            }
            else if (self.chartPageObject!.chartCount() > 1)
            {
                if(self.screenLayout == ScreenLayout.OneByOne2)
                {
                    self.screenLayout = ScreenLayout.TwoByOne;
                }
                else
                {
                    self.screenLayout = ScreenLayout.OneByOne2;
                }
                
                self.updateScreenLayout(animate: true)
            }
        }
    }
    
    @objc private func tapContainerFunc3(recognizer : UITapGestureRecognizer) {
        
        if(self.orderMode)
        {
            self.chartPageObject?.swapChartPosition(atSourceIndex: 2, forDestinationIndex: 0)
            self.chartPageObject!.deleteCacheImage()
            self.chartPageObject!.saveChartPage(completion: { (successfule) in
                
                self.refreshCharts(changeLayout: true)
                
            })
        }
        else
        {
            // if tapping on the 3rd item you must have more than 2 charts
            if(self.screenLayout == ScreenLayout.OneByOne3)
            {
                self.screenLayout = ScreenLayout.TwoByTwo;
            }
            else
            {
                self.screenLayout = ScreenLayout.OneByOne3;
            }
            
            self.updateScreenLayout(animate: true)
        }
    }
    
    @objc private func tapContainerFunc4(recognizer : UITapGestureRecognizer) {
        
        if(self.orderMode)
        {
            if (self.chartPageObject!.chartCount() > 3)
            {
                self.chartPageObject?.swapChartPosition(atSourceIndex: 3, forDestinationIndex: 0)
                self.chartPageObject!.deleteCacheImage()
                self.chartPageObject!.saveChartPage(completion: { (successfule) in
                    
                    self.refreshCharts(changeLayout: true)
                    
                })
            }
        }
        else
        {
            if (self.chartPageObject!.chartCount() > 3)
            {
                // if tapping on the 4th item you must have more than 4 charts
                if(self.screenLayout == ScreenLayout.OneByOne4)
                {
                    self.screenLayout = ScreenLayout.TwoByTwo;
                }
                else
                {
                    self.screenLayout = ScreenLayout.OneByOne4;
                }
                
                self.updateScreenLayout(animate: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addButtonPressed(_ sender: UIBarButtonItem) {
        
        CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
            
            if(successful)
            {
                if(DashboardDataObject.shared.isConfigurationDataAvailable())
                {
                    self.performSegue(withIdentifier: "addChartSegue", sender: self)
                }
                else
                {
                    let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Configuration data is unavailable please log out and log in again", preferredStyle: .alert)
                    let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to add charts while offline", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func editButtonPressed(_ sender: UIBarButtonItem) {
        
        CLNetworkStatusCheck.networkStatusCheck(completionhandler: { (successful : Bool) in
            
            if(successful)
            {
                if(DashboardDataObject.shared.isConfigurationDataAvailable())
                {
                    self.performSegue(withIdentifier: "editChartSegue", sender: self)
                }
                else
                {
                    let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Configuration data is unavailable please log out and log in again", preferredStyle: .alert)
                    let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to edit charts while offline", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func deleteButtonPressed(_ sender: UIBarButtonItem) {
        
        let alertController : UIAlertController = UIAlertController.init(title: "Confirm", message: "Are you sure you wish to delete the current chart?", preferredStyle: .alert)

        let yesAction = UIAlertAction.init(title: "Yes", style: .default, handler: { (action) in
            
            DispatchQueue.main.async {
                
                var pass = false
                
                if(self.screenLayout == .OneByOne1)
                {
                    pass = self.chartPageObject!.removeChart(atLocation: 0)
                }
                else if(self.screenLayout == .OneByOne2)
                {
                    pass = self.chartPageObject!.removeChart(atLocation: 1)
                }
                else if(self.screenLayout == .OneByOne3)
                {
                    pass = self.chartPageObject!.removeChart(atLocation: 2)
                }
                else if(self.screenLayout == .OneByOne4)
                {
                    pass = self.chartPageObject!.removeChart(atLocation: 3)
                }
                
                if(pass)
                {
                    self.chartPageObject!.deleteCacheImage()
                    self.chartPageObject!.saveChartPage(completion: { (successfule) in

                        self.refreshCharts(changeLayout: true)

                    })
                }
            }
        })
        alertController.addAction(yesAction)
        let noAction = UIAlertAction.init(title: "No", style: .default, handler: nil)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    func shareButtonPressed(_ sender: UIBarButtonItem) {
    
        DashboardRootViewController.rootViewController().displayShare(chartPage: self.chartPageObject!, forButton: sender)
    }
    
    func orderButtonPressed(_ sender: UIBarButtonItem) {
        
        self.orderMode = !self.orderMode
        
        self.updateButtons()
    }
    
    func closeButtonPressed(_ sender: UIBarButtonItem) {
        
        if(self.chartPageObject!.chartCount() >= 3)
        {
            self.screenLayout = ScreenLayout.TwoByTwo;
        }
        else if(self.chartPageObject!.chartCount() >= 1)
        {
            self.screenLayout = ScreenLayout.TwoByOne;
        }
            
        self.updateScreenLayout(animate: true)
    }
    
    func updateButtons() {
    
        if(self.chartPageObject!.chartCount() > 1 && (self.screenLayout == .OneByOne1 || self.screenLayout == .OneByOne2 || self.screenLayout == .OneByOne3 || self.screenLayout == .OneByOne4))
        {
            self.navigationItem.leftBarButtonItem = self.closeButton
        }
        else
        {
            self.navigationItem.leftBarButtonItem = nil
        }
        
        if(self.screenLayout == .OneByOne1 && self.chartPageObject!.chartCount() == 1)
        {
            self.navigationItem.rightBarButtonItems = [self.editButton, self.addButton, self.shareButton]
        }
        else if(self.chartPageObject!.chartCount() > 1 && self.chartPageObject!.chartCount() < 4 && (self.screenLayout == .OneByOne1 || self.screenLayout == .OneByOne2 || self.screenLayout == .OneByOne3 || self.screenLayout == .OneByOne4))
        {
            // OneByOne4 is not possible with 2 or 3 charts
            self.navigationItem.rightBarButtonItems = [self.editButton, self.deleteButton]
        }
        else if(self.chartPageObject!.chartCount() > 1 && self.chartPageObject!.chartCount() < 4 && (self.screenLayout == .TwoByTwo || self.screenLayout == .TwoByOne))
        {
            if(self.orderMode)
            {
                self.orderButton.title = "Done"
                self.navigationItem.rightBarButtonItems = [self.orderButton]
            }
            else
            {
                self.orderButton.title = "Order"
                self.navigationItem.rightBarButtonItems = [self.orderButton, self.addButton, self.shareButton]
            }
        }
        else if(self.chartPageObject!.chartCount() >= 4)
        {
            if (self.screenLayout == .OneByOne1 || self.screenLayout == .OneByOne2 || self.screenLayout == .OneByOne3 || self.screenLayout == .OneByOne4)
            {
                self.navigationItem.rightBarButtonItems = [self.editButton, self.deleteButton]
            }
            else if (self.screenLayout == .TwoByTwo || self.screenLayout == .TwoByOne)
            {
                if(self.orderMode)
                {
                    self.orderButton.title = "Done"
                    self.navigationItem.rightBarButtonItems = [self.orderButton]
                }
                else
                {
                    self.orderButton.title = "Order"
                    self.navigationItem.rightBarButtonItems = [self.orderButton, self.shareButton]
                }
            }
        }
        updateChartInteraction()
    }
    
    func updateScreenLayout(animate : Bool) {
        
        if(animate)
        {
             self.view.layoutIfNeeded()
        }
        
        let screenWidth = self.view.frame.size.width
        let screenHeight = self.view.frame.size.height
        
        if(self.screenLayout == ScreenLayout.TwoByTwo)
        {
            self.containerTwoWidth.constant = screenWidth / 2;
            self.containerThreeHeight.constant = screenHeight / 2;
        }
        else if(self.screenLayout == ScreenLayout.TwoByOne)
        {
            if(self.view.frame.size.width > self.view.frame.size.height)
            {
                self.containerTwoWidth.constant = screenWidth / 2;
                self.containerThreeHeight.constant = 0;
            }
            else
            {
                self.containerTwoWidth.constant = 0;
                self.containerThreeHeight.constant = screenHeight / 2;
            }
        }
        else if(self.screenLayout == ScreenLayout.OneByOne1)
        {
            self.containerTwoWidth.constant = 0;
            self.containerThreeHeight.constant = 0;
        }
        else if(self.screenLayout == ScreenLayout.OneByOne2)
        {
            self.containerTwoWidth.constant = screenWidth;
            self.containerThreeHeight.constant = 0;
        }
        else if(self.screenLayout == ScreenLayout.OneByOne3)
        {
            self.containerTwoWidth.constant = 0;
            self.containerThreeHeight.constant = screenHeight;
        }
        else if(self.screenLayout == ScreenLayout.OneByOne4)
        {
            self.containerTwoWidth.constant = screenWidth;
            self.containerThreeHeight.constant = screenHeight;
        }

        if(animate)
        {
            UIView.animate(withDuration: 0.5) {
                
                self.view.layoutIfNeeded()
                
            }
        }
        
        self.updateButtons()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { _ in
            
                self.updateScreenLayout(animate: true)
            
            }, completion: nil)
    
    }
    
    func chartRenderComplete(chartViewController : ChartViewController)
    {
        self.updateChartInteraction()
    }

    func updateChartInteraction()
    {
        let cvc1 = self.containerViewController1?.currentViewController as? ChartViewController
        let cvc2 = self.containerViewController2?.currentViewController as? ChartViewController
        let cvc3 = self.containerViewController3?.currentViewController as? ChartViewController
        let cvc4 = self.containerViewController4?.currentViewController as? ChartViewController
        
        if(self.screenLayout == .TwoByOne || self.screenLayout == .TwoByTwo)
        {
            cvc1?.activeChartView?.isUserInteractionEnabled = false
            cvc2?.activeChartView?.isUserInteractionEnabled = false
            cvc3?.activeChartView?.isUserInteractionEnabled = false
            cvc4?.activeChartView?.isUserInteractionEnabled = false
        }
        else
        {
            cvc1?.activeChartView?.isUserInteractionEnabled = true
            cvc2?.activeChartView?.isUserInteractionEnabled = true
            cvc3?.activeChartView?.isUserInteractionEnabled = true
            cvc4?.activeChartView?.isUserInteractionEnabled = true
        }
        
        cvc1?.resetChartDimensions()
        cvc2?.resetChartDimensions()
        cvc3?.resetChartDimensions()
        cvc4?.resetChartDimensions()
        
    }
    
    func refreshCharts(changeLayout : Bool)
    {
        var cvc = self.containerViewController1?.currentViewController as! ChartViewController
        if (self.chartPageObject!.chartCount() > 0)
        {
            let chart = self.chartPageObject!.getChart(selection: 0)
            cvc.generateChart(chartDataObject: chart!, completion: { (successful) in

                self.updateChartInteraction()
                
            })
        }
        else
        {
            cvc.clearChart()
        }

        cvc = self.containerViewController2?.currentViewController as! ChartViewController
        if (self.chartPageObject!.chartCount() > 1)
        {
            let chart = self.chartPageObject!.getChart(selection: 1)
            cvc.generateChart(chartDataObject: chart!, completion: { (successful) in
                
                self.updateChartInteraction()
                
            })
        }
        else
        {
            cvc.clearChart()
        }

        // Allow the chart to display in space 2 or 3 depending in the device orientation
        cvc = self.containerViewController3?.currentViewController as! ChartViewController
        if (self.chartPageObject!.chartCount() > 1)
        {
            var chart = self.chartPageObject!.getChart(selection: 1)

            if (self.chartPageObject!.chartCount() > 2)
            {
                chart = self.chartPageObject!.getChart(selection: 2)
                cvc.clearChart()
            }
            cvc.generateChart(chartDataObject: chart!, completion: { (successful) in
                
                self.updateChartInteraction()
                
            })
        }
        else
        {
            cvc.clearChart()
        }

        cvc = self.containerViewController4?.currentViewController as! ChartViewController
        if (self.chartPageObject!.chartCount() > 3)
        {
            let chart = self.chartPageObject!.getChart(selection: 3)
            cvc.generateChart(chartDataObject: chart!, completion: { (successful) in
                
                self.updateChartInteraction()
                
            })
        }
        else
        {
            cvc.clearChart()
        }

        if(changeLayout)
        {
            if(self.chartPageObject!.chartCount() == 1)
            {
                self.screenLayout = ScreenLayout.OneByOne1
            }
            else if(self.chartPageObject!.chartCount() == 2)
            {
                self.screenLayout = ScreenLayout.TwoByOne
            }
            else if(self.chartPageObject!.chartCount() > 2)
            {
                self.screenLayout = ScreenLayout.TwoByTwo
            }
            
            self.updateScreenLayout(animate: true)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "EmbedContainer1") {
            
            if(self.containerViewController1 == nil)
            {
                self.containerViewController1 = segue.destination as? ContainerViewController;
                self.containerViewController1!.display(title : "Charts", fromStoryboard: "Main", entryPoint: "ChartViewController", animate: false, initialize:nil, completion: { (finished: Bool,  _ viewController : UIViewController?) -> () in
                    
                    let cvc = viewController as! ChartViewController
                    cvc.chartPageObject = self.chartPageObject
                    cvc.chartPageDelegate = self
                    if(self.displayMode)
                    {
                        cvc.useCache = true
                        cvc.saveCache = true
                        cvc.enableAnimation = false
                        if (self.chartPageObject!.chartCount() > 0)
                        {
                            let chart = self.chartPageObject!.getChart(selection: 0)
                            cvc.generateChart(chartDataObject: chart!, completion:nil)
                        }
                    }
                    
                })
            }
        }
        else if(segue.identifier == "EmbedContainer2") {
            
            if(self.containerViewController2 == nil)
            {
                self.containerViewController2 = segue.destination as? ContainerViewController;
                self.containerViewController2!.display(title : "Charts", fromStoryboard: "Main", entryPoint: "ChartViewController", animate: false, initialize:nil, completion: { (finished: Bool,  _ viewController : UIViewController?) -> () in
                    
                    let cvc = viewController as! ChartViewController
                    cvc.chartPageObject = self.chartPageObject
                    cvc.chartPageDelegate = self
                    if(self.displayMode)
                    {
                        cvc.useCache = true
                        cvc.saveCache = true
                        cvc.enableAnimation = false
                        if (self.chartPageObject!.chartCount() > 1)
                        {
                            let chart = self.chartPageObject!.getChart(selection: 1)
                            cvc.generateChart(chartDataObject: chart!, completion:nil)
                        }
                    }
                })
            }
        }
        else if(segue.identifier == "EmbedContainer3") {
            
            if(self.containerViewController3 == nil)
            {
                self.containerViewController3 = segue.destination as? ContainerViewController;
                self.containerViewController3!.display(title : "Charts", fromStoryboard: "Main", entryPoint: "ChartViewController", animate: false, initialize:nil, completion: { (finished: Bool,  _ viewController : UIViewController?) -> () in
                    
                    let cvc = viewController as! ChartViewController
                    cvc.chartPageObject = self.chartPageObject
                    cvc.chartPageDelegate = self
                    if(self.displayMode)
                    {
                        cvc.useCache = true
                        cvc.saveCache = true
                        cvc.enableAnimation = false
                        // This allows duplicating the Chart
                        if (self.chartPageObject!.chartCount() >= 2)
                        {
                            var chart = self.chartPageObject!.getChart(selection: 1)
                            if (self.chartPageObject!.chartCount() > 2)
                            {
                                chart = self.chartPageObject!.getChart(selection: 2)
                            }
                            cvc.generateChart(chartDataObject: chart!, completion:nil)
                        }
                    }
                    
                })
            }
        }
        else if(segue.identifier == "EmbedContainer4") {
            
            if(self.containerViewController4 == nil)
            {
                self.containerViewController4 = segue.destination as? ContainerViewController;
                self.containerViewController4!.display(title : "Charts", fromStoryboard: "Main", entryPoint: "ChartViewController", animate: false, initialize:nil, completion: { (finished: Bool,  _ viewController : UIViewController?) -> () in
                    
                    let cvc = viewController as! ChartViewController
                    cvc.chartPageObject = self.chartPageObject
                    cvc.chartPageDelegate = self
                    if(self.displayMode)
                    {
                        cvc.useCache = true
                        cvc.saveCache = true
                        cvc.enableAnimation = false
                        if (self.chartPageObject!.chartCount() > 3)
                        {
                            let chart = self.chartPageObject!.getChart(selection: 3)
                            cvc.generateChart(chartDataObject: chart!, completion:nil)
                        }
                    }
                    
                    
                })
            }
        }
        else if(segue.identifier == "addChartSegue")
        {
            let navCtrl = segue.destination as! UINavigationController
            let vc = navCtrl.topViewController as! ChartConfigurationViewController
            vc.chartConfigurationDelegate = self
            vc.selectedChart = self.chartPageObject!.chartCount()
            vc.chartPageObject = self.chartPageObject
            
            self.addMode = true
        }
        else if(segue.identifier == "editChartSegue")
        {
            let navCtrl = segue.destination as! UINavigationController
            let vc = navCtrl.topViewController as! ChartConfigurationViewController
            if(self.screenLayout == .OneByOne1)
            {
                vc.selectedChart = 0
            }
            else if(self.screenLayout == .OneByOne2)
            {
                vc.selectedChart = 1
            }
            else if(self.screenLayout == .OneByOne3)
            {
                vc.selectedChart = 2
            }
            else if(self.screenLayout == .OneByOne4)
            {
                vc.selectedChart = 3
            }
            vc.chartConfigurationDelegate = self
            vc.chartPageObject = self.chartPageObject
            
            self.addMode = false
        }
    }
    
    func chartConfigurationDismissedSaved()
    {
        // Need to reload the Chart page UI, delete the preview image

        self.dismiss(animated: true) {
            
            if(self.addMode)
            {
                self.refreshCharts(changeLayout: true)
            }
            else
            {
                self.refreshCharts(changeLayout: false)
            }
            self.title = self.chartPageObject?.name
        }
    }
    
    func chartConfigurationDismissedCancelled()
    {
        if(!self.chartPageObject!.isNew)
        {
            self.chartPageObject?.deleteCacheImage()
            let reloadedChartPage = ChartPageDataObject.loadChartPage(filename:  self.chartPageObject!.identifier! + ".chartpage")
            _ = DashboardDataObject.shared.update(chartPage: reloadedChartPage!)
            self.chartPageObject = reloadedChartPage
        }

        // Need to reload the Chart page from storage and UI to redisplay, delete the preview image
        self.dismiss(animated: true, completion: nil)
    }
    

}
