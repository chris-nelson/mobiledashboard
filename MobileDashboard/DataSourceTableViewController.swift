//
//  DataSourceTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/4/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class DataSourceTableViewController: UITableViewController {

    weak var chartDelegate : ChartConfigurationDelegate?
    var chartDataSource : ChartDataSource?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        if let chartSource = self.chartDataSource
        {
            let index = DashboardDataObject.shared.dataSourcesSortedArray().index(of: chartSource)
            let indexPath = IndexPath.init(row: index!, section: 0)
            self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DashboardDataObject.shared.dataSourcesSortedArray().count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataSourceCell", for: indexPath)

        let dataSource : ChartDataSource = DashboardDataObject.shared.dataSourcesSortedArray()[indexPath.row]
        
        cell.textLabel?.text = dataSource.name
        // Configure the cell...
        if (self.chartDataSource?.identifier == dataSource.identifier)
        {
            cell.textLabel?.textColor = .blue
        }
        else
        {
            cell.textLabel?.textColor = .black
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let chartDelegate = self.chartDelegate
        {
            let dataSource : ChartDataSource = DashboardDataObject.shared.dataSourcesSortedArray()[indexPath.row]
            self.chartDataSource = dataSource
            chartDelegate.selectChartSource(chartsource: dataSource, action: .MoveForward)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
