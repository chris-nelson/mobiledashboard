//
//  HudController.h
//  DashExMail
//
//  Created by Christopher Nelson on 9/21/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HudController : NSObject

+ (instancetype)sharedHud;

- (void) showHUD:(UIView*)view;
- (void) showHUD:(UIView*)view title:(NSString*)title;

- (void) showHUDPercent:(UIView*)view title:(NSString*)title;
- (void) updateHUDPercent:(CGFloat)progress; // 0.0 to 1.0 as the percent

- (void) hideHUD:(UIView*)view;

@end
