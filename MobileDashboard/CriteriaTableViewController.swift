//
//  CriteriaTableViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/4/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class CriteriaTableViewController: UITableViewController {

    weak var chartDelegate : ChartConfigurationDelegate?
//    var chartDataObject : ChartDataObject?
    var chartDataSource : ChartDataSource?
//    var chartQueryObject : ChartQueryCriteriaObject?
    
    var remoteQueryParamArray : [ChartFieldParameterObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        for (_, fieldParam) in self.chartDataSource!.fieldParameters
        {
            if(fieldParam.filterType == .remoteOnly || fieldParam.filterType == .localAndRemote)
            {
                remoteQueryParamArray.append(fieldParam)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.remoteQueryParamArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "queryCell", for: indexPath) as! QueryTextTableViewCell
        
        let queryParam = self.remoteQueryParamArray[indexPath.row]
        
        cell.parameterNameLabel.text = queryParam.name

        // Configure the cell...

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let fieldParam = self.remoteQueryParamArray[indexPath.row]

        self.chartDelegate?.enableCriteriaDefinition(proposedRoutingMode: .addTextCriteria, fieldParam: fieldParam)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
