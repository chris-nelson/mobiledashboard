//
//  ColorSelectionViewController.m
//  Pods
//
//  Created by Christopher Nelson on 9/7/15.
//
//

#import "ColorSelectionViewController.h"
#import "CLColorPickerView.h"

@interface ColorSelectionViewController () <CLColorPickerDelegate>

@property (weak, nonatomic) IBOutlet CLColorPickerView *colorPickerView;

@end

@implementation ColorSelectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(self.titleOverride.length > 0)
        self.title = self.titleOverride;
    else
        self.title = @"Color Select";
    
    if(self.enableSaveButton)
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save Color" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed:)];
    
    if(self.enableCloseButton)
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    NSInteger currentPage = [[NSUserDefaults standardUserDefaults] integerForKey:@"UICColorPickerPage"];
    
    self.colorPickerView.selectedColor = self.selectedColor;
    self.colorPickerView.hideClearColorButton = self.hideClearColorButton;
    self.colorPickerView.delegate = self;
    [self.colorPickerView generateColorGrid];
    [self.colorPickerView scrollToPage:currentPage animated:NO];
}

- (void) saveButtonPressed:(UIBarButtonItem*)sender
{
    if(self.delegate != nil)
    {
        UIColor* color = self.colorPickerView.selectedColor;
        [self.delegate saveColorSelection:self color:color];
    }
}

- (void) cancelButtonPressed:(UIBarButtonItem*)sender
{
    if(self.delegate != nil)
    {
        [self.delegate cancelColorSelection:self];

    }
}

- (void) colorPickerView:(CLColorPickerView*)colorPicker selectedColorSelection:(UIColor*)color
{
    if(self.delegate != nil)
    {
        NSInteger currentPage = colorPicker.currentPage;
        [[NSUserDefaults standardUserDefaults] setInteger:currentPage forKey:@"UICColorPickerPage"];
        [self.delegate saveColorSelection:self color:color];
    }
}

- (void) colorPickerView:(CLColorPickerView*)colorPicker didScrollToPage:(NSUInteger*)colorPage;
{
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)colorPage forKey:@"UICColorPickerPage"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
