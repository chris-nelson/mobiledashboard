//
//  ChartConfigurationViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/3/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

enum ChartConfigurationAction {
    
    case StayOnCurrentPage
    case MoveForward
    case MoveBackward
    case Cancel
    case Save
}

enum RoutingMode {
    
    case standardRouting
    case selectCriteria
    case addTextCriteria
}

protocol ConfigurationDelegate : class
{
    func chartConfigurationDismissedSaved()
    func chartConfigurationDismissedCancelled()
}

protocol AddButtonDelegate : class {
    
    func addButtonPressed()
    
}

protocol ChartConfigurationDelegate : class {
    
    func selectChartSource(chartsource : ChartDataSource, action : ChartConfigurationAction)
    func selectChartType(chartType: ChartType, action: ChartConfigurationAction)
    
    func enableAddButton(routingMode : RoutingMode)
    func enableAddButton(addDelegate : AddButtonDelegate)
    func enableCriteriaDefinition(proposedRoutingMode : RoutingMode, fieldParam: ChartFieldParameterObject)
    
    func moveNext()
    func movePrevious()
    
    func dismissCharting()
}

protocol EntryFieldDelegate : class {
    
    func entryValid() -> Bool
    func displayErrorMessage()
    
}

protocol SaveChartDelegate : class
{
    func saveChart(completion : ((_ successful : Bool)->Void)?)
}

class ChartConfigurationViewController: UIViewController, ContainerViewControllerDelegate, ChartConfigurationDelegate {

    weak var chartConfigurationDelegate : ConfigurationDelegate?
    
    var containerViewController : ContainerViewController?
    var seguesArray : [String] = []
    var rerouteSeguesArray : [String] = []
    
    var selectedSegue : Int = 0
    var selectedRerouteSegue : Int = 0
    
    var routingMode : RoutingMode = .standardRouting
    var altRoutingMode : RoutingMode = .standardRouting
    
    var selectedChart : Int = 0
    var chartPageObject : ChartPageDataObject?

    var chartDataSource : ChartDataSource?
    
    weak var entryDelegate : EntryFieldDelegate?
    weak var saveDelegate : SaveChartDelegate?
    var currentChartQueryParameterObject : ChartQueryParameterObject?
    
    weak var addButtonDelegate : AddButtonDelegate?

    @IBOutlet weak var addButton: UIBarButtonItem!
    
    @IBOutlet weak var previousButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.updateSegueList()
        
        if(self.chartPageObject == nil)
        {
            self.chartPageObject = ChartPageDataObject.init()
            self.chartPageObject?.identifier = NSUUID().uuidString
            self.chartPageObject?.isNew = true
            
        }
        else
        {
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            if(chartDataObject != nil)
            {
                self.chartDataSource = DashboardDataObject.shared.chartDataSources[chartDataObject!.dataSourceIdentifier]
            }
        }

        self.selectSegue(selection: 0, animate:false, forward: true)
        
        self.definesPresentationContext = true
    }
    
    func updateSegueList()
    {
        self.seguesArray = ["chartDataSourceSegue", "criteriaSummary", "selectChartTypeSegue", "chartDetailsSegue", "selectAxisSegue", "selectAccumulatorSegue", "columnColorsSegue", "previewChartSegue", "saveChartSegue", "savingChartSegue"]
        
        if(self.chartPageObject != nil)
        {
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            if(chartDataObject != nil)
            {
                if(chartDataObject?.chartType == .barChart || chartDataObject?.chartType == .horizonatalBarChart || chartDataObject?.chartType == .singleLineChart)
                {
                    self.seguesArray = ["chartDataSourceSegue", "criteriaSummary", "selectChartTypeSegue", "chartDetailsSegue", "chartAdditionalDetailsSegue", "selectAxisSegue", "selectAccumulatorSegue", "columnColorsSegue", "previewChartSegue", "saveChartSegue", "savingChartSegue"]
                }
                else if(chartDataObject?.chartType == .stackedBarChart || chartDataObject?.chartType == .horizonatalStackedBarChart || chartDataObject?.chartType == .lineChart || chartDataObject?.chartType == .scatterChart)
                {
                    self.seguesArray = ["chartDataSourceSegue", "criteriaSummary", "selectChartTypeSegue", "chartDetailsSegue", "chartAdditionalDetailsSegue", "selectAxisSegue", "selectAccumulatorSegue", "selectGroupingSegue", "columnColorsSegue", "previewChartSegue", "saveChartSegue", "savingChartSegue"]
                }
                else if(chartDataObject?.chartType == .pieChart)
                {
                    self.seguesArray = ["chartDataSourceSegue", "criteriaSummary", "selectChartTypeSegue", "chartDetailsSegue", "chartPieDetailsSegue", "selectAxisSegue", "selectAccumulatorSegue", "columnColorsSegue", "previewChartSegue", "saveChartSegue", "savingChartSegue"]
                }
                /*
                else if(chartDataObject?.chartType == .lineChart || chartDataObject?.chartType == .scatterChart)
                {
                    self.seguesArray = ["chartDataSourceSegue", "criteriaSummary", "selectChartTypeSegue", "chartDetailsSegue", "chartAdditionalDetailsSegue", "selectAxisSegue", "selectAccumulatorSegue", "selectGroupingSegue", "columnColorsSegue", "previewChartSegue", "saveChartSegue", "savingChartSegue"]
                }*/
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "EmbedContainer") {
            
            if(self.containerViewController == nil)
            {
                self.containerViewController = segue.destination as? ContainerViewController
                self.containerViewController?.delegate = self
            }
        }
    }
    
    
   
    
// Add a entry for selection and number
//    [{"activityCompletionDate":"2016-08-16","cnt":3,"fixtureLocated":"No"},{"activityCompletionDate":"2016-08-16","cnt":10,"fixtureLocated":"Yes"}]
// Add the field to accumulate on - fixtureLocated
// Add the field to accumulate - cnt
    
    
    
    func containerPrepare(for segue : UIStoryboardSegue, sender : Any?) {
    
        self.addButton.isEnabled = false
        self.altRoutingMode = .standardRouting
        self.addButtonDelegate = nil
        self.entryDelegate = nil
        self.saveDelegate = nil

        if segue.identifier == "chartDataSourceSegue"
        {
            let vc : DataSourceTableViewController = segue.destination as! DataSourceTableViewController
            vc.chartDelegate = self
            vc.chartDataSource = self.chartDataSource
            
            self.title = vc.title
        }
        else if segue.identifier == "criteriaSummary"
        {
            let vc : CriteriaSummaryTableViewController = segue.destination as! CriteriaSummaryTableViewController
            vc.chartDelegate = self
            
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            
            self.title = vc.title
        }
        else if segue.identifier == "defineQuerySegue"
        {
            let vc : CriteriaTableViewController = segue.destination as! CriteriaTableViewController
            vc.chartDelegate = self
            vc.chartDataSource = self.chartDataSource
//            vc.chartDataObject = self.chartDataObject
            
            self.title = vc.title
        }
        else if segue.identifier == "enterTextCriteria"
        {
            let vc : CriteriaTextEntryViewController = segue.destination as! CriteriaTextEntryViewController
            vc.chartDelegate = self
            vc.chartQueryParameterObject = self.currentChartQueryParameterObject
            vc.numberField = false
            
            self.entryDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "enterNumberCriteria"
        {
            let vc : CriteriaTextEntryViewController = segue.destination as! CriteriaTextEntryViewController
            vc.chartDelegate = self
            vc.chartQueryParameterObject = self.currentChartQueryParameterObject
            vc.numberField = true
            
            self.entryDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "enterDateCriteria"
        {
            let vc : CriteriaDateEntryViewController = segue.destination as! CriteriaDateEntryViewController
            vc.chartDelegate = self
            vc.chartQueryParameterObject = self.currentChartQueryParameterObject
            
            self.entryDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "enterSelectionCriteria"
        {
            let vc : CriteriaSelectionEntryViewController = segue.destination as! CriteriaSelectionEntryViewController
            vc.chartDelegate = self
            vc.chartQueryParameterObject = self.currentChartQueryParameterObject
            
            self.entryDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "selectChartTypeSegue"
        {
            let vc : ChartTypeSelectionCollectionViewController = segue.destination as! ChartTypeSelectionCollectionViewController
            vc.chartDelegate = self
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            
            self.entryDelegate = nil
            self.title = vc.title
        }
        else if segue.identifier == "chartDetailsSegue"
        {
            let vc : ChartDetailsViewController = segue.destination as! ChartDetailsViewController
            vc.chartDelegate = self
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            
            self.entryDelegate = nil
            self.title = vc.title
        }
        else if segue.identifier == "chartAdditionalDetailsSegue"
        {
            let vc : ChartAdditionalDetailsViewController = segue.destination as! ChartAdditionalDetailsViewController
            vc.chartDelegate = self
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            
            self.entryDelegate = nil
            self.title = vc.title
        }
        else if segue.identifier == "chartPieDetailsSegue"
        {
            let vc : ChartPieDetailsViewController = segue.destination as! ChartPieDetailsViewController
            vc.chartDelegate = self
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            
            self.entryDelegate = nil
            self.title = vc.title
        }
        else if segue.identifier == "selectAxisSegue"
        {
            let vc : ChartAxisSelectionTableViewController = segue.destination as! ChartAxisSelectionTableViewController
            vc.chartDelegate = self
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            vc.chartDataSource = self.chartDataSource
            
            self.entryDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "selectAccumulatorSegue"
        {
            let vc : ChartAccumulatorTableViewController = segue.destination as! ChartAccumulatorTableViewController
            vc.chartDelegate = self
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            vc.chartDataSource = self.chartDataSource
            
            self.entryDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "selectGroupingSegue"
        {
            let vc : ChartGroupingTableViewController = segue.destination as! ChartGroupingTableViewController
            vc.chartDelegate = self
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            vc.chartDataSource = self.chartDataSource
            
            self.entryDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "columnColorsSegue"
        {
            let vc : ColumnColorTableViewController  = segue.destination as! ColumnColorTableViewController
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartDataObject = chartDataObject
            vc.chartDelegate = self
            
            self.addButtonDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "previewChartSegue"
        {
            let vc : ChartViewController = segue.destination as! ChartViewController
            vc.useCache = false
            vc.saveCache = false
            vc.displayFeedback = true
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartObject = chartDataObject
            
            self.title = vc.title
        }
        else if segue.identifier == "saveChartSegue"
        {
            let vc : SaveChartViewController = segue.destination as! SaveChartViewController
//            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            vc.chartPageObject = self.chartPageObject
            vc.chartDelegate = self
            self.entryDelegate = vc
            self.saveDelegate = vc
            self.title = vc.title
        }
        else if segue.identifier == "savingChartSegue"
        {
            let vc : UIViewController = segue.destination
            self.title = vc.title
            let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
            chartDataObject?.clearChartDataCache()
            self.chartPageObject?.isNew = false
            self.perform(#selector(dismissCharting), with: nil, afterDelay: 1.5)
        }
    }
    //
    // Save and display an actual chart before the details
    // Have the criteria go to a follow up screen for the details of the item, dates, subselections, etc
    
    func updateUI(){
        
        if(canMovePrevious())
        {
            self.previousButton.isEnabled = true
        }
        else
        {
            self.previousButton.isEnabled = false
        }

        if(canMoveNext())
        {
            self.nextButton.isEnabled = true
        }
        else
        {
            self.nextButton.isEnabled = false
        }
    }

    func performChartAction(action : ChartConfigurationAction)
    {
        switch action {
        case .MoveForward:
            self.moveNext()
        case .MoveBackward:
            self.movePrevious()
        default:
            break
        }
    }
    
    func selectChartSource(chartsource : ChartDataSource, action : ChartConfigurationAction)
    {
        self.chartDataSource = chartsource
        
        var chartDataObject : ChartDataObject? = self.chartPageObject?.getChart(selection: self.selectedChart)
        
        if let existingChartObject = chartDataObject
        {
            if existingChartObject.dataSourceIdentifier != chartDataSource!.identifier
            {
                chartDataObject = ChartDataObject.init(identifier: NSUUID().uuidString, dataSourceIdentifier: self.chartDataSource!.identifier)
                chartDataObject?.dataSourceIdentifier = self.chartDataSource!.identifier
                chartDataObject?.chartQuery = ChartQueryCriteriaObject.init()
                chartDataObject?.chartQuery.dataSourceIdentifier = self.chartDataSource!.identifier
                self.currentChartQueryParameterObject = nil
            }
        }
        else
        {
            chartDataObject = ChartDataObject.init(identifier: NSUUID().uuidString, dataSourceIdentifier: self.chartDataSource!.identifier)
            chartDataObject?.dataSourceIdentifier = self.chartDataSource!.identifier
            chartDataObject?.chartQuery  = ChartQueryCriteriaObject.init()
            chartDataObject?.chartQuery.dataSourceIdentifier = self.chartDataSource!.identifier
            self.currentChartQueryParameterObject = nil
        }
        
        _ = self.chartPageObject!.assignChart(preferredLocation: self.selectedChart, chartObject : chartDataObject!)
        
        self.performChartAction(action: action)
    }
    
    func selectChartType(chartType: ChartType, action: ChartConfigurationAction)
    {
        let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
        chartDataObject?.chartType = chartType
        
        self.updateSegueList()
        
        self.performChartAction(action: action)
    }
    
    func enableAddButton(routingMode proposedRoutingMode : RoutingMode)
    {
        self.altRoutingMode = proposedRoutingMode;
        self.addButton.isEnabled = true;
    }
    
    func enableAddButton(addDelegate : AddButtonDelegate)
    {
        self.addButtonDelegate = addDelegate
        self.addButton.isEnabled = true;
    }
    
    func enableCriteriaDefinition(proposedRoutingMode : RoutingMode, fieldParam: ChartFieldParameterObject)
    {
        self.altRoutingMode = proposedRoutingMode;
        self.currentChartQueryParameterObject = nil

        if(self.altRoutingMode == .addTextCriteria)
        {
            let queryParam = ChartQueryParameterObject.init()
            queryParam.fieldParameterIdentifier = fieldParam.name
            queryParam.fieldParameterName = fieldParam.name

            self.currentChartQueryParameterObject = queryParam
            if(fieldParam.type == .date)
            {
                self.rerouteSeguesArray = ["criteriaSummary", "defineQuerySegue", "enterDateCriteria", "criteriaSummary"]
            }
            else
            {
                if(fieldParam.type == .number)
                {
                    self.rerouteSeguesArray = ["criteriaSummary", "defineQuerySegue", "enterNumberCriteria", "criteriaSummary"]
                }
                else
                {
                    self.rerouteSeguesArray = ["criteriaSummary", "defineQuerySegue", "enterTextCriteria", "criteriaSummary"]
                }
                
                if let domainURL = fieldParam.domainUrl
                {
                    if(domainURL.lengthOfBytes(using: String.Encoding.utf8) > 0)
                    {
                        self.rerouteSeguesArray = ["criteriaSummary", "defineQuerySegue", "enterSelectionCriteria", "criteriaSummary"]
                    }
                }
            }
            self.selectedRerouteSegue = 2;
            self.selectSegue(selection: self.selectedRerouteSegue, animate:true, forward: true)
        }
    }

    @IBAction func addButtonPressed(_ sender: Any) {
        
        if(self.addButtonDelegate != nil)
        {
            self.addButtonDelegate?.addButtonPressed()
        }
        else if(self.altRoutingMode == .selectCriteria)
        {
            self.rerouteSeguesArray = ["criteriaSummary", "defineQuerySegue"]
            self.selectedRerouteSegue = 1;
            self.selectSegue(selection: self.selectedRerouteSegue, animate:true, forward: true)
        }
        
   }
    
    func selectSegue (selection : Int, animate: Bool, forward : Bool) {
        
        if(rerouteSeguesArray.count > 0)
        {
            self.selectedRerouteSegue = selection
            let segueRerouteIdentifier = self.rerouteSeguesArray[self.selectedRerouteSegue]
            self.containerViewController?.executeSegueWithIdentifier(identifier: segueRerouteIdentifier, animate:animate, forward: forward)
            
            let segueIdentifier = self.seguesArray[self.selectedSegue]
            
            if(segueIdentifier == segueRerouteIdentifier)
            {
                self.rerouteSeguesArray = []
                self.selectedRerouteSegue = 0;
            }
        }
        else if(selection >= 0 && selection < self.seguesArray.count)
        {
            self.selectedSegue = selection
            let segueIdentifier = self.seguesArray[self.selectedSegue]
            self.containerViewController?.executeSegueWithIdentifier(identifier: segueIdentifier, animate:animate, forward: forward)
        }
        
        updateUI()
    }
    
    func canMovePrevious () ->Bool {
        
        if(rerouteSeguesArray.count > 0)
        {
            return ((self.selectedRerouteSegue - 1) >= 0)
        }
        
        return ((self.selectedSegue - 1) >= 0)
    }
    
    func canMoveNext () ->Bool {
        
        if(rerouteSeguesArray.count > 0)
        {
            return (self.selectedRerouteSegue + 1 < self.rerouteSeguesArray.count)
        }
        else if(self.selectedSegue == 0 && self.chartDataSource == nil)
        {
            return false
        }
        
        return (self.selectedSegue + 1 < self.seguesArray.count)
        
    }
    
    func moveNext()
    {
        if let testEntryDelegate = self.entryDelegate
        {
            if (!testEntryDelegate.entryValid())
            {
                testEntryDelegate.displayErrorMessage()
                
                return
            }
            else
            {
                if(self.currentChartQueryParameterObject != nil)
                {
                    let chartDataObject = self.chartPageObject?.getChart(selection: self.selectedChart)
                    if let fieldValue = self.currentChartQueryParameterObject!.fieldParameterValue
                    {
                        if(fieldValue.lengthOfBytes(using: .utf8) > 0)
                        {
                            chartDataObject?.chartQuery.chartQueryParamArray.append(self.currentChartQueryParameterObject!)
                        }
                    }
                    self.currentChartQueryParameterObject = nil;
                }
            }
        }

        if self.canMoveNext()
        {
            if(rerouteSeguesArray.count > 0)
            {
                self.selectSegue(selection: self.selectedRerouteSegue + 1, animate:true, forward: true)
            }
            else
            {
                if let testSaveDelegate = self.saveDelegate
                {
                    testSaveDelegate.saveChart(completion: { (success) in
                        
                        if(success)
                        {
                            self.selectSegue(selection: self.selectedSegue + 1, animate:true, forward: true)
                        }
                        else
                        {
                            DispatchQueue.main.async {

                                let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to save chart", preferredStyle: .alert)
                                let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                                alertController.addAction(action)
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    })
                }
                else
                {
                    self.selectSegue(selection: self.selectedSegue + 1, animate:true, forward: true)
                }
            }
        }
    }
    
    func movePrevious()
    {
        if self.canMovePrevious()
        {
            if(rerouteSeguesArray.count > 0)
            {
                self.selectSegue(selection: self.selectedRerouteSegue - 1, animate:true, forward: false)
            }
            else
            {
                self.selectSegue(selection: self.selectedSegue - 1, animate:true, forward: false)
            }
        }
    }
    
    func dismissCharting()
    {
        self.chartPageObject?.deleteCacheImage()
        self.chartConfigurationDelegate!.chartConfigurationDismissedSaved()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
    
        if(!self.chartPageObject!.isNew)
        {
            self.chartPageObject?.deleteCacheImage()
            let reloadedChartPage = ChartPageDataObject.loadChartPage(filename:  self.chartPageObject!.identifier! + ".chartpage")
            _ = DashboardDataObject.shared.update(chartPage: reloadedChartPage!)
        }
        
        self.chartConfigurationDelegate!.chartConfigurationDismissedCancelled()
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {

        self.movePrevious()
    }
    
    @IBAction func nextButtonPressed(_ sender: UIBarButtonItem) {
        
        self.moveNext()
    
    }
}
