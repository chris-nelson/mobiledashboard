//
//  QueryTextTableViewCell.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 1/4/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class QueryTextTableViewCell: UITableViewCell {

    @IBOutlet weak var parameterNameLabel: UILabel!
    @IBOutlet weak var parameterTextField: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
