//
//  ChartMenuTableViewCell.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/6/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var chartImageView: UIImageView!
    @IBOutlet weak var chartTextLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func prepareForReuse() {
        
        self.chartImageView.image = nil
    
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
