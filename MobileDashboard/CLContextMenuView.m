//
//  CLMenuView.h
//
//  Created by Christopher Nelson on 2/15/16.
//  Copyright © 2016 Odeon Software. All rights reserved.
//

#import "CLContextMenuView.h"
#import "CLContextMenuItemView.h"

@interface CLContextMenuView () <CLContextMenuItemDelegate>

@property (nonatomic, strong) UIView* veilView;
@property (nonatomic, strong) UIView* blurView;

@property (nonatomic, strong) NSMutableArray* allMenuItems;
@property (nonatomic, strong) NSMutableArray* menuItems;
@property (nonatomic, assign) BOOL animating;

@property (nonatomic, weak) UIViewController* rootViewController;

@end

@implementation CLContextMenuView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void) dealloc
{
    NSLog(@"dealloc - MenuView");
}


- (void) commonInit
{
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMenu:)];
    [self addGestureRecognizer:tapGesture];
    
    self.rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];

    self.blurView = [CLContextMenuItemView createDimmingView];
    
    [self addSubview:self.blurView];
    self.blurView.frame = self.bounds;
    [self sendSubviewToBack:self.blurView];

    
    self.startEndAnimationDuration = 1.25;
    self.animationDuration = 1.5;
    self.animationUnfoldDuration = .5;
    
    self.menuLabelWidth = 80;
    self.menuIconWidth = 30;
    self.menuHeight = 30;
}

- (NSMutableArray*) allMenuItems
{
    if(_allMenuItems == nil)
    {
        _allMenuItems = [[NSMutableArray alloc]init];
    }
    
    return _allMenuItems;
}

- (NSMutableArray*) menuItems
{
    if(_menuItems == nil)
    {
        _menuItems = [[NSMutableArray alloc]init];
    }
    
    return _menuItems;
}

- (void) tapMenu:(UITapGestureRecognizer*)sender
{
    [self displayMenu];
}

- (void) displayMenu
{
    if(!self.animating)
    {
        if(self.veilView == nil)
        {
            self.animating = YES;
            
            self.veilView = [[UIView alloc]initWithFrame:self.rootViewController.view.frame];
            self.veilView.backgroundColor = [UIColor blackColor];
            self.veilView.alpha = 0.0;
            UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePopupMenu:)];
            [self.veilView addGestureRecognizer:tapGesture];
            
            [self.rootViewController.view addSubview:self.veilView];
            
            [self.rootViewController.view bringSubviewToFront:self.veilView];
            [self.rootViewController.view bringSubviewToFront:self];
            [self.allMenuItems removeAllObjects];
            [self.menuItems removeAllObjects];
            
            [self.delegate menuSetup:self];
            
            [UIView animateWithDuration:self.startEndAnimationDuration animations:^{
                
                self.veilView.alpha = 0.25;
                
            }completion:^(BOOL finished) {
                
                NSInteger cnt = 0;
                while(cnt < self.allMenuItems.count)
                {
                    BOOL pass = NO;
                    CLContextMenuItemView* view = self.allMenuItems[cnt];
                    if(view.menuEntry.canDisplayMenuEntry != nil)
                    {
                        if(view.menuEntry.canDisplayMenuEntry())
                            pass = YES;
                    }
                    else
                        pass = YES;
                    
                    if(pass)
                    {
                        [self.menuItems addObject:view];
                    }
                    cnt++;
                }
                
                
                [self displaySubMenu:0 afterSiblingView:self];
                
            }];
        }
        else
        {
            [self hidePopupMenu:nil];
        }
    }
}

- (void) addMenuEntry:(CLMenuEntryObject*)menuEntry
{
    CGPoint anchorPoint = CGPointMake(self.frame.origin.x, self.frame.origin.y);
    anchorPoint = [self.rootViewController.view convertPoint:anchorPoint fromView:self];
    if(self.poistionOverridePoint.x != 0 && self.poistionOverridePoint.y != 0)
        anchorPoint = CGPointMake(self.poistionOverridePoint.x, self.poistionOverridePoint.y);
    
    CLContextMenuItemView* view = [[CLContextMenuItemView alloc] initWithFrame:CGRectMake(anchorPoint.x, anchorPoint.y, self.menuIconWidth, 0)];
    
    view.menuImageView.frame = CGRectMake(0, 0, self.menuIconWidth, self.menuHeight);
    view.delegate = self;
    view.menuEntry = menuEntry;
    view.hidden = YES;
    //self.hidden = YES;
    [self.rootViewController.view addSubview:view];
    [self.allMenuItems addObject:view];
}

- (void) displaySubMenu:(NSUInteger)menuLevel afterSiblingView:(UIView*)siblingView
{
    if(menuLevel < self.menuItems.count)
    {
        //        view.frame = CGRectMake(siblingView.frame.origin.x, siblingView.frame.origin.y + siblingView.frame.size.height, siblingView.frame.size.width, 0);
        
        __block CLContextMenuItemView* nextView = nil;
        __block CLContextMenuItemView* nextSibView = nil;
        NSInteger offset = 0;
        if(menuLevel == 0)
        {
            offset = self.poistionOverridePoint.y;
            nextView = self.menuItems[0];
            if(self.poistionOverridePoint.x != 0 && self.poistionOverridePoint.y != 0)
                nextView.frame = CGRectMake(self.poistionOverridePoint.x, offset + siblingView.frame.origin.y + siblingView.frame.size.height, self.menuIconWidth, self.menuHeight);
            else
                nextView.frame = self.frame;
            nextView.blurView.frame = CGRectMake(0, 0, self.menuIconWidth, self.menuHeight);
            nextView.blurView.hidden = YES;
        }
        
        void(^relocateMenu)() = ^void()
        {
            NSInteger loop = menuLevel;
            while(loop < self.menuItems.count)
            {
                nextView = self.menuItems[loop];
                [nextView removeFromSuperview];
                
                [self.rootViewController.view insertSubview:nextView aboveSubview:self.veilView];
                if(self.poistionOverridePoint.x != 0 && self.poistionOverridePoint.y != 0)
                    nextView.frame = CGRectMake(self.poistionOverridePoint.x, offset + siblingView.frame.origin.y + siblingView.frame.size.height, self.menuIconWidth, self.menuHeight);
                else
                    nextView.frame = CGRectMake(siblingView.frame.origin.x, siblingView.frame.origin.y + siblingView.frame.size.height, self.menuIconWidth, self.menuHeight);//siblingView.frame.size.height);
                [nextView sendSubviewToBack:nextView.blurView];
                nextView.blurView.frame = CGRectMake(0, 0, self.menuIconWidth, self.menuHeight);
                nextView.menuLabel.frame = CGRectMake(0, 5, 0, self.menuHeight - 10);
                
                if(nextSibView == nil)
                {
                    nextSibView = nextView;
                    nextSibView.hidden = NO;
                    nextView.blurView.hidden = NO;
                }
                
                //nextView.blurView.frame = CGRectMake(0, 0, 0, self.menuHeight);
                loop++;
            }
        };
        
        if(menuLevel == 0 && self.hideFirstAnimation)
        {
            relocateMenu();
            [self displaySubMenu:menuLevel + 1 afterSiblingView:nextView];
        }
        else
        {
            [UIView animateWithDuration:self.animationUnfoldDuration animations:^{
                
                relocateMenu();
                //            [self layoutIfNeeded];
                
            } completion:^(BOOL finished) {
                
                //nextSibView.hidden = NO;
                [self displaySubMenu:menuLevel + 1 afterSiblingView:nextView];
                
            }];
        }
    }
    else
    {
        [UIView animateWithDuration:self.animationDuration animations:^{
            
            NSInteger loop = 0;
            while(loop < self.menuItems.count)
            {
                CLContextMenuItemView* view = self.menuItems[loop];
                view.frame = CGRectMake(view.frame.origin.x - self.menuLabelWidth, view.frame.origin.y, view.frame.size.width + self.menuLabelWidth, self.menuHeight);
                view.menuLabel.frame = CGRectMake(0, 5, self.menuLabelWidth - 5, self.menuHeight - 10);
                view.blurView.frame = CGRectMake(0, 0, self.menuLabelWidth + self.menuIconWidth, self.menuHeight);
//                view.blurView.frame = CGRectMake(0, 0, self.menuLabelWidth, self.menuHeight);

                view.menuImageView.frame = CGRectMake(view.frame.size.width - self.menuIconWidth, 0, self.menuIconWidth, self.menuHeight);
                loop++;
            }
        }completion:^(BOOL finished) {
            
            NSLog(@"DONE");
            self.animating = NO;
            
        }];
    }
}

- (void) hidePopupMenu:(UITapGestureRecognizer*)sender
{
    [self hideMenu:YES];
}

- (void) hideMenu:(BOOL)animation
{
    [self hideMainMenu:self.menuItems.count - 1 animation:animation];
}

- (void) hideMainMenu:(NSInteger)menuLevel animation:(BOOL)animation
{
    if(!self.animating)
    {
        self.animating = YES;
        CGFloat animationDuration = self.animationDuration;
        if(!animation)
            animationDuration = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            
            NSInteger loop = 0;
            while(loop < self.menuItems.count)
            {
                CLContextMenuItemView* view = self.menuItems[loop];
                NSInteger width = self.menuIconWidth;//self.frame.size.width;
                view.frame = CGRectMake(view.frame.origin.x + (view.frame.size.width - width), view.frame.origin.y, width, self.menuHeight);
                view.menuImageView.frame = CGRectMake(0, 0, self.menuIconWidth, self.menuHeight);
                view.menuLabel.frame = CGRectMake(0, 5, 0, self.menuHeight - 10);
                view.blurView.frame = CGRectMake(0, 0, self.menuIconWidth, self.menuHeight);
                loop++;
            }
        }completion:^(BOOL finished) {
            
            [self hideSubMenu:menuLevel animation:animation];
        }];
    }
}

- (void) hideSubMenu:(NSInteger)menuLevel  animation:(BOOL)animation
{
    if(menuLevel >= 0)
    {
        CGFloat animationDuration = self.animationUnfoldDuration;
        if(!animation)
            animationDuration = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            
            CLContextMenuItemView* view = self.menuItems[menuLevel];
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - view.frame.size.height + view.frame.size.height, view.frame.size.width, 0);//siblingView.frame.size.height);
            view.menuImageView.frame = CGRectMake(0, 0, self.menuIconWidth, 0);
            
            view.menuLabel.frame = CGRectMake(0, 5, 0, 0);
            view.blurView.frame = CGRectMake(0, 5, self.menuIconWidth, 0);
        } completion:^(BOOL finished) {
            
            [self hideSubMenu:menuLevel - 1 animation:animation];
            
        }];
    }
    else
    {
        CGFloat animationDuration = self.startEndAnimationDuration;
        if(!animation)
            animationDuration = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            
            self.veilView.alpha = 0;
            
        } completion:^(BOOL finished) {
            
            [self.veilView removeFromSuperview];
            self.veilView = nil;
            for(UIView* view in self.menuItems)
            {
                [view removeFromSuperview];
            }
            [self.menuItems removeAllObjects];
            self.animating = NO;
            //self.hidden = NO;
        }];
    }
}


@end
