//
//  ChartPageDataObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 12/9/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartPageDataObject: NSObject, NSCoding, UIActivityItemSource {

    var identifier : String?
    var name : String?
    var isNew : Bool = false;
    let uti = "com.amerch.MobileDashboard.chartpage"
    
    var downloadingData : Bool = false
    var processingData : Bool = false
    var dataDownloadFailed : Bool = false
    
    var chartItem = 0

    private var displayCharts : [ChartDataObject] = []
    
    override init() {
        
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {

        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.identifier = aDecoder.decodeObject(forKey: "identifier") as? String ?? ""
        self.displayCharts = aDecoder.decodeObject(forKey: "displayCharts") as? [ChartDataObject] ?? []
    }
    
    func encode(with aCoder: NSCoder) {

        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.identifier, forKey: "identifier")
        aCoder.encode(self.displayCharts, forKey: "displayCharts")
    }
    
    override var description: String {
        get {
            
            if let desc = self.name
            {
                return desc
            }
            
            return super.description
            
        }
    }
    
    func getChart(selection : Int) -> ChartDataObject?
    {
        var chartDataObject : ChartDataObject? = nil
        
        if(selection < self.displayCharts.count && selection >= 0)
        {
            chartDataObject = self.displayCharts[selection]
        }
        
        return chartDataObject
    }

    func assignChart(preferredLocation : Int, chartObject : ChartDataObject) -> Int
    {
        var assignedLocation = -1
        
        if(preferredLocation < self.displayCharts.count && preferredLocation >= 0)
        {
            self.displayCharts[preferredLocation] = chartObject
        }
        else
        {
            if(self.displayCharts.count < 4)
            {
                self.displayCharts.append(chartObject)
                assignedLocation = self.displayCharts.count - 1
            }
        }
        
        return assignedLocation
    }
    
    func removeChart(atLocation: Int) -> Bool
    {
        var pass : Bool  = false
        
        if(atLocation >= 0 && atLocation < self.displayCharts.count)
        {
            let chart = self.displayCharts[atLocation]
            chart.clearChartDataCache()
            self.displayCharts.remove(at: atLocation)
        
            pass = true
        }
        
        return pass
    }
    
    func chartCount() -> Int
    {
        return self.displayCharts.count
    }
    
    func chartImageCacheURL() -> URL {
        
        let path : String = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        var url : URL = URL.init(fileURLWithPath: path)
        url = url.appendingPathComponent(String.init(format: "%@.png", self.identifier!))
        
        return url;
    }
    
    func refreshCharts(completion : ((_ successful : Bool, _ downloaded : Bool, _ dataArray :  [[String:Any]]?)->Void)?)
    {
        if(!self.downloadingData)
        {
            self.downloadingData = true
            self.dataDownloadFailed = false
            
            var queues = [() -> ()]()
            var count : Int = 0

            for chartDataObject in self.displayCharts
            {
                let downloadChartData : () -> () = {
                    
                    let chartDataSource = DashboardDataObject.shared.chartDataSources[chartDataObject.dataSourceIdentifier]
                    
                    chartDataObject.getChartData(chartSource: chartDataSource, loadCache: true, saveCache: true, completion: { (successful, downloaded, dataArray) in
                        
                        if(dataArray == nil)
                        {
                            self.dataDownloadFailed = true
                        }
                        
                        if(count + 1 < queues.count)
                        {
                            let block = queues[count + 1]
                            count += 1
                            block()
                        }
                        else
                        {
                            if(completion != nil)
                            {
                                self.downloadingData = false
                                completion!(successful, downloaded, dataArray)
                            }
                        }
                    })
                    
                }
/*
                if(!chartDataObject.chartHasData())
                {
                    //                chartDataObject.getChartData(chartSource: chartDataSource, loadCache: true, saveCache: true, completion:nil)
                    
                    
                }
                else
                {
                    if(completion != nil)
                    {
                        completion!(false, false, nil)
                    }
                }*/
                
                queues.append(downloadChartData)
            }
            let block = queues[0]
            block()
        }
    }
    
    func swapChartPosition(atSourceIndex : Int, forDestinationIndex : Int)
    {
        if((atSourceIndex >= 0 && atSourceIndex < self.displayCharts.count) && (forDestinationIndex >= 0 && forDestinationIndex < self.displayCharts.count))
        {
            let itemToMove : ChartDataObject = self.displayCharts[atSourceIndex]
            self.displayCharts.remove(at: atSourceIndex)
            self.displayCharts.insert(itemToMove, at: forDestinationIndex)
        }
    }
    
    func deleteCacheImage()
    {
        let chartURL = self.chartImageCacheURL()
        print("Delete Page Image Cache")
        
        if(FileManager.default.fileExists(atPath: chartURL.path))
        {
            do
            {
                try FileManager.default.removeItem(atPath: chartURL.path)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        self.dataDownloadFailed = false
    }
    
    func chartsHaveData() -> Bool
    {
        var pass = true
        
        for chartDataObject in self.displayCharts
        {
            if(!chartDataObject.chartHasData())
            {
                pass = false
                break
            }
        }
        
        if(pass)
        {
            pass = true
        }
        
        return pass
    }
    
    func deleteChartFiles()
    {
        let cachesURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        
        for chartDataObject in self.displayCharts
        {
            let queryFileName : String! = "\(chartDataObject.identifier!).plist"
            let queryDeffileURL = cachesURL?.appendingPathComponent(queryFileName)
            if(FileManager.default.fileExists(atPath: queryDeffileURL!.path))
            {
                do
                {
                    try FileManager.default.removeItem(atPath: queryDeffileURL!.path)
                }
                catch
                {
                    print(error.localizedDescription)
                }
            }
        }
        self.dataDownloadFailed = false
    }
    
    func deleteFile()
    {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        
        self.deleteChartFiles()
        
        let currentDomainFileURL : URL! = documentsURL?.appendingPathComponent("\(self.identifier!).chartpage")
        if(FileManager.default.fileExists(atPath: currentDomainFileURL!.path))
        {
            do
            {
                try FileManager.default.removeItem(atPath: currentDomainFileURL!.path)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        
    }

    func saveChartPage(completion : ((_ successful : Bool)->Void)?)
    {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let currentDomainFileURL : URL! = documentsURL?.appendingPathComponent("\(self.identifier!).chartpage")
        
        if(NSKeyedArchiver.archiveRootObject(self, toFile: currentDomainFileURL.path))
        {
            print("Saved")
            if(completion != nil)
            {
                completion!(true)
            }
        }
        else
        {
            print("Not Saved")
            if(completion != nil)
            {
                completion!(false)
            }
        }
    }
    
    static func loadChartPage(filename : String) -> ChartPageDataObject?
    {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let currentDomainFileURL = documentsURL?.appendingPathComponent("\(filename)")
        
        let chartPageObject : ChartPageDataObject? = NSKeyedUnarchiver.unarchiveObject(withFile: (currentDomainFileURL?.path)!) as? ChartPageDataObject
        
        return chartPageObject
    }
    
    static func loadChartPage(fileURL : URL) -> ChartPageDataObject?
    {
        let chartPageObject : ChartPageDataObject? = NSKeyedUnarchiver.unarchiveObject(withFile: fileURL.path) as? ChartPageDataObject
        
        return chartPageObject
    }
    
    // MARK: - UIActivityItemSource
    
    public func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any
    {
        return NSData.init()
    }

    public func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any?
    {
        if(activityType == UIActivityType.airDrop)
        {
            let data = NSKeyedArchiver.archivedData(withRootObject: self)
            
            return data
        }

        let url = self.chartImageCacheURL()
        var image : UIImage?

        if FileManager.default.fileExists(atPath: url.path)
        {
            if let imageData = try? Data(contentsOf: url)
            {
                image = UIImage.init(data: imageData)
            }
        }

        return image
    }
    
    public func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivityType?) -> String
    {
        var subject : String = ""
        
        if(activityType == UIActivityType.airDrop)
        {
            subject = "MobileDashboard.ChartPage"
        }
        
        return subject
    }
    
    public func activityViewController(_ activityViewController: UIActivityViewController, dataTypeIdentifierForActivityType activityType: UIActivityType?) -> String
    {
        var identifier = "public.image"
        
        if(activityType == UIActivityType.airDrop)
        {
            identifier = uti
        }
        
        return identifier
    }
    
    public func activityViewController(_ activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: UIActivityType?, suggestedSize size: CGSize) -> UIImage?
    {
        let url = self.chartImageCacheURL()
        var scaledImage : UIImage?
        var image : UIImage?
        
        if FileManager.default.fileExists(atPath: url.path)
        {
            if let imageData = try? Data(contentsOf: url)
            {
                image = UIImage.init(data: imageData)
                scaledImage = UIImage.init(image: image, scaledToFitTo: size)
            }
        }
        
        return scaledImage

    }
}


