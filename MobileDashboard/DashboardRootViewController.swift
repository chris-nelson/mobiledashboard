//
//  DashboardRootViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/29/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit
import LocalAuthentication

//https://cocoapods.org/pods/Charts
//http://www.appcoda.com/ios-charts-api-tutorial/
//https://github.com/danielgindi/Charts#usage
//http://stackoverflow.com/questions/24183812/swift-warning-equivalent

class DashboardRootViewController: UIViewController, PinEntryViewControllerDelegate, CLExternalDisplayManagerDelegate {

    var containerViewController : ContainerViewController?
    var externalViewController : ContainerViewController?
    
    var activeLogIn = false
    var fastLogin : Bool = true
    var cachedViewController : UIViewController?
    @IBOutlet weak var blockingView: UIView!
    
    static func rootViewController() -> DashboardRootViewController {
        
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window!.rootViewController as! DashboardRootViewController
        
        return rootViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
//        CLExternalDisplayManager.sharedInstance().updateAvailableModes()
//        CLExternalDisplayManager.sharedInstance().delegate = self
//        self.externalDisplayDidChange()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func externalDisplayDidChange()
    {
        if(CLExternalDisplayManager.sharedInstance().availableModes.count > 0)
        {
            CLExternalDisplayManager.sharedInstance().selectedScreenMode = 0
            CLExternalDisplayManager.sharedInstance().updateAvailableModes()
//            let currentView =
        }
        else
        {
            CLExternalDisplayManager.sharedInstance().selectedScreenMode = -1;
        }
    }

    func refreshMenuItems() {
        
        if let navCntr = self.containerViewController?.currentViewController as? UINavigationController
        {
            if let chartMenuTableVC = navCntr.childViewControllers.first as? ChartMenuTableViewController
            {
                chartMenuTableVC.reloadCharts()
            }
            else if let chartMenuCollectionVC =  navCntr.childViewControllers.first as? ChartMenuCollectionViewController
            {
                chartMenuCollectionVC.reloadCharts()
            }
        }
        
    }
    
    func selectLogInScreen() {
        
        self.containerViewController!.display(title : "Log In", fromStoryboard: "Main", entryPoint: "LogIn", animate: true, initialize:nil, completion:nil)
    }
    
    func selectPINEntryScreen() {
        
        if let userPIN = DashboardSettingsObject.shared.userPIN
        {
            self.activeLogIn = true;
            self.containerViewController!.display(title : "PIN Entry", fromStoryboard: "Main", entryPoint: "PINEntryNav", animate: true,
                                                  
            initialize:{ (finished: Bool,  _ viewController : UIViewController?) -> () in
                
                if(finished)
                {
                    let navVC = viewController as! UINavigationController
                    let vc = navVC.topViewController as! PINEntryViewController
                    
                    vc.delegate = self
                    vc.pinMode = CLPinMode.verifyPin
                    vc.pinCodeToCheck = userPIN
                    
                    if(self.cachedViewController != nil)
                    {
                        vc.allowCancel = false
                        vc.allowClearing = false
                    }
                }
                
                print("VC - ", viewController?.title ?? "ViewController unavailable")
            },
            completion: { (finished: Bool,  _ viewController : UIViewController?) -> () in
                
                print("VC - ", viewController?.title ?? "ViewController unavailable")
                
            })
        }
        else
        {
            self.selectCurrentMenuScreen(completion: nil)
        }
        //                            self.selectPINEntryScreen()
        //                            DashboardRootViewController.rootViewController().selectCurrentMenuScreen()
      
//        self.containerViewController!.display(title : "PIN Entry", fromStoryboard: "Main", entryPoint: "PINEntryNav", completion:nil)
    }
    
    func returnLogin() {
        
 //        var pass : Bool = false
        if(!self.activeLogIn && DashboardSettingsObject.shared.userPIN != nil && DashboardSettingsObject.shared.userPIN!.lengthOfBytes(using: .utf8) > 0)
        {
            self.cachedViewController = containerViewController?.currentViewController
            
            let context : LAContext = LAContext.init()
            // Set text for the localized fallback button.
            context.localizedFallbackTitle = "Enter PIN";
            let touchIDActive = context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil)
            
            if(DashboardSettingsObject.shared.useTouchId && touchIDActive)
            {
                self.blockingView.isHidden = false
                self.view .bringSubview(toFront: self.blockingView)
                self.verifyWithTouchID()
            }
            else
            {
                self.selectPINEntryScreen()
            }
        }
    }
    
    func selectCurrentMenuScreen(completion: ((_ finished : Bool, _ viewController : UIViewController?)->Void)?) {

        if( DashboardSettingsObject.shared.useTableView)
        {
            self.containerViewController!.display(title : "Charts", fromStoryboard: "Main", entryPoint: "ChartMenuTableViewNav", animate: true, initialize:nil, completion: { (finished: Bool,  _ viewController : UIViewController?) -> () in
                
                print("VC - ", viewController?.title ?? "ViewController unavailable")
                if(completion != nil)
                {
                    completion!(finished, viewController)
                }
                
            })
        }
        else
        {
            self.containerViewController!.display(title : "Charts", fromStoryboard: "Main", entryPoint: "ChartMenuCollectionViewNav", animate: true, initialize:nil, completion: { (finished: Bool,  _ viewController : UIViewController?) -> () in
                
                print("VC - ", viewController?.title ?? "ViewController unavailable")
                if(completion != nil)
                {
                    completion!(finished, viewController)
                }
                
            })
        }
        
    }
    
    func LogOut() {
        
        DashboardSettingsObject.shared.userName = nil
        DashboardSettingsObject.shared.password = ""
        DashboardSettingsObject.shared.userPIN = ""
        DashboardSettingsObject.shared.useTouchId = false
        
        DashboardDataObject.shared.resetData()
        DashboardDataObject.shared.loggedIn = false

        
        self.selectLogInScreen()

    }
    
    override var  supportedInterfaceOrientations: UIInterfaceOrientationMask {
        
        var mask : UIInterfaceOrientationMask =  [.portrait, .portraitUpsideDown, .landscapeLeft, .landscapeRight]
        
        if var vc = self.containerViewController?.currentViewController
        {
            if vc is UINavigationController
            {
                let navvc = vc as! UINavigationController
                vc = navvc.topViewController!
            }
            mask = vc.supportedInterfaceOrientations
        }
        
        return mask
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "EmbedContainer") {
            
            if(self.containerViewController == nil)
            {
                self.containerViewController = segue.destination as? ContainerViewController
                
//                self.externalViewController = self.
                
                if let _ = DashboardSettingsObject.shared.userPIN
                {
                    self.autoLogIn(delayedLogin: false)
                }
                else
                {
                    self.selectLogInScreen()
                }
            }
        }
        else if(segue.identifier == "PINVerifySegue")
        {
            let navVC = segue.destination as! UINavigationController
            let vc = navVC.topViewController as! PINEntryViewController
            
            vc.delegate = self
            vc.pinMode = CLPinMode.verifyPin
            vc.pinCodeToCheck = DashboardSettingsObject.shared.userPIN
        }
    }

    func attemptLogin(userName : String?, password : String?, completion : @escaping (_ successful : Bool)->Void) {
        
        DashboardDataObject.shared.performLogin(userName: userName, password: password, completion: { (successful : Bool) in
         
            completion(successful)
        })
    }
    
    func processLogin(fromTouchID : Bool) {
        
        DispatchQueue.main.async {
            
            if(self.cachedViewController != nil)
            {
                if(!fromTouchID)
                {
                    self.containerViewController!.display(viewController: self.cachedViewController!, animate: true, completion: { (success, viewController) in
                        
                        self.cachedViewController = nil;
                        
                    })
                }
                else
                {
                    self.cachedViewController = nil;
                }
            }
            else
            {
                HudController.sharedHud().showHUD(self.view, title: "Downloading Content")
                self.refreshData(completion: { (success) in
                    
                    DispatchQueue.main.async {
                        
                        HudController.sharedHud().hideHUD(self.view)
                        
//                        if(success)
//                        {
                            // @Warning: Handle the inability to download initial data or the cached data does not exist
                            DashboardRootViewController.rootViewController().selectCurrentMenuScreen(completion: { (successful, viewController) in
                                
                            })
//                        }
                        
                    }
                })
            }
        }
    }
    
    func autoLogIn(delayedLogin : Bool)
    {
        let username = DashboardSettingsObject.shared.userName
        let password = DashboardSettingsObject.shared.password
        
        self.attemptLogin(userName: username, password: password, completion: { (successful) in
            
            DispatchQueue.main.async {
                
                if(successful)
                {
                    if(self.fastLogin && !delayedLogin) {
                        
                        self.processLogin(fromTouchID: false)
                    }
                    else
                    {
                        if(delayedLogin)
                        {
                            self.processLogin(fromTouchID: false)
                        }
                        else
                        {
                            let context : LAContext = LAContext.init()
                            // Set text for the localized fallback button.
                            context.localizedFallbackTitle = "Enter PIN";
                            let touchIDActive = context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil)
                            
                            if(DashboardSettingsObject.shared.useTouchId && touchIDActive)
                            {
                                self.verifyWithTouchID()
                            }
                            else
                            {
                                self.selectPINEntryScreen()
                            }
                        }
                    }
                }
                else
                {
                    self.selectLogInScreen()
                }
                
            }
        })
    }
    
    func refreshData( completion : @escaping (_ successful : Bool)->Void ) {
        
// Warning: process failures
        DashboardDataObject.shared.loadDomains(completion: { (successful) in
            
            if(successful)
            {
                DashboardDataObject.shared.loadQueryDefs(completion: { (successful) in

                    completion(successful)
                })
            }
            else
            {
                completion(false)
            }
        })
    }
    
    func verifyWithTouchID()
    {
        let context : LAContext = LAContext.init()
        
        // Set text for the localized fallback button.
        context.localizedFallbackTitle = "Enter PIN";
    
        context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Unlock access to Mobile Dashboard") { (success, error) in

            DispatchQueue.main.async {

                if (success)
                {
                    self.blockingView.isHidden = true
                    self.view.sendSubview(toBack: self.blockingView)

                    self.processLogin(fromTouchID: true)
                }
                else
                {
                    let when = DispatchTime.now() + 0.25 // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {

                        self.blockingView.isHidden = true
                        self.view.sendSubview(toBack: self.blockingView)

                        self.selectPINEntryScreen()
                    }
                }
            }
        }
    }

    // MARK: PinEntryViewControllerDelegate
    
    func pinEntrySuccessCode(pinCode : String, pinMode : CLPinMode)
    {
        if(pinMode == .verifyPin)
        {
            self.processLogin(fromTouchID: false)
        }
        self.activeLogIn = false;
    }
    
    func pinEntryFailure(count : Int)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func pinEntryCancel()
    {
        if let _ = DashboardSettingsObject.shared.userPIN
        {
            self.selectPINEntryScreen()
        }
        else
        {
            self.selectLogInScreen()
        }
    }

    func displayShare(chartPage : ChartPageDataObject, forButton : UIBarButtonItem)
    {
        let avc = UIActivityViewController.init(activityItems: [chartPage], applicationActivities: nil)
        
        avc.popoverPresentationController?.barButtonItem = forButton
        
        /*avc.completionWithItemsHandler = {(activityType, completed:Bool, returnedItems:[AnyObject]?, error: NSError?) in
            
            // Return if cancelled
            if (!completed) {
                return
            }
            
            //activity complete
            //some code here
            
            
        }*/
        self.present(avc, animated: true, completion: nil)
        
    }

    static func previewForChartPage(chartPageDataObject : ChartPageDataObject,  iPhoneSize : Bool, completion: @escaping ((_ successful : Bool)->Void)) {
        
        if(chartPageDataObject.processingData)
        {
            return;
        }
        
        chartPageDataObject.processingData = true
        // Try send back a notification when the item is complete to whoever needs it
        
        
//        chartPageDataObject.refreshCharts(completion: { (successful, downlaoded, dataArray) in
        
//            completion(true)

        var params = [String : Any]()
        params["completion"] = completion
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let fourWayViewController : FourWayViewController = storyboard.instantiateViewController(withIdentifier: "chartVC") as! FourWayViewController
        
        fourWayViewController.chartPageObject = chartPageDataObject
        fourWayViewController.displayMode = true
        
//        let iPhoneSize = false
        if(iPhoneSize)
        {
            fourWayViewController.view.frame = CGRect(x:0, y:0, width:320, height:568)
        }
        else
        {
            fourWayViewController.view.frame = CGRect(x:0, y:0, width:1024, height:768)
        }
        //        chartViewController.select(chartType: chartType)
        
        DashboardRootViewController.rootViewController().view.isHidden = true
        DashboardRootViewController.rootViewController().view.addSubview(fourWayViewController.view)
        DashboardRootViewController.rootViewController().view.sendSubview(toBack: fourWayViewController.view)
        fourWayViewController.chartPageObject = chartPageDataObject;
        fourWayViewController.screenLayout = .OneByOne1
        if(fourWayViewController.chartPageObject!.chartCount() > 2)
        {
            fourWayViewController.screenLayout = .TwoByTwo
        }
        else if(fourWayViewController.chartPageObject!.chartCount() > 1)
        {
            fourWayViewController.screenLayout = .TwoByOne
        }
        fourWayViewController.view.setNeedsLayout()
        fourWayViewController.view.layoutIfNeeded()
        DashboardRootViewController.rootViewController().view.isHidden = false
        
        params["viewController"] = fourWayViewController
        params["chartPageDataObject"] = chartPageDataObject
        
//        perform(#selector(DashboardRootViewController.takeImage), with: params, afterDelay: 0.0)
        perform(#selector(DashboardRootViewController.takeImage), with: params, afterDelay: 1.5)
//        })
    }
    
    static func processChartPage(dict : [String : Any])
    {
        let chartPageDataObject : ChartPageDataObject = dict["chartPageDataObject"] as!  ChartPageDataObject

        if(chartPageDataObject.chartsHaveData())
        {
            perform(#selector(DashboardRootViewController.takeImage), with: dict, afterDelay: 0.5)
//            print("Processing Completed . . .")
        }
        else
        {
            perform(#selector(DashboardRootViewController.processChartPage), with: dict, afterDelay: 0.5)
//            print("Processing . . .")
        }
    }
    
    static func takeImage(dict : [String : Any])
    {
        let chartPageDataObject : ChartPageDataObject = dict["chartPageDataObject"] as!  ChartPageDataObject

        let fourWayViewController : FourWayViewController = dict["viewController"] as! FourWayViewController
        
        let completion : ((_ successful : Bool)->Void) = dict["completion"] as! ((_ successful : Bool)->Void)
        
        //let image : UIImage? = UIImage.init(named: "ScreenShot")
        var image : UIImage? = DashboardRootViewController.imageWithView(view: fourWayViewController.view, scale: 1.0)
        let rect : CGRect = CGRect(x: 0, y: 20, width:(image?.size.width)!, height:((image?.size.height)! - 20))
        
        let imageRef:CGImage = image!.cgImage!.cropping(to: rect)!
        image = UIImage.init(cgImage:imageRef)
        
        let url = chartPageDataObject.chartImageCacheURL()
        do
        {
            
            try UIImagePNGRepresentation(image!)?.write(to: url)
            
        }
        catch
        {
            print(error)
        }
        
        if(image == nil)
        {
            print("No Image")
        }
//        else
//        {
//            print("Image Available")
//        }
        chartPageDataObject.processingData = false

        completion((image != nil))
        fourWayViewController.view.removeFromSuperview()
    }
    
    static func imageWithView(view : UIView, scale : CGFloat) -> UIImage?
    {
        var viewScale : CGFloat = scale
        if ( viewScale < 0.0)
        {
            viewScale = 0.0
        }
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, viewScale)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        
        let image : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    

}
