//
//  ChartViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/29/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit
import Charts

class ChartViewController: UIViewController, ChartViewDelegate, IAxisValueFormatter {

    var enableAnimation : Bool = true
    
    var chartPageObject : ChartPageDataObject?
    var chartObject : ChartDataObject?
    
    weak var activeChartView : ChartViewBase?
    
    var useCache : Bool = true
    var saveCache : Bool = true
    var displayFeedback : Bool = false
    
    weak var chartPageDelegate : ChartPageDelegate?
    
    var xAxisLabels : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    deinit {

        print("deinit - ChartViewController")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if(self.chartObject != nil)
        {
            self.generateChart(chartDataObject: self.chartObject!, completion: { (successful) in
                
                if(self.chartPageDelegate != nil)
                {
                    self.chartPageDelegate!.chartRenderComplete(chartViewController: self)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func generateChart(chartDataObject : ChartDataObject, completion : ((_ successful : Bool)->Void)? )
    {
        self.chartObject = chartDataObject
        
        if(self.displayFeedback)
        {
            HudController.sharedHud().showHUD(self.view, title: "Downloading Data")
        }
        let chartDataSource : ChartDataSource? = DashboardDataObject.shared.chartDataSources[self.chartObject!.dataSourceIdentifier]
//        if(chartDataSource != nil)
//        {
            var chartResults : [String : Int] = [:]
            var chartGroupResults : [String : [[String : Int]]] = [:]
            self.chartObject?.getChartData(chartSource: chartDataSource, loadCache : self.useCache, saveCache:self.saveCache, completion: { (successful, downloaded, dataArray) in
                
                DispatchQueue.main.async {

//                    if(downloaded)
//                    {
//                        if(self.chartPageObject != nil)
//                        {
//                            self.chartPageObject!.deleteCacheImage()
//                        }
//                    }
                    
                    if(self.displayFeedback)
                    {
                        HudController.sharedHud().hideHUD(self.view)
                    }
                    
                    if(successful && dataArray != nil)
                    {
                        // @Warning: Handle the inability to download initial data or the cached data does not exist
                        
                        for dict in dataArray!
                        {
                            //Warning:  This will crash it accumulator is not an Int, fix so that it accumulates for the count of the value for the column
                            let column : String = dict[chartDataObject.axisColumn!] as! String
                            var columnVal : Int = 0
                            if(chartDataObject.accumulatorColumn == "[Counter]")
                            {
                                columnVal = 1
                            }
                            else
                            {
                                columnVal = dict[chartDataObject.accumulatorColumn!] as! Int
                            }
                            
                            let results : Int? = chartResults[column]
                            if (results == nil)
                            {
                                chartResults[column] = columnVal
                            }
                            else
                            {
                                columnVal = results! + columnVal;
                                chartResults[column] = columnVal
                            }
                            
                            if(self.chartObject?.groupingColumn != nil && self.chartObject?.groupingColumn != "")// && self.chartObject?.groupingColumn != "[No Grouping]")
                            {
                                let axisVal : String? = dict[self.chartObject!.axisColumn!] as! String?
                                let colVal : String? = dict[self.chartObject!.groupingColumn!] as! String?
                                var groupVal = 1
                                if(chartDataObject.accumulatorColumn != "[Counter]")
                                {
                                    groupVal = dict[chartDataObject.accumulatorColumn!] as! Int
                                }
                                if(axisVal != nil)
                                {
                                    var currentArray = chartGroupResults[axisVal!]
                                    if(currentArray != nil)
                                    {
                                        var found = false
                                        for var currentDict in currentArray!
                                        {
                                            if(currentDict[colVal!] != nil)
                                            {
                                                found = true
                                                let foundGroupVal = currentDict[colVal!]
                                                currentDict[colVal!] = foundGroupVal! + groupVal
                                                break
                                            }
                                            
                                        }
                                        if(!found)
                                        {
                                            currentArray?.append([colVal!: groupVal])
                                            chartGroupResults[axisVal!] = currentArray
                                        }
                                    }
                                    else
                                    {
                                        chartGroupResults[axisVal!] = [[colVal!: groupVal]]
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if(self.displayFeedback)
                        {
                            let alertController : UIAlertController = UIAlertController.init(title: "Error", message: "Unable to download data.", preferredStyle: .alert)
                            let action = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                            alertController.addAction(action)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    self.clearChart()
                    
                    if(chartDataObject.chartType == .barChart)
                    {
                        self.createBarChart(data: chartResults, horizontal : false)
                    }
                    else if(chartDataObject.chartType == .horizonatalBarChart)
                    {
                        self.createBarChart(data: chartResults, horizontal : true)
                    }
                    else if(chartDataObject.chartType == .stackedBarChart)
                    {
                        self.createStackedBarChart(data: chartGroupResults, horizontal : false)
                    }
                    else if(chartDataObject.chartType == .horizonatalStackedBarChart)
                    {
                        self.createStackedBarChart(data: chartGroupResults, horizontal : true)
                    }
                    else if(chartDataObject.chartType == .pieChart)
                    {
                        self.createPieChart(dataArray: chartResults)
                    }
                    else if(chartDataObject.chartType == .singleLineChart)
                    {
                        self.createSingleLineChart(data: chartResults)
                    }
                    else if(chartDataObject.chartType == .lineChart)
                    {
                        self.createLineChart(data: chartGroupResults)
                    }
                    else if(chartDataObject.chartType == .scatterChart)
                    {
                        self.createScatterChart(data : chartGroupResults)
                    }
                    else if(chartDataObject.chartType == .bubbleChart)
                    {
                        self.setBubbleChart()
                    }
                    else if(chartDataObject.chartType == .cancelstickChart)
                    {
                        self.setCandlestickChart()
                    }
                    else if(chartDataObject.chartType == .radarChart)
                    {
                        self.setRadarChart()
                    }
                    
                    if(completion != nil)
                    {
                        completion!(successful)
                    }
                }
                
            })
//        }
//        else
//        {
//            completion!(false)
//        }
    }

    func clearChart()
    {
        for view : UIView in self.view.subviews
        {
            if( view.tag == 500)
            {
                view.removeFromSuperview()
            }
        }
    }
    
    func stringForValue(_ value: Double,
                        axis: AxisBase?) -> String
    {
        
        let str = self.xAxisLabels[Int(value) % xAxisLabels.count];
        
        return str
    }
    
    func resetChartDimensions()
    {
        if let barChartView = self.activeChartView as? BarChartView
        {
            barChartView.fitScreen()
        }
        if let barChartView = self.activeChartView as? LineChartView
        {
            barChartView.fitScreen()
        }
        self.activeChartView?.highlightValue(nil)
        self.activeChartView?.setNeedsDisplay()

    }
    
    private func createBarChart(data : [String : Int], horizontal : Bool)
    {
        var barChartView : BarChartView? = nil
        
        if(horizontal)
        {
            barChartView = HorizontalBarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        }
        else
        {
            barChartView = BarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        }

        // Chart Creation
        barChartView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        barChartView!.delegate = self
        barChartView!.noDataText = "No data available for the chart."
        barChartView!.noDataFont = UIFont(name: "HelveticaNeue", size: 32.0)
        barChartView!.tag = 500
        self.view.addSubview(barChartView!)
        self.activeChartView = barChartView

        // Chart general settings
        if let desc = self.chartObject!.chartDescription
        {
            if desc.lengthOfBytes(using: .utf8) > 0
            {
                barChartView!.chartDescription?.text = desc
                barChartView!.chartDescription?.enabled = true
                barChartView!.chartDescription?.font = NSUIFont.systemFont(ofSize: 14.0)
            }
            else
            {
                barChartView!.chartDescription?.enabled = false
            }
        }
        else
        {
             barChartView!.chartDescription?.enabled = false
        }
        
        let leftAxis = barChartView!.leftAxis
        leftAxis.axisMinimum = 0.0
        
        let rightAxis = barChartView!.rightAxis
        rightAxis.axisMinimum = 0.0
        rightAxis.enabled = false
        
        // Configure popup marker
        let marker : XYMarkerView? = XYMarkerView.init(color: .gray, font: UIFont.systemFont(ofSize: 14.0), textColor: .white, insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0), xAxisValueFormatter: self)
        marker!.chartView = barChartView;
        marker?.minimumSize = CGSize.init(width: 80.0, height: 40.0)
        barChartView!.marker = marker;
        
        barChartView!.backgroundColor = self.chartObject!.chartBackgroundColor
        
        let xAxis = barChartView!.xAxis;
        if(self.chartObject!.xAxisLabelsCount > 0)
        {
            xAxis.labelPosition = .bottom
            xAxis.labelFont = UIFont.systemFont(ofSize: 14.0)
            xAxis.drawGridLinesEnabled = true;
            xAxis.granularity = 1.0; // only intervals of 1 day
            xAxis.labelCount = self.chartObject!.xAxisLabelsCount
//            xAxis.valueFormatter = self
            xAxis.enabled = true;
        }
        else
        {
            xAxis.enabled = false;
        }
        
        if(self.chartObject?.gridLinesType == .xAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            xAxis.drawGridLinesEnabled = true
            xAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            xAxis.drawGridLinesEnabled = false
        }
        if(self.chartObject?.gridLinesType == .yAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            leftAxis.drawGridLinesEnabled = true
            leftAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            leftAxis.drawGridLinesEnabled = false
        }

        barChartView!.borderColor = self.chartObject!.borderColor
        barChartView!.borderLineWidth = CGFloat(self.chartObject!.borderWidth)
        if(self.chartObject!.borderWidth > 0)
        {
            barChartView!.drawBordersEnabled = true
        }
        else
        {
            barChartView!.drawBordersEnabled = false
        }
        
        /*
         let legend = barChartView!.legend;
         legend.horizontalAlignment = .right
         legend.verticalAlignment = .bottom
         legend.orientation = .horizontal
         legend.direction = .leftToRight
         legend.form = .square;
         legend.formSize = 8.0;
         legend.formToTextSpace = 4.0;
         legend.xEntrySpace = 6.0;
         */

        // Bar Chart configuration
        if(self.chartObject!.displayValueLabelLocation == .hide || self.chartObject!.displayValueLabelLocation == .showAbove)
        {
            barChartView!.drawValueAboveBarEnabled = true
        }
        else
        {
            barChartView!.drawValueAboveBarEnabled = false
        }

        // Process the chart's data
        
        var chartData : BarChartData? = nil

        // Load the chart up with data
        if(data.count > 0)
        {
            // Read the label names
            self.xAxisLabels = []
            for (item, _) in data
            {
                self.xAxisLabels.append(item)
            }
            self.xAxisLabels = self.xAxisLabels.sorted()
            
            // For each label get the corresponding data, in order
            var dataValues : [Int] = []
            for label in self.xAxisLabels
            {
                let dataValue = data[label]
                dataValues.append(dataValue!)
            }
            
            var dataEntries: [BarChartDataEntry] = []
            
            for cnt in 0..<self.xAxisLabels.count
            {
                let dataEntry = BarChartDataEntry(x: Double(cnt), y: Double(dataValues[cnt]), data: self.xAxisLabels as AnyObject?)
                dataEntries.append(dataEntry)
            }
            
            let chartDataSet = BarChartDataSet(values: dataEntries, label: self.chartObject?.accumulatorColumn)
            chartData = BarChartData(dataSet: chartDataSet)
            if(self.chartObject!.displayValueLabelLocation == .hide)
            {
                chartData?.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 0.0))
            }
            else
            {
                chartData?.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 12.0))
            }
            
            if(self.chartObject!.displayAsSingleColor)
            {
                chartDataSet.colors = [self.chartObject!.chartColors![0]]
            }
            else
            {
                chartDataSet.colors = self.chartObject!.chartColors!
            }
            
            // Setup animations
            if (self.enableAnimation)
            {
                if(self.chartObject!.animationType == .xAxis)
                {
                    barChartView!.animate(xAxisDuration: 2.0)
                }
                else if(self.chartObject!.animationType == .yAxis)
                {
                    barChartView!.animate(yAxisDuration: 2.0)
                }
                else if(self.chartObject!.animationType == .xyAxis)
                {
                    barChartView!.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
                }
            }
        }
        
        barChartView!.data = chartData
    }
    
    /*private func setBarChart()
    {
        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]//, "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0] //, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0

        let barChartView = BarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        barChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        barChartView.delegate = self
        self.view.addSubview(barChartView)
//        barChartView.drawValueAboveBarEnabled = false
        
        let xAxis : XAxis = barChartView.xAxis;
        barChartView.xAxis.labelFont = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(14.0))!
        xAxis.xOffset = 0.0;
        xAxis.yOffset = 0.0;
        xAxis.valueFormatter = self;
        xAxis.labelTextColor = UIColor.black;

        
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.isUserInteractionEnabled = false
        
        var dataEntries: [BarChartDataEntry] = []
        
        for cnt in 0..<months.count {
            let dataEntry = BarChartDataEntry(x: Double(cnt), y: unitsSold[cnt], data: months as AnyObject?)
            dataEntries.append(dataEntry)
        }
        
        barChartView.chartDescription?.text = "User Data"
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
//        chartDataSet.stackLabels = months
        let chartData = BarChartData(dataSet: chartDataSet)
        //        chartDataSet.colors = [UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        chartDataSet.colors = ChartColorTemplates.colorful()
        barChartView.backgroundColor = UIColor(red: 189/255, green: 195/255, blue: 199/255, alpha: 1)
        
        if (self.enableAnimation)
        {
            barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        }
        //        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        let ll = ChartLimitLine(limit: 10.0, label: "Target")
        barChartView.rightAxis.addLimitLine(ll)
        
        barChartView.data = chartData
    }*/
    
    private func createStackedBarChart(data : [String : [[String : Int]]], horizontal : Bool)
    {
        var barChartView : BarChartView? = nil
        
        if(horizontal)
        {
            barChartView = HorizontalBarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        }
        else
        {
            barChartView = BarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        }

        barChartView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        barChartView!.delegate = self
        barChartView!.noDataText = "No data available for the chart."
        barChartView!.noDataFont = UIFont(name: "HelveticaNeue", size: 32.0)
        barChartView!.tag = 500
        self.view.addSubview(barChartView!)
        self.activeChartView = barChartView

        // Bar Chart general settings
        if let desc = self.chartObject!.chartDescription
        {
            if desc.lengthOfBytes(using: .utf8) > 0
            {
                barChartView!.chartDescription?.text = desc
                barChartView!.chartDescription?.enabled = true
                barChartView!.chartDescription?.font = NSUIFont.systemFont(ofSize: 14.0)
            }
            else
            {
                barChartView!.chartDescription?.enabled = false
            }
        }
        else
        {
            barChartView!.chartDescription?.enabled = false
        }
        
        let leftAxis = barChartView!.leftAxis
        leftAxis.axisMinimum = 0.0

        let rightAxis = barChartView!.rightAxis
        rightAxis.axisMinimum = 0.0
        rightAxis.enabled = false
        
        // Configure popup marker
        let marker : XYMarkerView? = XYMarkerView.init(color: .gray, font: UIFont.systemFont(ofSize: 14.0), textColor: .white, insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0), xAxisValueFormatter: self)
        marker!.chartView = barChartView;
        marker?.minimumSize = CGSize.init(width: 80.0, height: 40.0)
        barChartView!.marker = marker;

        // Bar Chart configuration
        if(self.chartObject!.displayValueLabelLocation == .hide || self.chartObject!.displayValueLabelLocation == .showAbove)
        {
            barChartView!.drawValueAboveBarEnabled = true
        }
        else
        {
            barChartView!.drawValueAboveBarEnabled = false
        }
        barChartView!.backgroundColor = self.chartObject!.chartBackgroundColor
        
        let xAxis = barChartView!.xAxis;
        if(self.chartObject!.xAxisLabelsCount > 0)
        {
            xAxis.labelPosition = .bottom
            xAxis.labelFont = UIFont.systemFont(ofSize: 14.0)
            xAxis.drawGridLinesEnabled = true;
            xAxis.granularity = 1.0; // only intervals of 1 day
            xAxis.labelCount = self.chartObject!.xAxisLabelsCount
//            xAxis.valueFormatter = self
            xAxis.enabled = true;
        }
        else
        {
            xAxis.enabled = false;
        }
        if(self.chartObject?.gridLinesType == .xAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            xAxis.drawGridLinesEnabled = true
            xAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            xAxis.drawGridLinesEnabled = false
        }
        if(self.chartObject?.gridLinesType == .yAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            leftAxis.drawGridLinesEnabled = true
            leftAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            leftAxis.drawGridLinesEnabled = false
        }
        

        barChartView!.borderColor = self.chartObject!.borderColor
        barChartView!.borderLineWidth = CGFloat(self.chartObject!.borderWidth)
        
        if(self.chartObject!.borderWidth > 0)
        {
            barChartView!.drawBordersEnabled = true
        }
        else
        {
            barChartView!.drawBordersEnabled = false
        }
        
/*
        let legend = barChartView!.legend;
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .bottom
        legend.orientation = .horizontal
        legend.direction = .leftToRight
        legend.form = .square;
        legend.formSize = 8.0;
        legend.formToTextSpace = 4.0;
        legend.xEntrySpace = 6.0;
*/
        
        // Process the chart's data
        
        self.xAxisLabels = []
        
        for (item, _) in data
        {
            self.xAxisLabels.append(item)
        }
        self.xAxisLabels = data.keys.sorted(by: { (key1, key2) -> Bool in
        
            if(horizontal)
            {
                if(key1.compare(key2) == .orderedDescending)
                {
                    return true
                }
            }
            else
            {
                if(key1.compare(key2) == .orderedAscending)
                {
                    return true
                }
            }
            return false
        })
        
        var stackLabels : [String] = []
        for name in self.xAxisLabels
        {
            let dataArray = data[name]
            
            for dataDict in dataArray!
            {
                for dataName in dataDict.keys
                {
                    if(!stackLabels.contains(dataName))
                    {
                        stackLabels.append(dataName)
                    }
                }
            }

        }
        stackLabels = stackLabels.sorted()
        
        var yVals = [BarChartDataEntry]()
        
        var counter : Int = 0;
        for name in self.xAxisLabels
        {
            let dataArray = data[name]
            
            var dataResultsArray : [Double] = []
            for dataDict in dataArray!
            {
                for item in stackLabels
                {
                    let dataValue : Int? = dataDict[item]

                    if let itemDataValue = dataValue
                    {
                        dataResultsArray.append(Double(itemDataValue))
                    }
                    else
                    {
                        dataResultsArray.append(Double(0))
                    }
                }
            }
            yVals.append(BarChartDataEntry.init(x: Double(counter), yValues: dataResultsArray))
            
            counter = counter + 1
        }
        
        var set1 : BarChartDataSet?
        
        set1 = BarChartDataSet.init(values: yVals, label: self.chartObject?.chartDescription)
        
        if(self.chartObject!.chartColors!.count > stackLabels.count)
        {
            var colors : [UIColor] = []
            for cnt in 0..<stackLabels.count
            {
                let color = self.chartObject!.chartColors![cnt]
                colors.append(color)
            }
            set1!.colors = colors
        }
        else
        {
            set1!.colors = self.chartObject!.chartColors!
        }
        set1!.stackLabels = stackLabels
        
        var dataSets = [BarChartDataSet]()
        dataSets.append(set1!)
        
        let chartdata : BarChartData = BarChartData.init(dataSets: dataSets)
        if(self.chartObject!.displayValueLabelLocation == .hide)
        {
            chartdata.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 0.0))
        }
        else
        {
            chartdata.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 12.0))
        }
        
        // Setup animations
        if (self.enableAnimation)
        {
            if(self.chartObject!.animationType == .xAxis)
            {
                barChartView!.animate(xAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .yAxis)
            {
                barChartView!.animate(yAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .xyAxis)
            {
                barChartView!.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
            }
        }
        
        barChartView!.data = chartdata
    }
    
    
    /*func setStackedBarChart()
    {
        let months = ["Jan", "Feb", "Mar", "Apr", "May"]//, "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        
        let barChartView = BarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        barChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        barChartView.delegate = self
        barChartView.tag = 500
        self.view.addSubview(barChartView)
        barChartView.delegate = self;
        
        barChartView.chartDescription?.text = ""
        
        //        barChartView.maxVisibleValueCount = 60
        barChartView.pinchZoomEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        barChartView.drawBarShadowEnabled = false
        barChartView.drawValueAboveBarEnabled = false
        
        let leftAxis = barChartView.leftAxis;
        //        leftAxis.valueFormatter = NumberFormatter.init()
        //        leftAxis.valueFormatter.maximumFractionDigits = 1;
        //        leftAxis.valueFormatter.negativeSuffix = " $";
        //        leftAxis.valueFormatter.positiveSuffix = " $";
        leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        barChartView.rightAxis.enabled = false;
        
        let xAxis = barChartView.xAxis;
        xAxis.labelPosition = .top;
        
        let legend = barChartView.legend;
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .bottom
        legend.orientation = .horizontal
        legend.direction = .leftToRight
        legend.form = .square;
        legend.formSize = 8.0;
        legend.formToTextSpace = 4.0;
        legend.xEntrySpace = 6.0;
        
        
        var xVals = [String]()
        let count : Int = 5
        let range : Double = 25.0
        
        for i in 0..<count
        {
            xVals.append(months[i % 12])
        }
        
        var yVals = [BarChartDataEntry]()
        
        for i in 0..<count
        {
            let mult : Double = (range + 1);
            let val1 : Double = Double(arc4random_uniform(UInt32(mult))) + (mult / 3.0)
            let val2 : Double = Double(arc4random_uniform(UInt32(mult))) + (mult / 3.0)
            let val3 : Double = Double(arc4random_uniform(UInt32(mult))) + (mult / 3.0)
            //            let val4 : Double = Double(arc4random_uniform(UInt32(mult))) + (mult / 3.0)
            //            let val5 : Double = Double(arc4random_uniform(UInt32(mult))) + (mult / 3.0)
            
            yVals.append(BarChartDataEntry.init(x: Double(i), yValues: [val1, val2, val3/*, val4, val5*/]))
        }
        
        var set1 : BarChartDataSet?
        
        //        if (barChartView.data?.dataSetCount > 0)
        //        {
        //            set1 = barChartView.data?.dataSets[0] as? BarChartDataSet
        //            set1.yVals = yVals;
        //            barChartView.data.xValsObjc = xVals;
        //            [barChartView notifyDataSetChanged];
        //        }
        //        else
        //        {
        set1 = BarChartDataSet.init(values: yVals, label: "Statistics Vienna 2014")  //  [[BarChartDataSet alloc] initWithValues:yVals label:"DataSet"];
        
        set1!.colors = [ChartColorTemplates.vordiplom()[0], ChartColorTemplates.vordiplom()[1], ChartColorTemplates.vordiplom()[2]]
        set1!.stackLabels = ["Births", "Divorces", "Marriages"]
        
        var dataSets = [BarChartDataSet]()
        dataSets.append(set1!)
        
        let formatter : NumberFormatter = NumberFormatter.init()
        formatter.maximumFractionDigits = 1;
        formatter.negativeSuffix = " $";
        formatter.positiveSuffix = " $";
        
        let data : BarChartData = BarChartData.init(dataSets: dataSets)
        data.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 7.0))
        //            data.setValueFormatter(formatter)
        
        barChartView.data = data;
        //        }
    }*/

    func setBubbleChart()
    {
        let bubbleChartView = BubbleChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        bubbleChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        bubbleChartView.delegate = self
        self.view.addSubview(bubbleChartView)
//        bubbleChartView.isUserInteractionEnabled = false
        self.activeChartView = bubbleChartView

        var yVals1 : [BubbleChartDataEntry] = []
//        var yVals2 : [BubbleChartDataEntry] = []
//        var yVals3 : [BubbleChartDataEntry] = []
        
        for cnt in 0..<100
        {
            var val : Double = Double(arc4random_uniform(UInt32(100.0)))
            var size : Double = 1.0//Double(arc4random_uniform(UInt32(100.0)))
            yVals1.append(BubbleChartDataEntry.init(x: Double(cnt), y: val, size: CGFloat(size)))
            
            /*val = Double(arc4random_uniform(UInt32(100.0)))
            size =  Double(arc4random_uniform(UInt32(100.0)))
            yVals2.append(BubbleChartDataEntry.init(x: Double(cnt), y: val, size: CGFloat(size)))
            
            val = Double(arc4random_uniform(UInt32(100.0)))
            size =  Double(arc4random_uniform(UInt32(100.0)))
            yVals3.append(BubbleChartDataEntry.init(x: Double(cnt), y: val, size: CGFloat(size)))*/
        }
        
        let set1 = BubbleChartDataSet.init(values: yVals1, label: "DS 1")
        set1.setColor(UIColor.red, alpha: 0.50)
        set1.drawValuesEnabled = true
        
        /*let set2 = BubbleChartDataSet.init(values: yVals2, label: "DS 2")
        set2.setColor(UIColor.green, alpha: 0.50)
        set2.drawValuesEnabled = true
        
        let set3 = BubbleChartDataSet.init(values: yVals3, label: "DS 3")
        set3.setColor(UIColor.blue, alpha: 0.50)
        set3.drawValuesEnabled = true*/
        
        let dataSets : [BubbleChartDataSet] = [set1]//, set2, set3]
        
        let data : BubbleChartData = BubbleChartData.init()
        data.dataSets = dataSets
        data.setDrawValues(false)
        data.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 7.0))
        data.setHighlightCircleWidth(1.5)
        data.setValueTextColor(UIColor.white)
        
        if (self.enableAnimation)
        {
            bubbleChartView.animate(yAxisDuration: 3.0)
        }
        
        bubbleChartView.data = data
    }
    
    func setCandlestickChart()
    {
        let candlestickChartView = CandleStickChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        candlestickChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        candlestickChartView.delegate = self
        self.view.addSubview(candlestickChartView)
//        candlestickChartView.isUserInteractionEnabled = false

        self.activeChartView = candlestickChartView
        
        var yVals1 : [CandleChartDataEntry] = []
        
        for cnt in 0..<40
        {
            let mult: Double = 101.0
            let val = Double(arc4random_uniform(UInt32(40))) + mult
            let high = Double(arc4random_uniform(UInt32(40))) + 8.0
            let low = Double(arc4random_uniform(UInt32(40))) + 8.0
            let open = Double(arc4random_uniform(UInt32(40))) + 1.0
            let close = Double(arc4random_uniform(UInt32(40))) + 1.0
            let even : Bool = (cnt % 2 == 0)
            
            yVals1.append(CandleChartDataEntry.init(x: Double(cnt), shadowH: val + high, shadowL: val - low, open: val + open, close: even ? val - close : val + close))
        }
        
        let set1 = CandleChartDataSet.init(values: yVals1, label: "Data Set")
        set1.axisDependency = .left
        set1.setColor(UIColor(white: 80/255, alpha: CGFloat(1.0)))
        
        set1.shadowColor = UIColor.darkGray;
        set1.shadowWidth = 0.7;
        set1.decreasingColor = UIColor.red;
        set1.decreasingFilled = true;
        set1.increasingColor = UIColor(red:CGFloat(122/255), green:(242/255), blue:CGFloat(84/255), alpha:CGFloat(1.0));
        set1.increasingFilled = false;
        set1.neutralColor = UIColor.blue;

        let data = CandleChartData.init(dataSets: [set1])
        
        if (self.enableAnimation)
        {
            candlestickChartView.animate(yAxisDuration: 3)
        }
        
        candlestickChartView.data = data
    }
    /*
    func createHorizonatalBarChart(data : [String : Int])
    {
        let horizonatalBarChartChartView = HorizontalBarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        horizonatalBarChartChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        horizonatalBarChartChartView.delegate = self
        horizonatalBarChartChartView.noDataText = "No data available for the chart."
        horizonatalBarChartChartView.isUserInteractionEnabled = false
        self.view.addSubview(horizonatalBarChartChartView)
        horizonatalBarChartChartView.tag = 500

        horizonatalBarChartChartView.chartDescription?.enabled = false;
        
        horizonatalBarChartChartView.drawGridBackgroundEnabled = false;
        
//        horizonatalBarChartChartView.dragEnabled = true;
//        horizonatalBarChartChartView.setScaleEnabled(true);
//        horizonatalBarChartChartView.pinchZoomEnabled = false;
        
        // ChartYAxis *leftAxis = chartView.leftAxis;
        
        horizonatalBarChartChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom;
        horizonatalBarChartChartView.rightAxis.enabled = false;
        
        
        horizonatalBarChartChartView.drawBarShadowEnabled = false;
        horizonatalBarChartChartView.drawValueAboveBarEnabled = true;
        
//        horizonatalBarChartChartView.maxVisibleCount = 60;
        
        //        let xAxis : XAxis = horizonatalBarChartChartView.xAxis;
        horizonatalBarChartChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        horizonatalBarChartChartView.xAxis.labelFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        horizonatalBarChartChartView.xAxis.drawAxisLineEnabled = true;
        horizonatalBarChartChartView.xAxis.drawGridLinesEnabled = false;
        horizonatalBarChartChartView.xAxis.granularity = 10.0;
        
        //        ChartYAxis *leftAxis = horizonatalBarChartChartView.leftAxis;
        horizonatalBarChartChartView.leftAxis.labelFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        horizonatalBarChartChartView.leftAxis.drawAxisLineEnabled = true;
        horizonatalBarChartChartView.leftAxis.drawGridLinesEnabled = true;
        horizonatalBarChartChartView.leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        //        ChartYAxis *rightAxis = horizonatalBarChartChartView.rightAxis;
        horizonatalBarChartChartView.rightAxis.enabled = true;
        horizonatalBarChartChartView.rightAxis.labelFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        horizonatalBarChartChartView.rightAxis.drawAxisLineEnabled = true;
        horizonatalBarChartChartView.rightAxis.drawGridLinesEnabled = false;
        horizonatalBarChartChartView.rightAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        //        let legend : Legend = horizonatalBarChartChartView.legend;
        horizonatalBarChartChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.left
        horizonatalBarChartChartView.legend.verticalAlignment = Legend.VerticalAlignment.bottom
        horizonatalBarChartChartView.legend.orientation = Legend.Orientation.horizontal;
        horizonatalBarChartChartView.legend.drawInside = false;
        horizonatalBarChartChartView.legend.form = Legend.Form.square;
        horizonatalBarChartChartView.legend.formSize = 8.0;
        horizonatalBarChartChartView.legend.font = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(11.0))! // [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
        horizonatalBarChartChartView.legend.xEntrySpace = 4.0;
        
        horizonatalBarChartChartView.fitBars = true
        
        if (self.enableAnimation)
        {
            if(self.chartObject!.animationType == .xAxis)
            {
                horizonatalBarChartChartView.animate(xAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .yAxis)
            {
                horizonatalBarChartChartView.animate(yAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .xyAxis)
            {
                horizonatalBarChartChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
            }
        }
        
        horizonatalBarChartChartView.backgroundColor = self.chartObject?.chartBackgroundColor
        
        
        self.xAxisLabels = []
        var dataValues : [Int] = []
        
        for (item, dataValue) in data
        {
            self.xAxisLabels.append(item)
            dataValues.append(dataValue)
        }
        
        var dataEntries: [BarChartDataEntry] = []
        
        for cnt in 0..<self.xAxisLabels.count {
            let dataEntry = BarChartDataEntry(x: Double(cnt), y: Double(dataValues[cnt]), data: self.xAxisLabels as AnyObject?)
            //            let dataEntry = BarChartDataEntry(x: Double(cnt), yValues: [Double(dataValues[cnt]), Double(dataValues[cnt])])
            dataEntries.append(dataEntry)
        }

        
        
        
        
        let xAxis = horizonatalBarChartChartView.xAxis;
        xAxis.labelPosition = .bottom
        xAxis.labelFont = UIFont.systemFont(ofSize: 10.0)
        xAxis.drawGridLinesEnabled = true;
        xAxis.granularity = 1.0; // only intervals of 1 day
        xAxis.labelCount = 7;
        xAxis.valueFormatter = self
        //        xAxis.valueFormatter = [[DayAxisValueFormatter alloc] initForChart:_chartView];
        
        horizonatalBarChartChartView.chartDescription?.text = self.chartObject?.chartDescription//axisColumn
        let leftAxis = horizonatalBarChartChartView.leftAxis
        leftAxis.axisMinimum = 0.0
        
        let rightAxis = horizonatalBarChartChartView.rightAxis
        rightAxis.axisMinimum = 0.0

        
        
        
        
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: self.chartObject?.accumulatorColumn)
        //        chartDataSet.stackLabels = months
        let chartData = BarChartData(dataSet: chartDataSet)
        //        chartDataSet.colors = [UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
        chartDataSet.colors = self.chartObject!.chartColors!

        horizonatalBarChartChartView.data = chartData
        
        /*let barWidth : Double = 9.0;
        let spaceForBar : Int = 10;
        
        var yVals : [BarChartDataEntry] = [];
        
        for i in 0..<25
        {
            let mult : Double = (50 + 1);
            let val :Double = Double((arc4random_uniform(UInt32(mult))))
            yVals.append(BarChartDataEntry.init(x: Double(i * spaceForBar), y: val))// [[BarChartDataEntry alloc] initWithX:i * spaceForBar y:val]];
        }
        
        let set1 = BarChartDataSet.init(values: yVals, label: "DataSet")  //  [[BarChartDataSet alloc] initWithValues:yVals label:"DataSet"];
        
        var dataSets : [BarChartDataSet] = [];
        dataSets.append(set1)
        
        let data : BarChartData = BarChartData.init(dataSets: dataSets)
        data.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(10.0))) // setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
        data.barWidth = barWidth;
        
        horizonatalBarChartChartView.data = data;*/
    }
*/
    /*
    func setHorizonatalBarChart()
    {
        let horizonatalBarChartChartView = HorizontalBarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        horizonatalBarChartChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        horizonatalBarChartChartView.delegate = self
        self.view.addSubview(horizonatalBarChartChartView)
        horizonatalBarChartChartView.isUserInteractionEnabled = false

        horizonatalBarChartChartView.chartDescription?.enabled = false;
        
        horizonatalBarChartChartView.drawGridBackgroundEnabled = false;
        
        horizonatalBarChartChartView.dragEnabled = true;
        horizonatalBarChartChartView.setScaleEnabled(true);
        horizonatalBarChartChartView.pinchZoomEnabled = false;
        
        // ChartYAxis *leftAxis = chartView.leftAxis;
        
        horizonatalBarChartChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom;
        horizonatalBarChartChartView.rightAxis.enabled = false;
        
        
        horizonatalBarChartChartView.drawBarShadowEnabled = false;
        horizonatalBarChartChartView.drawValueAboveBarEnabled = true;
        
        horizonatalBarChartChartView.maxVisibleCount = 60;
        
        //        let xAxis : XAxis = horizonatalBarChartChartView.xAxis;
        horizonatalBarChartChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        horizonatalBarChartChartView.xAxis.labelFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        horizonatalBarChartChartView.xAxis.drawAxisLineEnabled = true;
        horizonatalBarChartChartView.xAxis.drawGridLinesEnabled = false;
        horizonatalBarChartChartView.xAxis.granularity = 10.0;
        
        //        ChartYAxis *leftAxis = horizonatalBarChartChartView.leftAxis;
        horizonatalBarChartChartView.leftAxis.labelFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        horizonatalBarChartChartView.leftAxis.drawAxisLineEnabled = true;
        horizonatalBarChartChartView.leftAxis.drawGridLinesEnabled = true;
        horizonatalBarChartChartView.leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        //        ChartYAxis *rightAxis = horizonatalBarChartChartView.rightAxis;
        horizonatalBarChartChartView.rightAxis.enabled = true;
        horizonatalBarChartChartView.rightAxis.labelFont = UIFont.systemFont(ofSize: CGFloat(10.0))
        horizonatalBarChartChartView.rightAxis.drawAxisLineEnabled = true;
        horizonatalBarChartChartView.rightAxis.drawGridLinesEnabled = false;
        horizonatalBarChartChartView.rightAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        //        let legend : Legend = horizonatalBarChartChartView.legend;
        horizonatalBarChartChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.left
        horizonatalBarChartChartView.legend.verticalAlignment = Legend.VerticalAlignment.bottom
        horizonatalBarChartChartView.legend.orientation = Legend.Orientation.horizontal;
        horizonatalBarChartChartView.legend.drawInside = false;
        horizonatalBarChartChartView.legend.form = Legend.Form.square;
        horizonatalBarChartChartView.legend.formSize = 8.0;
        horizonatalBarChartChartView.legend.font = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(11.0))! // [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
        horizonatalBarChartChartView.legend.xEntrySpace = 4.0;
        
        horizonatalBarChartChartView.fitBars = true
        
        if (self.enableAnimation)
        {
            horizonatalBarChartChartView.animate(yAxisDuration: 2.5)
        }
        
        let barWidth : Double = 9.0;
        let spaceForBar : Int = 10;
        
        var yVals : [BarChartDataEntry] = [];
        
        for i in 0..<25
        {
            let mult : Double = (50 + 1);
            let val :Double = Double((arc4random_uniform(UInt32(mult))))
            yVals.append(BarChartDataEntry.init(x: Double(i * spaceForBar), y: val))// [[BarChartDataEntry alloc] initWithX:i * spaceForBar y:val]];
        }
        
        let set1 = BarChartDataSet.init(values: yVals, label: "DataSet")  //  [[BarChartDataSet alloc] initWithValues:yVals label:"DataSet"];
        
        var dataSets : [BarChartDataSet] = [];
        dataSets.append(set1)
        
        let data : BarChartData = BarChartData.init(dataSets: dataSets)
        data.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(10.0))) // setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
        data.barWidth = barWidth;
        
        horizonatalBarChartChartView.data = data;
    }
    */
    
    func createSingleLineChart(data : [String : Int])
    {
        // Chart Creation
        let lineChartView = LineChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        lineChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        lineChartView.delegate = self
        lineChartView.noDataText = "No data available for the chart."
        lineChartView.noDataFont = UIFont(name: "HelveticaNeue", size: 32.0)
        lineChartView.tag = 500
        self.view.addSubview(lineChartView)
        self.activeChartView = lineChartView
        
        // Chart general settings
        if let desc = self.chartObject!.chartDescription
        {
            if desc.lengthOfBytes(using: .utf8) > 0
            {
                lineChartView.chartDescription?.text = desc
                lineChartView.chartDescription?.enabled = true
                lineChartView.chartDescription?.font = NSUIFont.systemFont(ofSize: 14.0)
            }
            else
            {
                lineChartView.chartDescription?.enabled = false
            }
        }
        else
        {
            lineChartView.chartDescription?.enabled = false
        }
        
        let leftAxis = lineChartView.leftAxis
        leftAxis.axisMinimum = 0.0
        
        let rightAxis = lineChartView.rightAxis
        rightAxis.axisMinimum = 0.0
        rightAxis.enabled = false

        let marker : XYMarkerView? = XYMarkerView.init(color: .gray, font: UIFont.systemFont(ofSize: 14.0), textColor: .white, insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0), xAxisValueFormatter: self)
        marker?.minimumSize = CGSize.init(width: 80.0, height: 40.0)
        lineChartView.marker = marker;

        lineChartView.backgroundColor = self.chartObject?.chartBackgroundColor
        
        let xAxis = lineChartView.xAxis;
        if(self.chartObject!.xAxisLabelsCount > 0)
        {
            xAxis.labelPosition = .bottom
            xAxis.labelFont = UIFont.systemFont(ofSize: 14.0)
            xAxis.drawGridLinesEnabled = true;
            xAxis.granularity = 1.0; // only intervals of 1 day
            xAxis.labelCount = self.chartObject!.xAxisLabelsCount
//            xAxis.valueFormatter = self
            xAxis.enabled = true;
        }
        else
        {
            xAxis.enabled = false;
        }
        if(self.chartObject?.gridLinesType == .xAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            xAxis.drawGridLinesEnabled = true
            xAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            xAxis.drawGridLinesEnabled = false
        }
        if(self.chartObject?.gridLinesType == .yAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            leftAxis.drawGridLinesEnabled = true
            leftAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            leftAxis.drawGridLinesEnabled = false
        }
        

//         Not working
        lineChartView.borderColor = self.chartObject!.borderColor
        lineChartView.borderLineWidth = CGFloat(self.chartObject!.borderWidth)
        if(self.chartObject!.borderWidth > 0)
        {
            lineChartView.drawBordersEnabled = true
        }
        else
        {
            lineChartView.drawBordersEnabled = false
        }
        
        /*
         let legend = barChartView!.legend;
         legend.horizontalAlignment = .right
         legend.verticalAlignment = .bottom
         legend.orientation = .horizontal
         legend.direction = .leftToRight
         legend.form = .square;
         legend.formSize = 8.0;
         legend.formToTextSpace = 4.0;
         legend.xEntrySpace = 6.0;
         */

        var chartData : LineChartData? = nil
        
        // Load the chart up with data
        if(data.count > 0)
        {
            // Read the label names
            self.xAxisLabels = []
            for (item, _) in data
            {
                self.xAxisLabels.append(item)
            }
            self.xAxisLabels = self.xAxisLabels.sorted()
            
            // For each label get the corresponding data, in order
            var dataValues : [Int] = []
            for label in self.xAxisLabels
            {
                let dataValue = data[label]
                dataValues.append(dataValue!)
            }
            
            var dataEntries: [ChartDataEntry] = []
            
            for cnt in 0..<self.xAxisLabels.count
            {
                let dataEntry = ChartDataEntry(x: Double(cnt), y: Double(dataValues[cnt]), data: self.xAxisLabels as AnyObject?)
                dataEntries.append(dataEntry)
            }
            
            let chartDataSet = LineChartDataSet(values: dataEntries, label: self.chartObject?.accumulatorColumn)
            chartData = LineChartData(dataSet: chartDataSet)
            if(self.chartObject!.displayValueLabelLocation == .hide)
            {
                chartData?.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 0.0))
            }
            else
            {
                chartData?.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 12.0))
            }
            
            chartDataSet.colors = [self.chartObject!.chartColors![0]]
            let colorVal = self.chartObject!.chartColors![0]
            
            chartDataSet.axisDependency = .left
            chartDataSet.setColor(colorVal)
            chartDataSet.setCircleColor(colorVal)
            chartDataSet.lineWidth = 2.0
            chartDataSet.circleRadius = 3.0
            chartDataSet.fillAlpha = 65/255.0
            chartDataSet.fillColor = colorVal
            chartDataSet.highlightColor = colorVal
            chartDataSet.drawCircleHoleEnabled = false

            // Setup animations
            if (self.enableAnimation)
            {
                if(self.chartObject!.animationType == .xAxis)
                {
                    lineChartView.animate(xAxisDuration: 2.0)
                }
                else if(self.chartObject!.animationType == .yAxis)
                {
                    lineChartView.animate(yAxisDuration: 2.0)
                }
                else if(self.chartObject!.animationType == .xyAxis)
                {
                    lineChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
                }
            }
        }
        lineChartView.data = chartData        
    }
    
    func createLineChart(data : [String : [[String : Int]]])
    {
        // Chart Creation
        let lineChartView = LineChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        lineChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        lineChartView.delegate = self
        lineChartView.noDataText = "No data available for the chart."
        lineChartView.noDataFont = UIFont(name: "HelveticaNeue", size: 32.0)
        lineChartView.tag = 500
        self.view.addSubview(lineChartView)
        self.activeChartView = lineChartView
        
        // Chart general settings
        if let desc = self.chartObject!.chartDescription
        {
            if desc.lengthOfBytes(using: .utf8) > 0
            {
                lineChartView.chartDescription?.text = desc
                lineChartView.chartDescription?.enabled = true
                lineChartView.chartDescription?.font = NSUIFont.systemFont(ofSize: 14.0)
            }
            else
            {
                lineChartView.chartDescription?.enabled = false
            }
        }
        else
        {
            lineChartView.chartDescription?.enabled = false
        }
        
        let leftAxis = lineChartView.leftAxis
        leftAxis.axisMinimum = 0.0
        
        let rightAxis = lineChartView.rightAxis
        rightAxis.axisMinimum = 0.0
        rightAxis.enabled = false

        // Configure popup marker
        let marker : XYMarkerView? = XYMarkerView.init(color: .gray, font: UIFont.systemFont(ofSize: 14.0), textColor: .white, insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0), xAxisValueFormatter: self)
        marker?.minimumSize = CGSize.init(width: 80.0, height: 40.0)
        lineChartView.marker = marker;
        
        lineChartView.backgroundColor = self.chartObject?.chartBackgroundColor

        let xAxis = lineChartView.xAxis;
        if(self.chartObject!.xAxisLabelsCount > 0)
        {
            xAxis.labelPosition = .bottom
            xAxis.labelFont = UIFont.systemFont(ofSize: 14.0)
            xAxis.drawGridLinesEnabled = true;
            xAxis.granularity = 1.0; // only intervals of 1 day
            xAxis.labelCount = self.chartObject!.xAxisLabelsCount
//            xAxis.valueFormchrisatter = self
            xAxis.enabled = true;
        }
        else
        {
            xAxis.enabled = false;
        }
        if(self.chartObject?.gridLinesType == .xAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            xAxis.drawGridLinesEnabled = true
            xAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            xAxis.drawGridLinesEnabled = false
        }
        if(self.chartObject?.gridLinesType == .yAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            leftAxis.drawGridLinesEnabled = true
            leftAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            leftAxis.drawGridLinesEnabled = false
        }
        

        lineChartView.borderColor = self.chartObject!.borderColor
        lineChartView.borderLineWidth = CGFloat(self.chartObject!.borderWidth)
        if(self.chartObject!.borderWidth > 0)
        {
            lineChartView.drawBordersEnabled = true
        }
        else
        {
            lineChartView.drawBordersEnabled = false
        }

        /*
         let legend = barChartView!.legend;
         legend.horizontalAlignment = .right
         legend.verticalAlignment = .bottom
         legend.orientation = .horizontal
         legend.direction = .leftToRight
         legend.form = .square;
         legend.formSize = 8.0;
         legend.formToTextSpace = 4.0;
         legend.xEntrySpace = 6.0;
         */
        

        
        var stackLabels : [String] = []
        for (item, _) in data
        {
            stackLabels.append(item)
        }
        
        stackLabels = data.keys.sorted(by: { (key1, key2) -> Bool in
            
            if(key1.compare(key2) == .orderedAscending)
            {
                return true
            }
            
            return false
        })

        self.xAxisLabels = []
        
        for name in stackLabels
        {
            let dataArray = data[name]
            
            for dataDict in dataArray!
            {
                for dataName in dataDict.keys
                {
                    if(!xAxisLabels.contains(dataName))
                    {
                        xAxisLabels.append(dataName)
                    }
                }
            }
            
        }
        xAxisLabels = xAxisLabels.sorted()
        
        var dataValues : [LineChartDataSet] = []
        
        
        var nameCnt = 0;
        var colorCount = 0;
        for name in stackLabels
        {
            var yVals = [ChartDataEntry]()
            let dataArray = data[name]
            var lookupDict : [String : Int] = [:]
            
            for dataDict in dataArray!
            {
                for (key, keyVal) in dataDict
                {
                    lookupDict[key] = keyVal
                }
            }

            var counter : Int = 0;
            for item in self.xAxisLabels
            {
                let dataValue : Int? = lookupDict[item]
                if let itemDataValue = dataValue
                {
                    yVals.append(ChartDataEntry.init(x: Double(counter), y:Double(itemDataValue)))
                }
                else
                {
                    yVals.append(ChartDataEntry.init(x: Double(counter), y:Double(0)))
                }
                
                counter = counter + 1
            }
            
            if(colorCount >= self.chartObject!.chartColors!.count)
            {
                colorCount = 0
            }
            let colorVal = self.chartObject!.chartColors![colorCount]
            
            let chartDataSet : LineChartDataSet = LineChartDataSet.init(values: yVals, label: name)
            chartDataSet.axisDependency = .left
            chartDataSet.setColor(colorVal)
            chartDataSet.setCircleColor(colorVal)
            chartDataSet.lineWidth = 2.0
            chartDataSet.circleRadius = 3.0
            chartDataSet.fillAlpha = 65/255.0
            chartDataSet.fillColor = colorVal
            chartDataSet.highlightColor = colorVal
            chartDataSet.drawCircleHoleEnabled = false
            
            dataValues.append(chartDataSet)
            nameCnt = nameCnt + 1
            colorCount = colorCount + 1
        }
        

        if (self.enableAnimation)
        {
            if(self.chartObject!.animationType == .xAxis)
            {
                lineChartView.animate(xAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .yAxis)
            {
                lineChartView.animate(yAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .xyAxis)
            {
                lineChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
            }
        }

        let lineChartData = LineChartData.init(dataSets: dataValues)
        if(self.chartObject!.displayValueLabelLocation == .hide)
        {
            lineChartData.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 0.0))
        }
        else
        {
            lineChartData.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 12.0))
        }
        lineChartView.data = lineChartData
    }
/*
    func setLineChart()
    {
        let lineChartView = LineChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        lineChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        lineChartView.delegate = self
        self.view.addSubview(lineChartView)
        lineChartView.isUserInteractionEnabled = false

        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
        let unitsSold = [20.0, 14.0, 7.0, 10.0, 12.0, 16.0]

        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<months.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: unitsSold[i])
            dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Units Sold")
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        if (self.enableAnimation)
        {
            lineChartView.animate(xAxisDuration: 2.0)
        }
        lineChartView.data = lineChartData
    }
*/
    func createPieChart(dataArray : [String : Int])
    {
        // Chart Creation
        let pieChartView = PieChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        pieChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        pieChartView.delegate = self
        pieChartView.noDataText = "No data available for the chart."
        pieChartView.tag = 500
        self.view.addSubview(pieChartView)
        self.activeChartView = pieChartView
        
        // Chart general settings
        if let desc = self.chartObject!.chartDescription
        {
            if desc.lengthOfBytes(using: .utf8) > 0
            {
                pieChartView.chartDescription?.text = desc
                pieChartView.chartDescription?.enabled = true
                pieChartView.chartDescription?.font = NSUIFont.systemFont(ofSize: 14.0)
            }
            else
            {
                pieChartView.chartDescription?.enabled = false
            }
        }
        else
        {
            pieChartView.chartDescription?.enabled = false
        }
        
        // Configure popup marker
        
        
        
        let marker : PieChartMarker? = PieChartMarker.init(color: .gray, font: UIFont.systemFont(ofSize: 14.0), textColor: .white, insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0))
        marker?.minimumSize = CGSize.init(width: 80.0, height: 40.0)
        pieChartView.marker = marker;

        pieChartView.backgroundColor = self.chartObject?.chartBackgroundColor

        /* Needed ???
        let xAxis = pieChartView.xAxis;
        if(self.chartObject!.xAxisLabelsCount > 0)
        {
            xAxis.labelPosition = .bottom
            xAxis.labelFont = UIFont.systemFont(ofSize: 14.0)
            xAxis.drawGridLinesEnabled = true;
            xAxis.granularity = 1.0; // only intervals of 1 day
            xAxis.labelCount = self.chartObject!.xAxisLabelsCount
            xAxis.valueFormatter = self
            xAxis.enabled = true;
        }
        else
        {
            xAxis.enabled = false;
        }
*/

        pieChartView.rotationEnabled = false;
        
        if(self.chartObject!.displayPieChartWithHole)
        {
            pieChartView.drawHoleEnabled = true
        }
        else
        {
            pieChartView.drawHoleEnabled = false
        }
        pieChartView.holeColor = self.chartObject!.pieChartHoleColor
        pieChartView.transparentCircleColor = self.chartObject!.pieChartHoleColor
        pieChartView.holeRadiusPercent = 0.58;

        if(self.chartObject!.displayHalfPieChart)
        {
            pieChartView.maxAngle = 180.0; // Half chart
            pieChartView.rotationAngle = 180.0; // Rotate to make the half on the upper side
        }
        else
        {
            pieChartView.maxAngle = 360.0; // Half chart
            pieChartView.rotationAngle = 270.0; // Rotate to make the half on the upper side
        }

        /*
         let legend = barChartView!.legend;
         legend.horizontalAlignment = .right
         legend.verticalAlignment = .bottom
         legend.orientation = .horizontal
         legend.direction = .leftToRight
         legend.form = .square;
         legend.formSize = 8.0;
         legend.formToTextSpace = 4.0;
         legend.xEntrySpace = 6.0;
         */


        self.xAxisLabels = []
        var dataValues : [Int] = []
        
        for (item, dataValue) in dataArray
        {
            self.xAxisLabels.append(item)
            dataValues.append(dataValue)
        }

        var dataEntries: [PieChartDataEntry] = []
        
        for cnt in 0..<self.xAxisLabels.count {
            let dataEntry = PieChartDataEntry.init(value: Double(dataValues[cnt]), label: self.xAxisLabels[cnt], data: Double(cnt) as AnyObject?)
            dataEntries.append(dataEntry)
        }

        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: self.chartObject?.accumulatorColumn)
        let pieChartData = PieChartData(dataSets:[pieChartDataSet])
        
        if (self.enableAnimation)
        {
            if(self.chartObject!.animationType == .xAxis)
            {
                pieChartView.animate(xAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .yAxis)
            {
                pieChartView.animate(yAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .xyAxis)
            {
                pieChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
            }
        }
        pieChartDataSet.colors = self.chartObject!.chartColors!
        
        if(self.chartObject!.displayValueLabelLocation == .hide)
        {
            pieChartData.setValueFont(UIFont.init(name: "HelveticaNeue-Bold", size: 0.0))
        }
        else
        {
            pieChartData.setValueFont(UIFont.init(name: "HelveticaNeue-Bold", size: 17.0))
        }
        pieChartView.data = pieChartData
    }
/*
    func setPieChart()
    {
        let pieChartView = PieChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        pieChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        pieChartView.delegate = self
        self.view.addSubview(pieChartView)
        pieChartView.isUserInteractionEnabled = false

        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0]

        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<months.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: unitsSold[i])
            dataEntries.append(dataEntry)
        }
        
        pieChartView.chartDescription?.text = "User Data"

        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "Units Sold")
        let pieChartData = PieChartData(dataSets:[pieChartDataSet])
        
        var colors: [UIColor] = []
        
        for _ in 0..<months.count {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        if (self.enableAnimation)
        {
            pieChartView.animate(xAxisDuration: 2.0)
        }
        pieChartDataSet.colors = colors
        pieChartView.data = pieChartData
    }
*/
    func setRadarChart()
    {
        let radarChartView : RadarChartView = RadarChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        radarChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        radarChartView.delegate = self
        self.view.addSubview(radarChartView)
//        radarChartView.isUserInteractionEnabled = false

        radarChartView.chartDescription?.enabled = false
        radarChartView.webLineWidth = 1.0
        radarChartView.innerWebLineWidth = 1.0;
        radarChartView.webColor = UIColor.lightGray;
        radarChartView.innerWebColor = UIColor.lightGray;
        radarChartView.webAlpha = 1.0;
        self.activeChartView = radarChartView

        let marker = RadarMarkerView.viewFromXib()
        marker?.chartView = radarChartView;
        radarChartView.marker = marker;
        
        let xAxis : XAxis = radarChartView.xAxis;
        radarChartView.xAxis.labelFont = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(14.0))!
        xAxis.xOffset = 0.0;
        xAxis.yOffset = 0.0;
        xAxis.valueFormatter = self;
        xAxis.labelTextColor = UIColor.black;
        
//        ChartYAxis *yAxis = radarChartView.yAxis;
        radarChartView.yAxis.labelFont =  UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(9.0))!
        radarChartView.yAxis.labelCount = 5;
        radarChartView.yAxis.axisMinimum = 0.0;
        radarChartView.yAxis.axisMaximum = 80.0;
        radarChartView.yAxis.drawLabelsEnabled = false;
        
//        ChartLegend *l = radarChartView.legend
        radarChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.center
        radarChartView.legend.verticalAlignment = Legend.VerticalAlignment.top
        radarChartView.legend.orientation = Legend.Orientation.horizontal
        radarChartView.legend.drawInside = false
        radarChartView.legend.font = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(14.0))!
        radarChartView.legend.xEntrySpace = 7.0;
        radarChartView.legend.yEntrySpace = 5.0;
        radarChartView.legend.textColor = UIColor.black;

        
        let mult : Double = 80.0
        let min : Double = 20.0
        let cnt : Int = 5
        
        var entries1 : [RadarChartDataEntry] = [];
        var entries2 : [RadarChartDataEntry] = [];
        
        // NOTE: The order of the entries when being added to the entries array determines their position around the center of the chart.
        for _ in 0..<cnt
        {
            entries1.append(RadarChartDataEntry.init(value: Double(arc4random_uniform(UInt32(mult))) + min))
            entries2.append(RadarChartDataEntry.init(value: Double(arc4random_uniform(UInt32(mult))) + min))
        }
        
        let set1 : RadarChartDataSet = RadarChartDataSet.init(values: entries1, label: "Last Week")
        set1.setColor(UIColor.init(colorLiteralRed:103/255.0, green:110/255.0, blue:129/255.0, alpha:Float(1.0)))
        set1.fillColor = UIColor.init(colorLiteralRed:103/255.0, green:110/255.0, blue:129/255.0, alpha:Float(1.0))
        
        set1.drawFilledEnabled = true;
        set1.fillAlpha = 0.7;
        set1.lineWidth = 2.0;
        set1.drawHighlightCircleEnabled = true
        set1.setDrawHighlightIndicators(false)
        
        let set2 : RadarChartDataSet = RadarChartDataSet.init(values:entries2, label:"This Week")
        set2.setColor(UIColor.init(colorLiteralRed:121/255.0, green:162/255.0, blue:175/255.0, alpha:Float(1.0)))
        set2.fillColor =  UIColor.init(colorLiteralRed:121/255.0, green:162/255.0, blue:175/255.0, alpha:Float(1.0))
        set2.drawFilledEnabled = true
        set2.fillAlpha = 0.7;
        set2.lineWidth = 2.0;
        set2.drawHighlightCircleEnabled = true
        set2.setDrawHighlightIndicators(false)
        
        let data : RadarChartData = RadarChartData.init(dataSets: [set1, set2])
        data.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size:CGFloat(8.0)))
        data.setDrawValues(false)
        data.setValueTextColor(UIColor.black)
        
        self.xAxisLabels =  ["Burger", "Steak", "Salad", "Pasta", "Pizza"]
        
        radarChartView.data = data;

        if (self.enableAnimation)
        {
            radarChartView.animate(xAxisDuration:1.4, yAxisDuration:1.4, easingOption:ChartEasingOption.easeOutBack)
        }
    }
    
    /*func stringForValue(_ value : Double, axis : AxisBase?) -> String {
        
        var activities : [String] = [];
        activities = ["Burger", "Steak", "Salad", "Pasta", "Pizza"]
        
        let sub : Int = Int(value) % activities.count
        let str : String = activities[sub]
        
        return str
    }*/

    func createScatterChart(data : [String : [[String : Int]]])
    {
        // Chart Creation
        let scatterChartView : ScatterChartView = ScatterChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        scatterChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        scatterChartView.delegate = self
        scatterChartView.noDataText = "No data available for the chart."
        scatterChartView.noDataFont = UIFont(name: "HelveticaNeue", size: 32.0)
        scatterChartView.tag = 500
        self.view.addSubview(scatterChartView)
        self.activeChartView = scatterChartView

        // Chart general settings
        if let desc = self.chartObject!.chartDescription
        {
            if desc.lengthOfBytes(using: .utf8) > 0
            {
                scatterChartView.chartDescription?.text = desc
                scatterChartView.chartDescription?.enabled = true
                scatterChartView.chartDescription?.font = NSUIFont.systemFont(ofSize: 14.0)
            }
            else
            {
                scatterChartView.chartDescription?.enabled = false
            }
        }
        else
        {
            scatterChartView.chartDescription?.enabled = false
        }

        let leftAxis = scatterChartView.leftAxis
        leftAxis.axisMinimum = 0.0
        
        let rightAxis = scatterChartView.rightAxis
        rightAxis.axisMinimum = 0.0
        rightAxis.enabled = false

        // Configure popup marker
        let marker : XYMarkerView? = XYMarkerView.init(color: .gray, font: UIFont.systemFont(ofSize: 14.0), textColor: .white, insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0), xAxisValueFormatter: self)
        marker!.chartView = scatterChartView;
        marker?.minimumSize = CGSize.init(width: 80.0, height: 40.0)
        scatterChartView.marker = marker;

        scatterChartView.backgroundColor = self.chartObject!.chartBackgroundColor

        let xAxis = scatterChartView.xAxis;
        if(self.chartObject!.xAxisLabelsCount > 0)
        {
            xAxis.labelPosition = .bottom
            xAxis.labelFont = UIFont.systemFont(ofSize: 14.0)
            xAxis.granularity = 1.0; // only intervals of 1 day
            xAxis.labelCount = self.chartObject!.xAxisLabelsCount
            
            
            
            
            
            
            
            
            //  Causing a retain cycle   START HERE
//            xAxis.valueFormatter = self
            
            
            
            
            
            
            
            xAxis.enabled = true;
        }
        else
        {
            xAxis.enabled = false;
        }

        if(self.chartObject?.gridLinesType == .xAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            xAxis.drawGridLinesEnabled = true
            xAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            xAxis.drawGridLinesEnabled = false
        }
        if(self.chartObject?.gridLinesType == .yAxisOnly || self.chartObject?.gridLinesType == .bothAxis)
        {
            leftAxis.drawGridLinesEnabled = true
            leftAxis.gridColor = self.chartObject!.gridLineColor
        }
        else
        {
            leftAxis.drawGridLinesEnabled = false
        }
        
        
//        scatterChartView.axis

        scatterChartView.borderColor = self.chartObject!.borderColor
        scatterChartView.borderLineWidth = CGFloat(self.chartObject!.borderWidth)
        if(self.chartObject!.borderWidth > 0)
        {
            scatterChartView.drawBordersEnabled = true
        }
        else
        {
            scatterChartView.drawBordersEnabled = false
        }

        
        /*
        //        ChartLegend *l = scatterChartView.legend;
        scatterChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.right
        scatterChartView.legend.verticalAlignment = Legend.VerticalAlignment.top
        scatterChartView.legend.orientation = Legend.Orientation.vertical
        scatterChartView.legend.drawInside = false;
        scatterChartView.legend.font = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(10.0))! //[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
        scatterChartView.legend.xOffset = 5.0;
        */

        /*
         let legend = barChartView!.legend;
         legend.horizontalAlignment = .right
         legend.verticalAlignment = .bottom
         legend.orientation = .horizontal
         legend.direction = .leftToRight
         legend.form = .square;
         legend.formSize = 8.0;
         legend.formToTextSpace = 4.0;
         legend.xEntrySpace = 6.0;
         */
        
        self.xAxisLabels = []
        
        for (item, _) in data
        {
            self.xAxisLabels.append(item)
        }
        self.xAxisLabels = data.keys.sorted(by: { (key1, key2) -> Bool in
            
            if(key1.compare(key2) == .orderedAscending)
            {
                return true
            }
            
            return false
        })
        
        var stackLabels : [String] = []
        for name in self.xAxisLabels
        {
            let dataArray = data[name]
            
            for dataDict in dataArray!
            {
                for dataName in dataDict.keys
                {
                    if(!stackLabels.contains(dataName))
                    {
                        stackLabels.append(dataName)
                    }
                }
            }
            
        }
        stackLabels = stackLabels.sorted()
        
        
        var dataValues : [ScatterChartDataSet] = []
        
        
        var nameCnt = 0;
        var colorCount = 0;
        for name in self.xAxisLabels
        {
            var yVals = [ChartDataEntry]()
            let dataArray = data[name]
            var lookupDict : [String : Int] = [:]
            
            for dataDict in dataArray!
            {
                for (key, keyVal) in dataDict
                {
                    lookupDict[key] = keyVal
                }
            }
            
            var counter : Int = 0;
            for item in stackLabels
            {
                let dataValue : Int? = lookupDict[item]
                if let itemDataValue = dataValue
                {
                    yVals.append(ChartDataEntry.init(x: Double(counter), y:Double(itemDataValue)))
                }
                else
                {
                    yVals.append(ChartDataEntry.init(x: Double(counter), y:Double(0)))
                }
                
                counter = counter + 1
            }
            
            if(colorCount >= self.chartObject!.chartColors!.count)
            {
                colorCount = 0
            }
            let colorVal = self.chartObject!.chartColors![colorCount]
            
            let set : ScatterChartDataSet = ScatterChartDataSet.init(values: yVals, label: name)
            set.axisDependency = .left
            set.setColor(colorVal)
            set.scatterShapeSize = 8.0
            set.highlightColor = colorVal
            
            dataValues.append(set)
            nameCnt = nameCnt + 1
            colorCount = colorCount + 1
        }
        
        if (self.enableAnimation)
        {
            if(self.chartObject!.animationType == .xAxis)
            {
                scatterChartView.animate(xAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .yAxis)
            {
                scatterChartView.animate(yAxisDuration: 2.0)
            }
            else if(self.chartObject!.animationType == .xyAxis)
            {
                scatterChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
            }
        }
        
        var lineChartData : ScatterChartData? = nil
        if(dataValues.count > 0)
        {
            lineChartData = ScatterChartData.init(dataSets: dataValues)
            if(self.chartObject!.displayValueLabelLocation == .hide)
            {
                lineChartData!.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 0.0))
            }
            else
            {
                lineChartData!.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: 12.0))
            }
        }

        scatterChartView.data = lineChartData
    }

    func setScatterChart()
    {
        let scatterChartView : ScatterChartView = ScatterChartView.init(frame: CGRect(x: 0, y:0, width:self.view.frame.size.width, height:self.view.frame.size.height))
        scatterChartView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        scatterChartView.delegate = self
        self.view.addSubview(scatterChartView)
//        scatterChartView.isUserInteractionEnabled = false
        self.activeChartView = scatterChartView

        scatterChartView.chartDescription?.enabled = false
        
        scatterChartView.drawGridBackgroundEnabled = false
        scatterChartView.dragEnabled = false
        scatterChartView.setScaleEnabled(false)
        scatterChartView.maxVisibleCount = 200;
        scatterChartView.pinchZoomEnabled = true;

//        ChartLegend *l = scatterChartView.legend;
        scatterChartView.legend.horizontalAlignment = Legend.HorizontalAlignment.right
        scatterChartView.legend.verticalAlignment = Legend.VerticalAlignment.top
        scatterChartView.legend.orientation = Legend.Orientation.vertical
        scatterChartView.legend.drawInside = false;
        scatterChartView.legend.font = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(10.0))! //[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
        scatterChartView.legend.xOffset = 5.0;
        
//        ChartYAxis *yl = scatterChartView.leftAxis;
        scatterChartView.leftAxis.labelFont = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(10.0))!
        scatterChartView.leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        scatterChartView.rightAxis.enabled = false;
        
//        ChartXAxis *xl = scatterChartView.xAxis;
        scatterChartView.xAxis.labelFont = UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(10.0))!
        scatterChartView.xAxis.drawGridLinesEnabled = false;
        
        let count : Int = 15
        let range : Double = 27.0
        var yVals1 : [ChartDataEntry] = []
        var yVals2 : [ChartDataEntry] = []
        var yVals3 : [ChartDataEntry] = []

        for i in  0..<count
        {
            var val : Double = Double(arc4random_uniform(UInt32(range))) + 3;
            yVals1.append(ChartDataEntry.init(x: Double(i), y: val))
            
            val = Double(arc4random_uniform(UInt32(range))) + 3;
            yVals2.append(ChartDataEntry.init(x: Double(i) + 0.33, y: val))
            
            val = Double(arc4random_uniform(UInt32(range))) + 3;
            yVals3.append(ChartDataEntry.init(x: Double(i) + 0.66, y: val))
        }
        
        let set1 : ScatterChartDataSet = ScatterChartDataSet.init(values: yVals1, label: "DS 1")
        set1.setScatterShape(ScatterChartDataSet.Shape.square)
        set1.setColor(ChartColorTemplates.colorful()[0])
        let set2 : ScatterChartDataSet = ScatterChartDataSet.init(values: yVals2, label: "DS 2")
        set2.setScatterShape(ScatterChartDataSet.Shape.circle)
        set2.setColor(ChartColorTemplates.colorful()[3])
        set2.scatterShapeHoleRadius = 3.5;
        set2.setColor(ChartColorTemplates.colorful()[1])
        let set3 : ScatterChartDataSet = ScatterChartDataSet.init(values: yVals3, label: "DS 3")
        set3.setScatterShape(ScatterChartDataSet.Shape.cross)
        set3.setColor(ChartColorTemplates.colorful()[2])
        
        set1.scatterShapeSize = 8.0
        set2.scatterShapeSize = 8.0
        set3.scatterShapeSize = 8.0
        
        var dataSets : [ScatterChartDataSet] = []
        dataSets.append(set1)
        dataSets.append(set2)
        dataSets.append(set3)
        
        let data : ScatterChartData = ScatterChartData.init(dataSets: dataSets)
        data.setValueFont(UIFont.init(name: "HelveticaNeue-Light", size: CGFloat(10.0)))
        
        if (self.enableAnimation)
        {
            scatterChartView.animate(yAxisDuration: 3)
        }
        scatterChartView.data = data;

    }
    
    // Mark - IAxisValueFormatter
    
    @IBAction func saveChart(_ sender: UIBarButtonItem) {
        
        //        barChartView.save(to: <#T##String#>, format: <#T##ChartViewBase.ImageFormat#>, compressionQuality: <#T##Double#>)
        
        //        barChartView.saveToPath(path: String, format: ChartViewBase.ImageFormat, compressionQuality: Double)
        
    }
    
    func chartValueSelected(_ chartView: Charts.ChartViewBase, entry: Charts.ChartDataEntry, highlight: Charts.Highlight)
    {
//        print("\(entry.y) in \(months![Int(entry.x)])")
    }

    // MARK: - Actions

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
/*
        if let senderValue = sender
        {
            if(senderValue as! String == "Red")
            {
                segue.destination.view.backgroundColor = UIColor.red
            }
            else if(senderValue as! String == "Green")
            {
                segue.destination.view.backgroundColor = UIColor.green
            }
            else if(senderValue as! String == "Blue")
            {
                segue.destination.view.backgroundColor = UIColor.blue
            }
        }*/
    }
}
