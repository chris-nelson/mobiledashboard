//
//  CLColorPickerView.m
//  Pods
//
//  Created by Christopher Nelson on 9/6/15.
//
//

#import "CLColorPickerView.h"

@interface CLColorPickerView () <UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray* gridStandardColors;
@property (nonatomic, assign) NSUInteger colorStandardRows;
@property (nonatomic, assign) NSUInteger colorStandardColumns;
@property (nonatomic, assign) NSUInteger entryStandardWidth;
@property (nonatomic, assign) NSUInteger entryStandardHeight;

@property (nonatomic, strong) NSMutableArray* gridGrayScaleColors;
@property (nonatomic, assign) NSUInteger colorGrayScaleRows;
@property (nonatomic, assign) NSUInteger colorGrayScaleColumns;
@property (nonatomic, assign) NSUInteger entryGrayScaleWidth;
@property (nonatomic, assign) NSUInteger entryGrayScaleHeight;

@property (nonatomic, strong) NSMutableArray* gridColors;
@property (nonatomic, assign) NSUInteger colorRows;
@property (nonatomic, assign) NSUInteger colorColumns;
@property (nonatomic, assign) NSUInteger entryWidth;
@property (nonatomic, assign) NSUInteger entryHeight;

@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIImageView *checkeredView;
@property (nonatomic, strong) UIImageView *colorBar;

@property (nonatomic, strong) UIButton* clearColorButton;

@property (nonatomic, weak) CALayer *selectedColorLayer;

@property (nonatomic, assign, readwrite) NSUInteger currentPage;

@end

static NSUInteger const myColorPages = 14;
static NSUInteger const entryBorder = 8;
static NSUInteger const colorBorder = 20;
static NSUInteger const selectedColorWidth = 100;
static NSUInteger const selectedColorHeight = 40;

@implementation CLColorPickerView

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
}

- (UIButton*) clearColorButton
{
    if(_clearColorButton == nil)
    {
        _clearColorButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _clearColorButton.frame = CGRectMake(self.checkeredView.frame.origin.x + (self.checkeredView.frame.size.width / 2), 20, 200, 44); // position in the parent view and set the size of the button
        [_clearColorButton setTitle:@"Clear" forState:UIControlStateNormal];
        // add targets and actions
        [_clearColorButton addTarget:self action:@selector(clearButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        // add to a view
        [self addSubview:_clearColorButton];
    }
    
    return _clearColorButton;
}

- (void) generateColorGrid
{
    self.colorRows = 0;
    self.colorColumns = 0;
    self.entryWidth = 0;
    self.entryHeight = 0;
    
    NSUInteger screenWidth = self.frame.size.width;
    NSUInteger screenHeight = self.frame.size.height;
    
    if(self.checkeredView.gestureRecognizers.count > 0)
        [self.checkeredView removeGestureRecognizer:self.checkeredView.gestureRecognizers[0]];
    [self.checkeredView removeFromSuperview];
    self.checkeredView = nil;

    if(self.colorBar.gestureRecognizers.count > 0)
        [self.colorBar removeGestureRecognizer:self.colorBar.gestureRecognizers[0]];
    [self.colorBar removeFromSuperview];
    self.colorBar = nil;
    
    if(self.scrollView.gestureRecognizers.count > 0)
        [self.scrollView removeGestureRecognizer:self.scrollView.gestureRecognizers[0]];
    [self.scrollView removeFromSuperview];
    self.scrollView = nil;
    
    UIImage* colorbar = [UIImage imageNamed:@"color-bar"];
    UIImage* checkered = [UIImage imageNamed:@"color-picker-checkered"];

    CGRect frame = CGRectMake((screenWidth / 2) - (selectedColorWidth / 2), 16, selectedColorWidth, selectedColorHeight);
    _checkeredView = [[UIImageView alloc] initWithFrame:frame];
    _checkeredView.layer.cornerRadius = 6.0;
    _checkeredView.layer.masksToBounds = YES;
    _checkeredView.backgroundColor = [UIColor colorWithPatternImage:checkered];
    self.checkeredView.userInteractionEnabled = YES;
    UITapGestureRecognizer *checkeredViewRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkeredViewTapped:)];
    [self.checkeredView addGestureRecognizer:checkeredViewRecognizer];
    [self addSubview:_checkeredView];
    
    self.clearColorButton.frame = CGRectMake(self.checkeredView.frame.origin.x + (self.checkeredView.frame.size.width / 2), 20, 200, 44); // position in the parent view and set the size of
    [self.clearColorButton setTitle:@"Clear Color" forState:UIControlStateNormal];
    
    if(self.hideClearColorButton)
        self.clearColorButton.hidden = YES;
    else
        self.clearColorButton.hidden = NO;

    CALayer *layer = [CALayer layer];
    layer.frame = frame;
    layer.cornerRadius = 6.0;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 2);
    layer.shadowOpacity = 0.8;
    
    [self.selectedColorLayer removeFromSuperlayer];
    [self.layer addSublayer:layer];
    self.selectedColorLayer = layer;
    self.selectedColorLayer.backgroundColor = self.selectedColor.CGColor;
    
    frame = CGRectMake(0, self.frame.size.height - 40, self.frame.size.width, 40);
    _colorBar = [[UIImageView alloc] initWithFrame:frame];
    self.colorBar.image = colorbar;
    [self addSubview:_colorBar];
    
    self.colorBar.userInteractionEnabled = YES;
    UITapGestureRecognizer *barRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(colorBarTapped:)];
    [self.colorBar addGestureRecognizer:barRecognizer];

    NSUInteger topOffest = 70;
    NSUInteger bottomOffest = 80;
    
    NSUInteger scrollOffest = 40;
    self.gridStandardColors = [[NSMutableArray alloc] init];
    self.gridGrayScaleColors = [[NSMutableArray alloc] init];
    self.gridColors = [[NSMutableArray alloc] init];

    if(screenWidth > screenHeight)
    {
        self.colorStandardRows = 2;
        self.colorStandardColumns = 7;
    }
    else
    {
        self.colorStandardRows = 7;
        self.colorStandardColumns = 2;
    }
    
    [self.gridStandardColors addObject:[UIColor blackColor]];
    [self.gridStandardColors addObject:[UIColor darkGrayColor]];
    [self.gridStandardColors addObject:[UIColor lightGrayColor]];
    [self.gridStandardColors addObject:[UIColor whiteColor]];
    [self.gridStandardColors addObject:[UIColor grayColor]];
    [self.gridStandardColors addObject:[UIColor redColor]];
    [self.gridStandardColors addObject:[UIColor greenColor]];
    [self.gridStandardColors addObject:[UIColor blueColor]];
    [self.gridStandardColors addObject:[UIColor cyanColor]];
    [self.gridStandardColors addObject:[UIColor yellowColor]];
    [self.gridStandardColors addObject:[UIColor magentaColor]];
    [self.gridStandardColors addObject:[UIColor orangeColor]];
    [self.gridStandardColors addObject:[UIColor purpleColor]];
    [self.gridStandardColors addObject:[UIColor brownColor]];

    if(screenWidth > screenHeight)
    {
        self.colorGrayScaleRows = 4;
        self.colorGrayScaleColumns = 8;
    }
    else
    {
        self.colorGrayScaleRows = 8;
        self.colorGrayScaleColumns = 4;
    }

    for(NSUInteger colorValue = 0;colorValue <= 32;colorValue++)
    {
        if(colorValue == 32)
            [self.gridGrayScaleColors addObject:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
        else
            [self.gridGrayScaleColors addObject:[UIColor colorWithRed:colorValue/32.0 green:colorValue/32.0 blue:colorValue/32.0 alpha:1]];
    }
    
    NSUInteger colorPages = 12;

    if(screenWidth > screenHeight)
    {
        self.colorRows = 4;
        self.colorColumns = 8;

        for (NSUInteger page = 0 ; page < colorPages; page++)
        {
            CGFloat hue = page * 30 / 360.0;
            int colorCount = self.colorRows * self.colorColumns;
            for (NSUInteger colorEntry = 0; colorEntry < colorCount; colorEntry++)
            {
                int row = colorEntry / self.colorColumns;
                int column = colorEntry % self.colorColumns;
                
                CGFloat saturation = row * 0.25 + 0.25;
                CGFloat luminosity = 1.0 - column * 0.12;
                UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:luminosity alpha:1.0];
                [self.gridColors addObject:color];
            }
        }
    }
    else
    {
        self.colorRows = 8;
        self.colorColumns = 4;

        for (NSUInteger page = 0 ; page < colorPages; page++)
        {
            CGFloat hue = page * 30 / 360.0;
            int colorCount = self.colorRows * self.colorColumns;
            for (NSUInteger colorEntry = 0; colorEntry < colorCount; colorEntry++)
            {
                int row = colorEntry / self.colorColumns;
                int column = colorEntry % self.colorColumns;
                
                CGFloat saturation = column * 0.25 + 0.25;
                CGFloat luminosity = 1.0 - row * 0.12;
                UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:luminosity alpha:1.0];
                [self.gridColors addObject:color];
            }
        }
    }
    
    CGRect rect = CGRectMake(colorBorder, topOffest, screenWidth - (colorBorder * 2), screenHeight - bottomOffest - scrollOffest);
    self.scrollView = [[UIScrollView alloc] initWithFrame:rect];
    //self.scrollView.backgroundColor = [UIColor greenColor];
    self.scrollView.pagingEnabled = YES;
    [self addSubview:_scrollView];

    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(colorGridTapped:)];
    [self.scrollView addGestureRecognizer:recognizer];
    
    UITapGestureRecognizer *recognizerDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(colorGridDoubleTapped:)];
    recognizerDoubleTap.numberOfTapsRequired = 2;
    [self.scrollView addGestureRecognizer:recognizerDoubleTap];
    
    self.entryStandardWidth = (rect.size.width - (entryBorder * self.colorStandardColumns)) / self.colorStandardColumns;
    self.entryStandardHeight = (rect.size.height - (entryBorder * self.colorStandardRows)) / self.colorStandardRows;
    
    self.entryGrayScaleWidth = (rect.size.width - (entryBorder * self.colorGrayScaleColumns)) / self.colorGrayScaleColumns;
    self.entryGrayScaleHeight = (rect.size.height - (entryBorder * self.colorGrayScaleRows)) / self.colorGrayScaleRows;
    
    self.entryWidth = (rect.size.width - (entryBorder * self.colorColumns)) / self.colorColumns;
    self.entryHeight = (rect.size.height - (entryBorder * self.colorRows)) / self.colorRows;
    
    NSUInteger index = 0;
    for (NSUInteger page = 0; page < myColorPages; page++)
    {
        NSArray* gridArray = nil;
        NSUInteger colorRows = 0;
        NSUInteger colorColumns = 0;
        NSUInteger entryWidth = 0;
        NSUInteger entryHeight = 0;
        
        if(page == 0)
        {
            gridArray = self.gridStandardColors;
            colorRows = self.colorStandardRows;
            colorColumns = self.colorStandardColumns;
            entryWidth = self.entryStandardWidth;
            entryHeight = self.entryStandardHeight;
            index = 0;
        }
        else if(page == 1)
        {
            gridArray = self.gridGrayScaleColors;
            colorRows = self.colorGrayScaleRows;
            colorColumns = self.colorGrayScaleColumns;
            entryWidth = self.entryGrayScaleWidth;
            entryHeight = self.entryGrayScaleHeight;
            index = 0;
        }
        else
        {
            gridArray = self.gridColors;
            colorRows = self.colorRows;
            colorColumns = self.colorColumns;
            entryWidth = self.entryWidth;
            entryHeight = self.entryHeight;
            if(page == 2)
                index = 0;
        }

        int colorCount = colorRows * colorColumns;
        for (NSUInteger colorEntry = 0; colorEntry < colorCount && index < gridArray.count; colorEntry++)
        {
            CALayer *layer = [CALayer layer];
            layer.cornerRadius = 20.0;
            UIColor *color = [gridArray objectAtIndex:index++];
            layer.backgroundColor = color.CGColor;
            
            int column = colorEntry % colorColumns;
            int row = colorEntry / colorColumns;
            
            layer.frame = CGRectMake(((page * rect.size.width) + entryBorder) + (column * (entryWidth + entryBorder)), (row * (entryHeight+ entryBorder)) + entryBorder, entryWidth - entryBorder, entryHeight - entryBorder);
            [self setupShadow:layer];
            [self.scrollView.layer addSublayer:layer];
        }
    }
    
    self.scrollView.delegate = self;
    self.scrollView.contentSize = CGSizeMake(rect.size.width * myColorPages, rect.size.height);
}

- (void) setupShadow:(CALayer *)layer
{
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOpacity = 0.8;
    layer.shadowOffset = CGSizeMake(0, 2);
    CGRect rect = layer.frame;
    rect.origin = CGPointZero;
    layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:layer.cornerRadius].CGPath;
}

- (void) colorTapped:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:self.scrollView];
    int page = point.x / self.scrollView.frame.size.width;
    int delta = (int)point.x % (int)(self.scrollView.frame.size.width);
    
    NSArray* gridArray = nil;
    NSUInteger colorRows = 0;
    NSUInteger colorColumns = 0;
    NSUInteger entryWidth = 0;
    NSUInteger entryHeight = 0;
    
    if(page == 0)
    {
        gridArray = self.gridStandardColors;
        colorRows = self.colorStandardRows;
        colorColumns = self.colorStandardColumns;
        entryWidth = self.entryStandardWidth;
        entryHeight = self.entryStandardHeight;
    }
    else if(page == 1)
    {
        gridArray = self.gridGrayScaleColors;
        colorRows = self.colorGrayScaleRows;
        colorColumns = self.colorGrayScaleColumns;
        entryWidth = self.entryGrayScaleWidth;
        entryHeight = self.entryGrayScaleHeight;
    }
    else
    {
        gridArray = self.gridColors;
        colorRows = self.colorRows;
        colorColumns = self.colorColumns;
        entryWidth = self.entryWidth;
        entryHeight = self.entryHeight;
    }

    int row = (int)((point.y - entryBorder) / (entryHeight + entryBorder));
    int column = (int)((delta - entryBorder) / (entryWidth + entryBorder));
    int colorCount = colorRows * colorColumns;
    int index = colorCount * page + row * colorColumns + column;
    
    if(page == 1)
        index = colorCount * (page - 1) + row * colorColumns + column;
    else if(page >= 2)
        index = colorCount * (page - 2) + row * colorColumns + column;
    if (index < gridArray.count)
    {
        self.selectedColor = [gridArray objectAtIndex:index];
        self.selectedColorLayer.backgroundColor = self.selectedColor.CGColor;
        [self.selectedColorLayer setNeedsDisplay];
        
        if ([self.delegate respondsToSelector:@selector(colorPickerView:selectedColorGrid:)])
        {
            [self.delegate colorPickerView:self selectedColorGrid:self.selectedColor];
        }
    }
}

- (void) clearButtonClicked:(UIButton *)sender
{
    NSUInteger screenWidth = self.frame.size.width;

    CGRect frame = CGRectMake((screenWidth / 2) - (selectedColorWidth / 2), 16, selectedColorWidth, selectedColorHeight);
    CALayer *layer = [CALayer layer];
    layer.frame = frame;
    layer.cornerRadius = 6.0;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 2);
    layer.shadowOpacity = 0.8;

    self.selectedColor = [UIColor clearColor];
    [self.selectedColorLayer removeFromSuperlayer];
    [self.layer addSublayer:layer];
    self.selectedColorLayer = layer;
    self.selectedColorLayer.backgroundColor = self.selectedColor.CGColor;
}

- (void) colorGridTapped:(UITapGestureRecognizer *)recognizer
{
    [self colorTapped:recognizer];
}

- (void) colorGridDoubleTapped:(UITapGestureRecognizer *)recognizer
{
    [self colorTapped:recognizer];

    if ([self.delegate respondsToSelector:@selector(colorPickerView:selectedColorSelection:)])
    {
        [self.delegate colorPickerView:self selectedColorSelection:self.selectedColor];
    }
}

- (void) checkeredViewTapped:(UITapGestureRecognizer *)recognizer
{
    if ([self.delegate respondsToSelector:@selector(colorPickerView:selectedColorSelection:)])
    {
        if(self.selectedColor == nil)
            [self.delegate colorPickerView:self selectedColorSelection:[UIColor clearColor]];
        else
            [self.delegate colorPickerView:self selectedColorSelection:self.selectedColor];
    }
}

- (void) colorBarTapped:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:self.colorBar];
    NSUInteger page = point.x / (self.frame.size.width / myColorPages);
    [self scrollToPage:page animated:YES];
}

- (void) scrollToPage:(NSUInteger)page animated:(BOOL)animated
{
    [self.scrollView scrollRectToVisible:CGRectMake(page * self.scrollView.frame.size.width, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height) animated:animated];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat width = scrollView.frame.size.width;
    self.currentPage = (scrollView.contentOffset.x + (0.5f * width)) / width;
    [self.delegate colorPickerView:self didScrollToPage:self.currentPage];
}

@end
