//
//  ExternalViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 2/3/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ExternalViewController: UIViewController, ContainerViewControllerDelegate {

    var containerViewController : ContainerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender:sender)
        
        if(segue.identifier == "embedToExternalSegue")
        {
            if(segue.destination is ContainerViewController)
            {
                self.containerViewController = segue.destination as? ContainerViewController
                self.containerViewController?.initialSegueIdentifier = nil
                self.containerViewController?.view.tag = 100
                self.containerViewController?.delegate = self

            }
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
