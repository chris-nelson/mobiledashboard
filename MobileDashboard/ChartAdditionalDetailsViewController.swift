//
// ChartAdditionalDetailsViewController.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 2/1/17.
//  Copyright © 2017 Anderson Merchandisers. All rights reserved.
//

import UIKit

class ChartAdditionalDetailsViewController: UIViewController, ColorSelectionDelegate {

    var chartDataObject : ChartDataObject?
    weak var chartDelegate : ChartConfigurationDelegate?

    @IBOutlet weak var displayAsSingleColorLabel: UILabel!
    @IBOutlet weak var displayAsSingleColorSwitch: UISwitch!
    @IBOutlet weak var borderWidthTextField: UITextField!
    @IBOutlet weak var borderWidthStepper: UIStepper!
    @IBOutlet weak var borderColorDisplayView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if(chartDataObject?.chartType == .barChart || chartDataObject?.chartType == .horizonatalBarChart)
        {
            self.displayAsSingleColorSwitch.isHidden = false
            self.displayAsSingleColorLabel.isHidden = false
        }
        else
        {
            self.chartDataObject!.displayAsSingleColor = false
            self.displayAsSingleColorSwitch.isHidden = true
            self.displayAsSingleColorLabel.isHidden = true
        }
        
        self.displayAsSingleColorSwitch.isOn = self.chartDataObject!.displayAsSingleColor
        self.borderWidthTextField.text = String(describing: self.chartDataObject!.borderWidth)
        self.borderWidthStepper.value = Double(self.chartDataObject!.borderWidth)
        self.borderColorDisplayView.backgroundColor = self.chartDataObject!.borderColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func displayAsSingleColorValueChanged(_ sender: UISwitch)
    {
        self.chartDataObject!.displayAsSingleColor = sender.isOn
    }
    
    @IBAction func borderWidthValueChanged(_ sender: UIStepper)
    {
        self.chartDataObject!.borderWidth = Int(sender.value)
        self.borderWidthTextField.text = String(describing: self.chartDataObject!.borderWidth)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        if(segue.identifier == "colorSelectionSegue")
        {
            let vc = segue.destination as! ColorSelectionViewController
            vc.enableSaveButton = true
            vc.enableCloseButton = true
            vc.hideClearColorButton = true
            vc.selectedColor = self.chartDataObject?.borderColor
            
            vc.titleOverride = "Border Color"
            
            vc.delegate = self
        }
    }
    
    // MARK: - ColorSelectionDelegate
    func saveColorSelection(_ colorSelectionViewController: ColorSelectionViewController!, color: UIColor!) {
        
        self.chartDataObject?.borderColor = color
        self.borderColorDisplayView.backgroundColor = self.chartDataObject?.borderColor
        
        self.navigationController!.popViewController(animated: true)
    }
    
    func cancelColorSelection(_ colorSelectionViewController: ColorSelectionViewController!) {
        
        self.navigationController!.popViewController(animated: true)
    }
}
