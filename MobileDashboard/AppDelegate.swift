//
//  AppDelegate.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/29/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
        if(DashboardDataObject.shared.loggedIn || DashboardDataObject.shared.offlineMode)
        {
            UIView.animate(withDuration: 0.0, animations: { 

                // Dismiss any dialogs that may be present
                DashboardRootViewController.rootViewController().dismiss(animated: false, completion: nil)
                
            }, completion: { (finished) in
                
                DashboardRootViewController.rootViewController().returnLogin()
                
            })
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.handleDocumentInbox()
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        _ = self.processInboxFile(url: url)
        
        return true
    }
    
    func processInboxFile(url: URL?) -> Bool
    {
//        CHECK OUT WHY THIS IS NOT IMPORTING CORRECTLY, IT CLEARS OUT EXISTING
//        Also be sure the data is already loaded before importing, use a delay or something
        var pass = false
//        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        if(DashboardDataObject.shared.dataLoaded)
        {
            if (url != nil)
            {
                let chartPageDataObjectFound : ChartPageDataObject? = ChartPageDataObject.loadChartPage(fileURL: url!)
                if let chartPageDataObject = chartPageDataObjectFound
                {
                    DashboardDataObject.shared.importFromShare(chartPage: chartPageDataObject)
                    chartPageDataObject.deleteCacheImage()
                    pass = true
                    chartPageDataObject.saveChartPage(completion: { (successful) in
                        
                        if(successful)
                        {
                            self.removeFile(itemURL: url!)
                        }
                        DashboardRootViewController .rootViewController().refreshMenuItems()
                    })
                }
                
            }
        }
        else
        {
            self.perform(#selector(processInboxFile), with: url, afterDelay: 1)
        }
        
        return pass
    }

    func handleDocumentInbox()
    {
        var refreshNeeded = false
        if(DashboardDataObject.shared.dataLoaded)
        {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            let inboxPathURL : URL! = documentsURL?.appendingPathComponent("Inbox")
            
            do
            {
                let files = try FileManager.default.contentsOfDirectory(atPath: (inboxPathURL?.path)!)
                for file : String in files
                {
                    if(file != ".DS_Store")
                    {
                        let url = inboxPathURL.appendingPathComponent(file)
                        refreshNeeded = self.processInboxFile(url: url)
                    }
                }
                
                if(refreshNeeded)
                {
                }
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        else
        {
            self.perform(#selector(handleDocumentInbox), with: nil, afterDelay: 30)
        }
    }

    func removeFile(itemURL : URL)
    {
        if(FileManager.default.fileExists(atPath: itemURL.path))
        {
            do
            {
                try FileManager.default.removeItem(atPath: itemURL.path)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
    }
}

