//
//  DashboardSettingsObject.swift
//  MobileDashboard
//
//  Created by Christopher Nelson on 11/29/16.
//  Copyright © 2016 Anderson Merchandisers. All rights reserved.
//

import UIKit
class DashboardSettingsObject: NSObject {

    static let shared = DashboardSettingsObject()
    private let keychain = KeychainWrapper.init(identifier: "MobileDashboard", accessGroup: nil)

    var useTableView : Bool {
        get {
            
            return UserDefaults.standard.bool(forKey: "useTableView")
            
        }
        
        set {
            
            UserDefaults.standard.set(newValue, forKey: "useTableView")
            UserDefaults.standard.synchronize()
            
        }
    }
    
    var useTouchId : Bool {
        get {
            
            return UserDefaults.standard.bool(forKey: "useTouchId")
            
        }
        
        set {
            
            UserDefaults.standard.set(newValue, forKey: "useTouchId")
            UserDefaults.standard.synchronize()
            
        }
    }
    
    var userName : String? {
        get {
            return UserDefaults.standard.string(forKey: "userName")
            
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "userName")
            UserDefaults.standard.synchronize()
            
        }
    }
    
    var password : String? {
        get {
            
            var password : String? = keychain!.object(forKey: kSecValueData) as? String
            if let pwd = password
            {
                if( pwd.lengthOfBytes(using: String.Encoding.utf8) == 0)
                {
                    password = nil
                }
            }
            return password;
            
        }
        
        set {
            
            keychain!.setObject(newValue, forKey:kSecValueData)
        }
    }

    var userPIN : String? {
        get {
            
            var PIN : String? = keychain!.object(forKey: kSecAttrAccount) as? String
            if let pin = PIN
            {
                if( pin.lengthOfBytes(using: String.Encoding.utf8) == 0)
                {
                    PIN = nil
                }
            }
            return PIN;
        }
        
        set {
            if(newValue == nil)
            {
                keychain!.setObject("", forKey:kSecAttrAccount)
            }
            else
            {
                keychain!.setObject(newValue, forKey:kSecAttrAccount)
            }
        }
    }
    
}
